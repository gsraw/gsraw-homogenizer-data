# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
This phase can involve the following tasks:

1.  Formatting the data into columns to match the schema of the target data typology.
  
2.  Correcting mismatches and ensuring that columns are in the same order.

* Version
0.1

* [Learn more](https://whimsical.com/daybat-data-factory-R2evXaPGqs9H5mTvQQRd34)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution flow ###


```mermaid
graph LR
A[New contribution] -- Define type --> B{Type}
B -- bug--> C(Create branch bugfix/) --> E[PR develop] --Approved --> Y(develop) -- PR --> X
E -- Rejected --> E
B -- hotfix --> X((main))
B -- feature--> F(Create branch feature/) --> E
```

### Who do I talk to? ###

* Repo owner or admin
- Jesús Armand Calejero Román
* Other community or team contact
- Joris Schoemaker