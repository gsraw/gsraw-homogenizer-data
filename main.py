#!/usr/bin/env python3
import copy
import importlib
import sys
import asyncio
import pathlib
SRC = '/src'
TARGETS = '/targets'
EXECUTE_FUNCTION = 'run'
python_version_split = sys.version.split('.')
python_version = python_version_split[0] + '.' + python_version_split[1]
LIB_64 = f'/venv/lib64/python{python_version}/site-packages'
LIB = f'/venv/lib/python{python_version}/site-packages'
# current_directory: str = str(pathlib.Path().resolve())

# current_directory: str = '/home/ec2-user/runner'
current_directory: str = str(pathlib.Path(__file__).parent.resolve())
print(current_directory)
src_directory: str = current_directory + SRC
targets_directory: str = current_directory + TARGETS
lib_64_directory: str = current_directory + LIB_64
lib_directory: str = current_directory + LIB
sys.path.append(src_directory)
sys.path.append(current_directory)
sys.path.append(targets_directory)
sys.path.append(lib_directory)
sys.path.append(lib_64_directory)
# import uvloop
import dates
FOLDER_OPTION = '-f'
EXECUTE_FUNC_NAME = '-n'
CRAWL_OPTION = '-c'
TARGET_NAME = '-t'
START_OPTION = '-s'
END_OPTION = '-e'

OPTION_VALUES = {
        FOLDER_OPTION: None,
        START_OPTION: None,
        END_OPTION: None,
        EXECUTE_FUNC_NAME: None,
        TARGET_NAME: None,
        CRAWL_OPTION: None,
    }

CRAWL_OPTION_VALUE = "crawl"

def get_module_path(target_path) -> str:
    return f"targets.{target_path}"


def get_module_executing_function(module_path, execute_function_name):
    module = importlib.import_module(module_path)
    start = getattr(module, execute_function_name)
    return start


def execute(value_dict):
    path = value_dict[FOLDER_OPTION].replace('/', '.')
    if path[0] == '.':
        path = path[1:]
    module_path = get_module_path(path)
    execute = get_module_executing_function(module_path, value_dict[EXECUTE_FUNC_NAME])
    is_crawling = value_dict[CRAWL_OPTION]
    if is_crawling is not None and is_crawling == CRAWL_OPTION_VALUE:
        execute(value_dict[TARGET_NAME])
    else:
        execute(value_dict[START_OPTION], value_dict[END_OPTION], value_dict[TARGET_NAME])


def split_daybat_date(daybat_date: str):
    split_start = daybat_date.split('w')
    year = int(split_start[0])
    week = int(split_start[1])
    return week, year


def check_and_set_date(value_dict: dict):
    curr_week = dates.TodayIso.get()
    start = value_dict[START_OPTION]
    if start == 'current':
        value_dict[START_OPTION] = curr_week
        start = value_dict[START_OPTION]
    end = value_dict[END_OPTION]
    if end == 'current':
        value_dict[END_OPTION] = curr_week
        end = value_dict[END_OPTION]
    if (start is not None) and (end is not None):
        assert check_if_date_is_valid(start)
        assert check_if_date_is_valid(end)
        s_week, s_year = split_daybat_date(start)
        e_week, e_year = split_daybat_date(end)
        if e_year > s_year:
            return True
        elif (e_year == s_year) and (e_week > s_week):
            return True
    elif start is not None:
        assert check_if_date_is_valid(start)
        return True
    elif end is None:
        value_dict[START_OPTION] = curr_week
        return True
    return False


def check_if_date_is_valid(week_arg_value: str) -> bool:
    try:
        dates.TodayIso.get_previous_week(week_arg_value)
        return True
    except Exception as e:
        return False


def check_if_option(some_arg: str) -> bool:
    return some_arg in OPTION_VALUES


def check_non_whitespace_arg_ahead(args, current_index: int) -> int:
    current_index = current_index + 1
    while current_index < len(args):
        arg = args[current_index]
        if not check_if_whitespace(arg):
            if check_if_option(arg):
                return -1
            return current_index
        else:
            current_index += 1
    return -1


def check_if_whitespace(some_arg: str) -> bool:
    return some_arg == ''


def args_to_dict() -> dict:
    value_dict = copy.deepcopy(OPTION_VALUES)
    args = sys.argv[1:]
    len_args = len(args)
    i = 0

    while i < len_args:
        value_not_found = True
        arg = args[i]
        if not check_if_whitespace(arg):
            if check_if_option(arg):
                value_index = check_non_whitespace_arg_ahead(args, i)
                if value_index == -1:
                    raise Exception(f"Invalid argument provided to option {arg}")
                value_dict[arg] = args[value_index]
                i = value_index
                value_not_found = False
        if value_not_found:
            i += 1
    return value_dict



if __name__ == '__main__':
    print("options example:\n"
          f"{FOLDER_OPTION} from targets directory path to script, so {FOLDER_OPTION} FTCA/parser or {FOLDER_OPTION} FTCA.parser\n"
          f"{EXECUTE_FUNC_NAME} name the function to execute, example {EXECUTE_FUNC_NAME} run\n"
          f"{TARGET_NAME} name of target, example {TARGET_NAME} ftca or {TARGET_NAME} ftca_test_run\n"
          f"{START_OPTION} start date, example {START_OPTION} 2021w46\n"
          f"{END_OPTION} final date to parse, example {START_OPTION} 2021w46 {END_OPTION} 2021w47\n"
          f"{CRAWL_OPTION} if you intend to crawl, example -c crawl. Note {END_OPTION}, {START_OPTION} are ignored when crawling \n"
          f"special example to run current week:\n"
          f"./main.py {FOLDER_OPTION} FTCA/parser {EXECUTE_FUNC_NAME} run {TARGET_NAME} ftca \n"
          f"Another example:"
          f"./main.py {FOLDER_OPTION} FTCA/parser {EXECUTE_FUNC_NAME} run {TARGET_NAME} ftca {START_OPTION} 2021w46 {END_OPTION}\n"
          )
    value_dict = args_to_dict()
    # asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
    print(f"debug value_dict = {value_dict}")
    if not check_and_set_date(value_dict):
        raise Exception("invalid options/values")
    print(f"argument_dict = {value_dict}")
    execute(value_dict)
