import asyncio
import collections
import gzip
import math
import traceback
from time import time
from typing import List
import uvloop

import dates
from crawling_models.generic_methods import validate_url, logger, compute_total_count, upload_all_urls_to_database, \
    wait_threads_to_shutdown, upload_to_database_post, upload_to_database_get, upload_to_database_unlocked_post, \
    upload_to_database_get_unlocked
from data_model.crawling_data_model import CrawlDataModel
from http_lib.session_pool_no_proxy import UnboundedNoProxySessionPool
from targets.IDEIT.crawl_object import DynamicData
from http_lib.ProxyManager import ProxyInterface, get_packet_stream_proxy_manager, get_packet_stream_europe_manager_random
from http_lib.session_pool import UnboundedSessionPool
from http_lib.token_manager import SessionManager, NoProxySessionManager
from sql.batch_holder import BatcHolder
from sql.pool import Pool

# 199466
# (Including unoccupied properties over 1 year old)
# Find information on new single-family homes and single-family homes for sale
# Number of publications today 128,200
# 74,591

class Uploader():

    def __init__(self, is_total_count_available: bool, conn_pool: Pool,
                 batch_holder: BatcHolder):
        self.is_total_count_available = is_total_count_available
        self.invalid_urls: List[DynamicData] = []
        self.count = 0
        self.conn_pool = conn_pool
        self.batch_holder = batch_holder

    async def upload(self, data: CrawlDataModel, _bytes: bytes, response_status: int):
        data.data_bytes = gzip.compress(_bytes)
        data.response_status = response_status
        data.c_parse_date = dates.get_now()
        if self.is_total_count_available:
            self.count += data.count
        if not validate_url(data.url):
            # Clean from memory, you can keep data for debugging purposes.
            data.data_bytes = None
            self.invalid_urls.append(data)
        else:
            await self.batch_holder.insert_data_model_in_database_async(listing=data, conn_pool=self.conn_pool)

    def get_total_count(self):
        return self.count

    def show_invalid_urls(self):
        if len(self.invalid_urls) > 0:
            start_str: str = "invalid_urls below:\n"
            for d_data in self.invalid_urls:
                start_str += f"invalid url = {d_data.url}\n"


class DescendingTree:

    async def descent_region(self, data: DynamicData, semaphore: asyncio.BoundedSemaphore,
                             session_manager: SessionManager, uploader: Uploader,
                             cities: List[DynamicData], headers: dict, url_set: set):
        pass


    def create_initial_url_list(self) -> List[DynamicData]:
        pass

    async def descent_regions_iteratively_non_unique(self,initial_cities: list,
                                          session_pool: SessionManager,
                                          semaphore: asyncio.BoundedSemaphore,
                                          headers: dict,
                                          conn_pool: Pool,
                                          batch_holder: BatcHolder,
                                          is_total_count_available: bool) -> Uploader:
        #TODO fix for uniques through data rather than urls.
        logger.debug("descent_regions_iteratively_non_unique is called")
        stop = len(initial_cities)
        temp_cities: list = []
        url_set: set = set()
        uploader: Uploader = Uploader(is_total_count_available=is_total_count_available,
                                      batch_holder=batch_holder,
                                      conn_pool=conn_pool)
        while stop > 0:
            for i in range(stop):
                city = initial_cities.pop()
                if not validate_url(city.url):
                    raise Exception("invalid url while descending\n"
                                    f"url={city.url}")
                temp_cities.append(
                    asyncio.create_task(
                        self.descent_region(data=city, semaphore=semaphore, session_manager=session_pool,
                                            uploader=uploader, cities=initial_cities, headers=headers,
                                            url_set=url_set)
                    )
                )
            await asyncio.gather(*temp_cities)
            temp_cities = []
            stop = len(initial_cities)
        return uploader


    async def descent_regions_iteratively(self,initial_cities: list,
                                          session_pool: SessionManager,
                                          semaphore: asyncio.BoundedSemaphore,
                                          headers: dict,
                                          conn_pool: Pool,
                                          batch_holder: BatcHolder,
                                          is_total_count_available: bool,
                                          uploader: Uploader = None) -> Uploader:
        logger.debug("descent_regions_iteratively is called")
        url_set: set = set()
        stop = len(initial_cities)
        temp_cities: list = []
        # uploader: Uploader = Uploader(is_total_count_available=is_total_count_available,
        #                               batch_holder=batch_holder,
        #                               conn_pool=conn_pool)
        while stop > 0:
            for i in range(stop):
                city = initial_cities.pop()
                url: str = city.url

                if not validate_url(url):
                    raise Exception("invalid url while descending\n"
                                    f"url={url}")
                if not (url in url_set):
                    url_set.add(url)
                    temp_cities.append(
                        asyncio.create_task(
                            self.descent_region(data=city, semaphore=semaphore, session_manager=session_pool,
                                                uploader=uploader, cities=initial_cities, headers=headers,
                                                url_set=url_set)
                        )
                    )
                else:
                    print(f"NON UNIQUE URL = {url}")
                    print(f"NON UNIQUE URL = {url in url_set}")
            print(f"LEN TASKS = {len(temp_cities)}")
            print(f"LEN UNIQUE = {len(url_set)}")
            print(f"LEN STOP = {stop}")
            await asyncio.gather(*temp_cities)
            temp_cities = []
            stop = len(initial_cities)
        print(f"LEN URL_SET = {len(url_set)}")
        return uploader

    async def start(self, conn_pool: Pool,
                    api_headers: dict,
                    database_insert_batch_size: int,
                    database_fields: list,
                    database_table_name: str,
                    concurrent_boundary: int,
                    total_count_is_available: bool,
                    session_pool: SessionManager,
                    is_post_http_request: bool,
                    allow_redirects = True
                    ) -> None:
        batch_holder = BatcHolder(fields=database_fields,
                                  table_name=database_table_name,
                                  batch_size=database_insert_batch_size)
        try:
            semaphore = asyncio.BoundedSemaphore(concurrent_boundary)
            _start = time()
            initial_urls: List[DynamicData] = self.create_initial_url_list()
            uploader: Uploader = Uploader(is_total_count_available=total_count_is_available,
                                          batch_holder=batch_holder,
                                          conn_pool=conn_pool)

            if is_post_http_request:
                uploader: Uploader = await self.descent_regions_iteratively_non_unique(initial_cities=initial_urls,
                                                                                       semaphore=semaphore,
                                                                                       session_pool=session_pool,
                                                                                       headers=api_headers,
                                                                                       batch_holder=batch_holder,
                                                                                       conn_pool=conn_pool,
                                                                                       is_total_count_available=total_count_is_available)
            else:
                uploader: Uploader = await self.descent_regions_iteratively(initial_cities=initial_urls,
                                                                            semaphore=semaphore,
                                                                            session_pool=session_pool,
                                                                            headers=api_headers,
                                                                            batch_holder=batch_holder,
                                                                            conn_pool=conn_pool,
                                                                            is_total_count_available=total_count_is_available,
                                                                            uploader=uploader
                                                                            )

            await wait_threads_to_shutdown(batch_holder, session_pool, conn_pool)
            logger.debug(f"run took seconds :: {(time() - _start)}\n"
                         f"run total count, available = {total_count_is_available}, count = {uploader.get_total_count()}\n"
                         f"run connection rate :: {session_pool.get_connection_rate()},\n"
                         f"run successful connections per failure = {session_pool.successful_connections_per_failure()}\n,"
                         f"run average connection failure speed = {session_pool.get_average_error_connections_speed()}\n,"
                         f"run average connection success speed = {session_pool.get_average_success_connections_speed()}\n,"
                         f"run average connection speed = {session_pool.get_average_connection_speed()},\n"
                         f"run total_errors = {session_pool.total_errors},\n"
                         f"total_connections = {session_pool.total_connections()}")
            uploader.show_invalid_urls()
        except Exception as e:
            logger.critical(f'asyncio total crash\n'
                            f'{traceback.format_exc()}')
            logger.debug(f'connection rate = {session_pool.get_connection_rate()}\n'
                         f'total connections = {session_pool.total_connections()}')
            batch_holder.shutdown_async(conn_pool, wait=True)
            await session_pool.close()
            conn_pool.close()


def run_with_proxy(api_headers: dict,
                   database_insert_batch_size: int,
                   database_fields: list,
                   database_table_name: str,
                   single_session_total_allowed_connections: int,
                   http_response_retry_error_codes: list,
                   proxy_manager: ProxyInterface,
                   session_parameter_data: bool,
                   concurrent_boundary: int,
                   create_table_query: str,
                   crawler: DescendingTree,
                   is_post_http_request,
                   total_count_is_available: bool = True,
                   allow_redirects = True
                   ):

    try:
        asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
        pool: Pool = Pool(1)
        conn = pool.get_connection()
        cursor = conn.cursor()
        cursor.execute(create_table_query)
        conn.commit()
        pool.release_connection(conn)
        s_pool: UnboundedSessionPool = UnboundedSessionPool(boundary=single_session_total_allowed_connections,
                                                            proxy_manager=proxy_manager)
        session_pool: SessionManager = SessionManager(err_codes=http_response_retry_error_codes,
                                                      session_pool=s_pool, param=session_parameter_data)
        asyncio.run(crawler.start(conn_pool=pool,
                                  api_headers=api_headers,
                                  database_insert_batch_size=database_insert_batch_size,
                                  database_fields=database_fields,
                                  database_table_name=database_table_name,
                                  concurrent_boundary=concurrent_boundary,
                                  total_count_is_available=total_count_is_available,
                                  session_pool=session_pool,
                                  is_post_http_request=is_post_http_request,
                                  allow_redirects=allow_redirects
                        )
                    )
    except Exception as e:
        logger.critical(f'total crash\n'
                        f'{traceback.format_exc()}')


def run_without_proxy(api_headers: dict,
                   database_insert_batch_size: int,
                   database_fields: list,
                   database_table_name: str,
                   single_session_total_allowed_connections: int,
                   http_response_retry_error_codes: list,
                   session_parameter_data: bool,
                   concurrent_boundary: int,
                   create_table_query: str,
                   crawler: DescendingTree,
                   is_post_http_request: bool,
                   total_count_is_available: bool = True):
    try:
        asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
        pool: Pool = Pool(1)
        conn = pool.get_connection()
        cursor = conn.cursor()
        cursor.execute(create_table_query)
        conn.commit()
        pool.release_connection(conn)
        s_pool: UnboundedNoProxySessionPool = UnboundedNoProxySessionPool(boundary=single_session_total_allowed_connections)
        session_pool: NoProxySessionManager = NoProxySessionManager(err_codes=http_response_retry_error_codes,
                                                                    session_pool=s_pool,
                                                                    param=session_parameter_data)
        asyncio.run(crawler.start(conn_pool=pool,
                          api_headers=api_headers,
                          database_insert_batch_size=database_insert_batch_size,
                          database_fields=database_fields,
                          database_table_name=database_table_name,
                          concurrent_boundary=concurrent_boundary,
                          total_count_is_available=total_count_is_available,
                          session_pool=session_pool,
                          is_post_http_request=is_post_http_request
                        )
                    )
    except Exception as e:
        logger.critical(f'total crash\n'
                        f'{traceback.format_exc()}')


def run_pre_setup_packet_stream(api_headers,
                               database_fields: list,
                               database_table_name: str,
                               http_response_retry_error_codes: list,
                               create_table_query: str,
                               crawler: DescendingTree,
                               is_post_http_request: bool,
                               total_count_is_available: bool = True,
                               session_parameter_data: bool = False
                               ):

    # proxy_manager: ProxyInterface =  get_packet_stream_proxy_manager()
    proxy_manager: ProxyInterface = get_packet_stream_europe_manager_random()
    database_insert_batch_size = 128
    single_session_total_allowed_connections = 100
    concurrent_boundary = 150
    run_with_proxy(api_headers=api_headers,
                   database_insert_batch_size=database_insert_batch_size,
                   database_fields=database_fields,
                   database_table_name=database_table_name,
                   concurrent_boundary=concurrent_boundary,
                   create_table_query=create_table_query,
                   total_count_is_available=total_count_is_available,
                   session_parameter_data=session_parameter_data,
                   http_response_retry_error_codes=http_response_retry_error_codes,
                   single_session_total_allowed_connections=single_session_total_allowed_connections,
                   proxy_manager=proxy_manager,
                   crawler=crawler,
                   is_post_http_request=is_post_http_request)