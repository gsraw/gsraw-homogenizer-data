import asyncio
import collections
import math
import time
import traceback
from typing import List, Tuple, Type

import uvloop

from crawling_models.generic_methods import wait_threads_to_shutdown, validate_url, \
    remainder_query, upload_all_urls_to_database, get_different_listings_two_tables, incremental_and_remainder
from dates import TodayIso
from http_lib.session_pool import UnboundedSessionPool
from http_lib.session_pool_no_proxy import UnboundedNoProxySessionPool
from http_lib.token_manager import SessionManager, NoProxySessionManager
from parsing.seek import SeekQuery
from sql.CRUD import mysql_connection, table_exists, DATABASE_NAME
from sql.table_naming import create_table_name
from src.log import container
from sql.batch_holder import BatcHolder
from sql.pool import Pool
import http_lib.ProxyManager as proxy_manager
from targets.SELC.detail_crawler_object import FixedData, DynamicData

logger = container.get_logger(f'DetailCrawler', f'crawl.log')
DETAIL_PAGE_COLUMN = 'detail_page'

def database_split_queue(maxi: int, table_name: str, fields: list, step_size: int, id_col: str,
                         start_at: int = 0)\
        -> List[str]:
    seek = SeekQuery(table_name=table_name, maxi=maxi, fields=fields, step_size=step_size,id_name=id_col,
                     start_at=start_at)
    table_list: List[str] = []
    while True:
        if seek.is_threshold_surpassed():
            break
        table_list.append(seek.get_next_batch())
    return table_list


class DetailCrawler:


    def create_dynamic_data(self, database_row: tuple) -> DynamicData:
        pass

    def create_all_dynamic_data_of_unique_urls(self,
                                               all_database_rows: tuple,
                                               invalid_urls: List[DynamicData],
                                               url_set: set) \
            -> collections.deque[DynamicData]:
        logger.debug('create_all_dynamic_data_of_unique_urls is called')
        data_queue = collections.deque([])
        for row in all_database_rows:
            dynamic_data: DynamicData = self.create_dynamic_data(row)
            url = dynamic_data.url
            if validate_url(url):
                if not (url in url_set):
                    url_set.add(url)
                    data_queue.append(dynamic_data)
            else:
                invalid_urls.append(dynamic_data)
        return data_queue

    async def start(self, headers: dict, insert_data_base_fields: List[str],
                    session_manager: SessionManager, select_data_base_fields: List[str],
                    create_table: str, table_name: str, select_table_name: str, c_pool: Pool,
                    total_url_step_size: int = 100000,
                    concurrent_boundary: int = 300,
                    upload_batch_size: int = 32,
                    allow_redirects = False,
                    remainder: bool = False,
                    start_at_id: int = 0,
                    incremental = False
                    ):
        db_uploader = BatcHolder(fields=insert_data_base_fields,
                                 batch_size=upload_batch_size,
                                 table_name=table_name)
        # if incremental and remainder:
        #     raise Exception('Remainder of incremental is unimplemented')
        try:
            _start = time.time()
            semaphore = asyncio.BoundedSemaphore(concurrent_boundary)
            invalid_urls: List[DynamicData] = []
            url_set: set = set()
            with mysql_connection() as co:

                if incremental and remainder:
                    previous_week_table_name: str = TodayIso.get_previous_week_table_name(select_table_name)
                    if table_exists(connection=co, table=previous_week_table_name, schema=DATABASE_NAME):
                        database_split_queries: list[str] = [incremental_and_remainder(table_one=select_table_name,
                                                                                       table_two=previous_week_table_name,
                                                                                       table_three=table_name,
                                                                                       url_column=DETAIL_PAGE_COLUMN)]
                    else:
                        raise Exception("cannot run incremental, previous week table does not exist")
                else:
                    if remainder:
                        # database_split_queries: List[str] = [remainder_query(serp_parse_table_name=select_table_name,
                        #                                                      dp_crawl_table_name=table_name)]
                        database_split_queries: List[str] = [get_different_listings_two_tables(
                            table_one=select_table_name,
                            table_two=table_name,
                            url_column=DETAIL_PAGE_COLUMN
                        )]
                    elif incremental:
                        previous_week_table_name: str = TodayIso.get_previous_week_table_name(select_table_name)
                        if table_exists(connection=co, table=previous_week_table_name, schema=DATABASE_NAME):
                            database_split_queries: List[str] = [
                                get_different_listings_two_tables(table_one= select_table_name,
                                                                  table_two=previous_week_table_name,
                                                                  url_column=DETAIL_PAGE_COLUMN)
                            ]
                        else:
                            raise Exception("cannot run incremental, previous week table does not exist")
                    else:
                        with co.cursor() as cu:
                            cu.execute(f"SELECT MAX(id) FROM {select_table_name}")
                            MAX = cu.fetchone()[0]
                        if total_url_step_size > MAX:
                            total_url_step_size = MAX
                        database_split_queries: List[str] = database_split_queue(fields=select_data_base_fields,
                                                                                 id_col='id',
                                                                                 maxi=MAX,
                                                                                 step_size=total_url_step_size,
                                                                                 table_name=select_table_name,
                                                                                 start_at=start_at_id)
                with co.cursor() as cu_n:
                    cu_n.execute(create_table)
                    co.commit()
            for data_query in database_split_queries:
                print(data_query)
                conn = c_pool.get_connection()
                cursor = conn.cursor()
                cursor.execute(data_query)
                logger.debug("rows are being fetched")
                all_rows: tuple = cursor.fetchall()
                cursor.close()
                c_pool.release_connection(conn)

                data_queue: collections.deque = self.create_all_dynamic_data_of_unique_urls(all_rows, invalid_urls, url_set)
                logger.debug(f"length_data_queue = {len(data_queue)}")
                # # free memory
                # all_rows = ()
                await upload_all_urls_to_database(all_urls=data_queue,
                                                  batch_holder=db_uploader,
                                                  conn_pool=c_pool,
                                                  headers=headers,
                                                  semaphore=semaphore,
                                                  is_post_http_request=False,
                                                  session_pool=session_manager,
                                                  allow_redirects=allow_redirects)

            await wait_threads_to_shutdown(batch_holder=db_uploader,
                                           session_pool=session_manager,
                                           conn_pool=c_pool)
            if len(invalid_urls) > 0:
                start_str: str = "invalid_urls below:\n"
                for d_data in invalid_urls:
                    start_str += f"invalid url = {d_data.url}\n"
                logger.debug(start_str)
            logger.debug(f"run took seconds :: {(time.time() - _start)}\n"
                         f"run connection rate :: {session_manager.get_connection_rate()},\n"
                         f"run successful connections per failure = {session_manager.successful_connections_per_failure()}\n,"
                         f"run average connection failure speed = {session_manager.get_average_error_connections_speed()}\n,"
                         f"run average connection success speed = {session_manager.get_average_success_connections_speed()}\n,"
                         f"run average connection speed = {session_manager.get_average_connection_speed()},\n"
                         f"run total_errors = {session_manager.total_errors},\n"
                         f"total_connections = {session_manager.total_connections()}\n"
                         f"total_unique urls = {len(url_set)}")
        except Exception as e:
            logger.debug(f"asyncio crash \n"
                         f"traceback = \n"
                         f"{traceback.format_exc()}")
            await wait_threads_to_shutdown(batch_holder=db_uploader,
                                           session_pool=session_manager,
                                           conn_pool=c_pool)


def run_without_proxy(
                   headers: dict,
                   insert_data_base_fields: List[str],
                   single_session_total_allowed_connections: int,
                   http_response_retry_error_codes: list,
                   session_parameter_data: bool,
                   create_table_query: str,
                   crawler: DetailCrawler,
                   select_table_name: str,
                   table_name: str,
                   select_data_fields: list[str],
                   return_exceptions: Tuple[Type[Exception], ...],
                   incremental: bool,
                   total_url_step_size: int = 100000,
                   concurrent_boundary: int = 300,
                   upload_batch_size: int = 64,
                   allow_redirects = False,
                   remainder: bool = False,
                   start_at_id: int = 0
                   ):
    try:
        asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
        pool: Pool = Pool(1)
        s_pool: UnboundedNoProxySessionPool = UnboundedNoProxySessionPool(boundary=single_session_total_allowed_connections)
        session_pool: NoProxySessionManager = NoProxySessionManager(err_codes=http_response_retry_error_codes,
                                                                    session_pool=s_pool,
                                                                    param=session_parameter_data,
                                                                    return_exceptions=return_exceptions)
        asyncio.run(crawler.start(headers=headers,
                                  insert_data_base_fields=insert_data_base_fields,
                                  c_pool=pool,
                                  create_table=create_table_query,
                                  session_manager=session_pool,
                                  table_name=table_name,
                                  select_table_name=select_table_name,
                                  select_data_base_fields=select_data_fields,
                                  concurrent_boundary=concurrent_boundary,
                                  upload_batch_size=upload_batch_size,
                                  total_url_step_size=total_url_step_size,
                                  allow_redirects=allow_redirects,
                                  remainder=remainder,
                                  start_at_id=start_at_id,
                                  incremental=incremental
                        )
                    )
    except Exception as e:
        logger.critical(f'total crash\n'
                        f'{traceback.format_exc()}')


def run_with_proxy(
                   headers: dict,
                   insert_data_base_fields: List[str],
                   single_session_total_allowed_connections: int,
                   http_response_retry_error_codes: list,
                   session_parameter_data: bool,
                   create_table_query: str,
                   crawler: DetailCrawler,
                   select_table_name: str,
                   table_name: str,
                   select_data_fields: list[str],
                   incremental: bool,
                   return_exceptions: Tuple[Type[Exception], ...],
                   proxy_interface: proxy_manager.ProxyInterface,
                   total_url_step_size: int = 10000,
                   concurrent_boundary: int = 300,
                   upload_batch_size: int = 64,
                   allow_redirects = False,
                   remainder: bool = False,
                   start_at_id: int = 0
                   ):
    try:
        asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
        pool: Pool = Pool(1)
        s_pool: UnboundedSessionPool = UnboundedSessionPool(boundary=single_session_total_allowed_connections,
                                                            proxy_manager=proxy_interface)
        session_pool: SessionManager = SessionManager(err_codes=http_response_retry_error_codes,
                                                      session_pool=s_pool,
                                                      param=session_parameter_data)
        asyncio.run(crawler.start(headers=headers,
                                  insert_data_base_fields=insert_data_base_fields,
                                  c_pool=pool,
                                  create_table=create_table_query,
                                  session_manager=session_pool,
                                  table_name=table_name,
                                  select_table_name=select_table_name,
                                  select_data_base_fields=select_data_fields,
                                  concurrent_boundary=concurrent_boundary,
                                  upload_batch_size=upload_batch_size,
                                  total_url_step_size=total_url_step_size,
                                  allow_redirects=allow_redirects,
                                  remainder=remainder,
                                  start_at_id=start_at_id,
                                  incremental=incremental
                        )
                    )
    except Exception as e:
        logger.critical(f'total crash\n'
                        f'{traceback.format_exc()}')