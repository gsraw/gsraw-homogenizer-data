import asyncio
import collections
import gzip
from time import time
from typing import Coroutine, List, Tuple, Deque
from validators.url import url
import dates
from src.log import container
from data_model.crawling_data_model import CrawlDataModel
from http_lib.token_manager import SessionManager
from sql.batch_holder import BatcHolder
from sql.pool import Pool
from collections.abc import Callable

logger_name = 'crawl'
file_name = f"{logger_name}.log"
logger = container.get_logger(logger_name, file_name)


async def upload_dynamic_to_database(data: CrawlDataModel , batch_holder: BatcHolder) -> None:
    data.c_parse_date = dates.get_now()
    logger.debug(f'uploading {url}')
    await batch_holder.insert_data_model_in_database_async_internal(listing=data)

async def upload_to_database_get_unlocked(data: CrawlDataModel, headers:dict, session_pool: SessionManager,
                              batch_holder: BatcHolder, conn_pool: Pool, allow_redirects = True):
    url = data.url
    data.data_bytes, data.response_status = await session_pool.compress_and_status(url=url, headers=headers, allow_redirects=allow_redirects)
    data.c_parse_date = dates.get_now()
    logger.debug(f'uploading {url}')
    # _start = time()
    await batch_holder.insert_data_model_in_database_async(listing=data, conn_pool=conn_pool)
    # logger.debug(f"db upload time :: {(time() - _start)}")


async def get_url_compressed(session_manager: SessionManager, url, headers, allow_redirects: bool ,_kb: int = 100) -> Tuple[bytes,int]:
    if _kb == 0:
        return await session_manager.compress_and_status(url=url,
                                                     headers=headers,
                                                    allow_redirects=allow_redirects
                                                                )
    counter: int = 0
    max_tries: int = 10
    while counter < max_tries:
        _bytes, status = await session_manager.get_bytes_and_status(url=url,
                                                                     headers=headers,
                                                                    allow_redirects=allow_redirects
                                                                     )
        kb = len(_bytes) / 1024
        logger.debug(f"file_size (KB) = {kb}, status={status}")
        if status == 404 or kb > _kb:
            return gzip.compress(_bytes), status
        counter += 1
    raise Exception(f"for the following URL size always <= {_kb} kb:\n"
                      f"url= {url}\n"
                      f"size (KB) = {kb}")

async def upload_to_database_get(data: CrawlDataModel, headers:dict, session_pool: SessionManager,
                             semaphore, batch_holder: BatcHolder, conn_pool: Pool, allow_redirects = True,
                             minimum_file_size_in_kb: int = 0):
    await semaphore.acquire()
    url = data.url
    data.data_bytes, data.response_status = await get_url_compressed(session_manager=session_pool,
                                                          url=url,
                                                          headers=headers,
                                                          allow_redirects=allow_redirects,
                                                          _kb=minimum_file_size_in_kb)

    #    await session_pool.compress_and_status(url=url, headers=headers, allow_redirects=allow_redirects)
    data.c_parse_date = dates.get_now()
    logger.debug(f'get_uploading {url}')
    # _start = time()
    await batch_holder.insert_data_model_in_database_async(listing=data, conn_pool=conn_pool)
    # logger.debug(f"db upload time :: {(time() - _start)}")
    semaphore.release()

async def upload_to_database_post(data: CrawlDataModel, headers:dict, session_pool: SessionManager,
                             semaphore, batch_holder: BatcHolder, conn_pool: Pool, allow_redirects = True,
                             minimum_file_size_in_kb: int = 0):
    await semaphore.acquire()
    url = data.url
    post_data = data.data
    data.data_bytes, data.response_status = await session_pool.post_compress_and_status(url=url,
                                                                                        headers=headers,
                                                                                        data=post_data,
                                                                                        allow_redirects=allow_redirects)
    data.c_parse_date = dates.get_now()
    logger.debug(f'uploading {url}')
    # _start = time()
    await batch_holder.insert_data_model_in_database_async(listing=data, conn_pool=conn_pool)
    # logger.debug(f"db upload time :: {(time() - _start)}")
    semaphore.release()


async def upload_to_database_unlocked_post(data: CrawlDataModel, headers:dict, session_pool: SessionManager, batch_holder: BatcHolder, conn_pool: Pool, allow_redirects = True):
    url = data.url
    post_data = data.data
    data.data_bytes, data.response_status = await session_pool.post_compress_and_status(url=url,
                                                                                        headers=headers,
                                                                                        data=post_data,
                                                                                        allow_redirects=allow_redirects)
    data.c_parse_date = dates.get_now()
    logger.debug(f'uploading {url}')
    # _start = time()
    await batch_holder.insert_data_model_in_database_async(listing=data, conn_pool=conn_pool)

async def upload_all_urls_to_database(headers: dict, session_pool: SessionManager,
                             semaphore, batch_holder: BatcHolder, conn_pool: Pool,
                             all_urls: Deque,
                             is_post_http_request: bool,
                             allow_redirects: bool,
                             minimum_file_size_in_kb:int = 0) -> None:

    upload_to_database: Callable[[CrawlDataModel,
                                  dict,
                                  SessionManager,
                                  asyncio.BoundedSemaphore,
                                  BatcHolder,
                                  Pool
                                  ],
                                 Coroutine]
    if is_post_http_request:
        upload_to_database = upload_to_database_post
    else:
        upload_to_database = upload_to_database_get
    length = len(all_urls)
    temp_list: asyncio.tasks = []
    for i in range(length):
        data = all_urls.pop()
        task = asyncio.create_task(upload_to_database(data, headers, session_pool,
                semaphore, batch_holder, conn_pool, allow_redirects, minimum_file_size_in_kb))
        task.add_done_callback(lambda x: temp_list.remove(x))
        temp_list.append(
            task
        )
    await asyncio.gather(*temp_list)


async def wait_threads_to_shutdown(batch_holder: BatcHolder, session_pool: SessionManager, conn_pool: Pool):
    batch_holder.shutdown_async(conn_pool, wait=True)
    await session_pool.close()
    conn_pool.close()


def validate_url(_url: str):
    return url(_url)


def compute_total_count(final_list: Deque) -> int:
    total_count: int = 0
    for item in final_list:
        total_count += item.count
    return total_count


def incremental_and_remainder(table_one: str,
                              table_two: str,
                              table_three: str,
                              url_column: str,
                              ):
    return f"SELECT listing_id, {url_column}\n" \
           f"FROM {table_one}\n" \
           f"WHERE listing_id NOT IN (select listing_id from {table_two})" \
           f"AND listing_id not in (select listing_id from {table_three});"


def get_different_listings_two_tables(table_one: str,
                                      table_two: str,
                                      url_column: str):
    return f"SELECT listing_id, {url_column}\n" \
           f"FROM {table_one}\n" \
           f"WHERE listing_id NOT IN (select listing_id from {table_two});"


def remainder_query(serp_parse_table_name: str, dp_crawl_table_name: str) -> str:
    return f"SELECT listing_id, detail_page\n" \
           f"FROM\n" \
           f"(\n" \
           f"SELECT {serp_parse_table_name}.listing_id, {serp_parse_table_name}.detail_page\n" \
           f"FROM {serp_parse_table_name}\n" \
           f"UNION ALL\n " \
           f"SELECT {dp_crawl_table_name}.listing_id, {dp_crawl_table_name}.url\n" \
           f"FROM {dp_crawl_table_name} \n" \
           f") t\n" \
           f"GROUP BY listing_id\n" \
           f"HAVING COUNT(*) = 1\n" \
           f"ORDER BY listing_id;"


