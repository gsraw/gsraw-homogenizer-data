import asyncio
import collections
import gzip
import math
import traceback
from time import time
from typing import List, Type
import uvloop

import dates
from crawling_models.generic_methods import validate_url, logger, compute_total_count, upload_all_urls_to_database, \
    wait_threads_to_shutdown
from data_model.crawling_data_model import CrawlDataModel
from http_lib.session_pool_no_proxy import UnboundedNoProxySessionPool
from targets.IDEIT.crawl_object import DynamicData
from http_lib.ProxyManager import ProxyInterface, get_packet_stream_proxy_manager, get_packet_stream_europe_manager_random
from http_lib.session_pool import UnboundedSessionPool
from http_lib.token_manager import SessionManager, NoProxySessionManager
from sql.batch_holder import BatcHolder
from sql.pool import Pool

#TODO rebuild later by determing all urls with Log(10000)
from text_preprocessing import url_param_to_dict, str_to_dict


class PredictableTunnel:


    async def descent_region(self, data: DynamicData, semaphore: asyncio.BoundedSemaphore,
                             session_manager: SessionManager, cities: List[DynamicData], headers: dict,
                             batch_holder: BatcHolder, conn_pool: Pool):
        pass


    def create_initial_url_list(self) -> List[DynamicData]:
        pass

    def has_next_page(self, response_bytes: bytes) -> (bool, int):
        pass

    async def get_next_and_upload_to_database_post(self,data: CrawlDataModel, headers: dict, session_pool: SessionManager,
                                                   batch_holder: BatcHolder, conn_pool: Pool, allow_redirects=True) -> (bool, int):
        url = data.url
        print(f"data= {str_to_dict(data.data)}")
        # print(f"data= {str_to_dict(data.data)}")
        _bytes: bytes = await session_pool.pool_post(url=url,
                                                     headers=headers,
                                                     data=str_to_dict(data.data)
                                                    )
        data.data_bytes = gzip.compress(_bytes)
        data.c_parse_date = dates.get_now()
        logger.debug(f'uploading {url}')
        # _start = time()
        await batch_holder.insert_data_model_in_database_async(listing=data, conn_pool=conn_pool)
        return self.has_next_page(_bytes)

    async def get_next_and_upload_to_database(self, data: CrawlDataModel, headers: dict, session_pool: SessionManager,
                                                   batch_holder: BatcHolder, conn_pool: Pool, allow_redirects=True) -> (bool, int):
        url = data.url
        _bytes = await session_pool.pool_get(url=url,
                                             headers=headers)
        data.data_bytes = gzip.compress(_bytes)
        data.c_parse_date = dates.get_now()
        # data.response_status = status
        logger.debug(f'uploading {url}')
        # _start = time()
        await batch_holder.insert_data_model_in_database_async(listing=data, conn_pool=conn_pool)
        return self.has_next_page(_bytes)

    async def descent_regions_iteratively_non_unique(self,initial_cities: list,
                                          session_pool: SessionManager,
                                          semaphore: asyncio.BoundedSemaphore,
                                          headers: dict, batch_holder: BatcHolder,
                                          conn_pool: Pool):
        logger.debug("descent_regions_iteratively_non_unique is called")
        stop = len(initial_cities)
        temp_cities: list = []
        while stop > 0:
            for i in range(stop):
                city = initial_cities.pop()

                temp_cities.append(
                    asyncio.create_task(
                        self.descent_region(data=city, semaphore=semaphore, session_manager=session_pool,
                                            cities=initial_cities, headers=headers, batch_holder= batch_holder,
                                            conn_pool=conn_pool)
                    )
                )
            await asyncio.gather(*temp_cities)
            temp_cities = []
            stop = len(initial_cities)

    async def descent_regions_iteratively(self,initial_cities: list,
                                          session_pool: SessionManager,
                                          semaphore: asyncio.BoundedSemaphore,
                                          headers: dict, batch_holder: BatcHolder,
                                          conn_pool: Pool):
        logger.debug("descent_regions_iteratively is called")
        url_set: set = set()
        stop = len(initial_cities)
        temp_cities: list = []
        while stop > 0:
            for i in range(stop):
                city = initial_cities.pop()
                if not (city.url in url_set):
                    url_set.add(city.url)
                    temp_cities.append(
                        asyncio.create_task(
                            self.descent_region(data=city, semaphore=semaphore, session_manager=session_pool,
                                                cities=initial_cities, headers=headers,batch_holder=batch_holder,
                                                conn_pool=conn_pool)
                        )
                    )
            print(f"LEN TASKS = {temp_cities}")
            await asyncio.gather(*temp_cities)
            temp_cities = []
            stop = len(initial_cities)
        print(f"LENGTH URL_SET = {len(url_set)}")


    async def start(self, conn_pool: Pool,
                    api_headers: dict,
                    database_insert_batch_size: int,
                    database_fields: list,
                    database_table_name: str,
                    concurrent_boundary: int,
                    total_count_is_available: bool,
                    session_pool: SessionManager,
                    is_post_http_request: bool,
                    allow_redirects = True
                    ) -> None:
        batch_holder = BatcHolder(fields=database_fields,
                                  table_name=database_table_name,
                                  batch_size=database_insert_batch_size)
        try:
            semaphore = asyncio.BoundedSemaphore(concurrent_boundary)
            _start = time()
            initial_urls: List[DynamicData] = self.create_initial_url_list()

            if is_post_http_request:
                await self.descent_regions_iteratively_non_unique(initial_cities=initial_urls,
                                                                  semaphore=semaphore,
                                                                  session_pool=session_pool,
                                                                  headers=api_headers,
                                                                  batch_holder=batch_holder,
                                                                  conn_pool=conn_pool
                                                                  )
            else:
                await self.descent_regions_iteratively(initial_cities=initial_urls,
                                                       semaphore=semaphore,
                                                       session_pool=session_pool,
                                                       headers=api_headers,
                                                       batch_holder=batch_holder,
                                                       conn_pool=conn_pool)

            await wait_threads_to_shutdown(batch_holder, session_pool, conn_pool)
            logger.debug(f"run took seconds :: {(time() - _start)}\n"
                         # f"run total count, available = {total_count_is_available}, count = {total_count}\n"
                         f"run connection rate :: {session_pool.get_connection_rate()},\n"
                         f"run successful connections per failure = {session_pool.successful_connections_per_failure()}\n,"
                         f"run average connection failure speed = {session_pool.get_average_error_connections_speed()}\n,"
                         f"run average connection success speed = {session_pool.get_average_success_connections_speed()}\n,"
                         f"run average connection speed = {session_pool.get_average_connection_speed()},\n"
                         f"run total_errors = {session_pool.total_errors},\n"
                         f"total_connections = {session_pool.total_connections()}")
        except Exception as e:
            logger.critical(f'asyncio total crash\n'
                            f'{traceback.format_exc()}')
            logger.debug(f'connection rate = {session_pool.get_connection_rate()}\n'
                         f'total connections = {session_pool.total_connections}')
            batch_holder.shutdown_async(conn_pool, wait=True)
            await session_pool.close()
            conn_pool.close()


def run_with_proxy(api_headers: dict,
                   database_insert_batch_size: int,
                   database_fields: list,
                   database_table_name: str,
                   single_session_total_allowed_connections: int,
                   http_response_retry_error_codes: list,
                   proxy_manager: ProxyInterface,
                   session_parameter_data: bool,
                   concurrent_boundary: int,
                   create_table_query: str,
                   crawler: PredictableTunnel,
                   is_post_http_request,
                   total_count_is_available: bool = True,
                   allow_redirects = True
                   ):

    try:
        asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
        pool: Pool = Pool(1)
        conn = pool.get_connection()
        cursor = conn.cursor()
        cursor.execute(create_table_query)
        conn.commit()
        pool.release_connection(conn)
        s_pool: UnboundedSessionPool = UnboundedSessionPool(boundary=single_session_total_allowed_connections,
                                                            proxy_manager=proxy_manager)
        session_pool: SessionManager = SessionManager(err_codes=http_response_retry_error_codes,
                                                      session_pool=s_pool, param=session_parameter_data)
        asyncio.run(crawler.start(conn_pool=pool,
                          api_headers=api_headers,
                          database_insert_batch_size=database_insert_batch_size,
                          database_fields=database_fields,
                          database_table_name=database_table_name,
                          concurrent_boundary=concurrent_boundary,
                          total_count_is_available=total_count_is_available,
                          session_pool=session_pool,
                          is_post_http_request=is_post_http_request,
                          allow_redirects=allow_redirects
                        )
                    )
    except Exception as e:
        logger.critical(f'total crash\n'
                        f'{traceback.format_exc()}')


def run_without_proxy(api_headers: dict,
                   database_insert_batch_size: int,
                   database_fields: list,
                   database_table_name: str,
                   single_session_total_allowed_connections: int,
                   http_response_retry_error_codes: list,
                   session_parameter_data: bool,
                   concurrent_boundary: int,
                   create_table_query: str,
                   crawler: PredictableTunnel,
                   is_post_http_request: bool,
                   total_count_is_available: bool = True,
                   return_exceptions: List[Type[Exception]] = ()):
    try:
        asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
        pool: Pool = Pool(1)
        conn = pool.get_connection()
        cursor = conn.cursor()
        cursor.execute(create_table_query)
        conn.commit()
        pool.release_connection(conn)
        s_pool: UnboundedNoProxySessionPool = UnboundedNoProxySessionPool(boundary=single_session_total_allowed_connections)
        session_pool: NoProxySessionManager = NoProxySessionManager(err_codes=http_response_retry_error_codes,
                                                                    session_pool=s_pool,
                                                                    param=session_parameter_data,
                                                                    return_exceptions=())
        asyncio.run(crawler.start(conn_pool=pool,
                          api_headers=api_headers,
                          database_insert_batch_size=database_insert_batch_size,
                          database_fields=database_fields,
                          database_table_name=database_table_name,
                          concurrent_boundary=concurrent_boundary,
                          total_count_is_available=total_count_is_available,
                          session_pool=session_pool,
                          is_post_http_request=is_post_http_request
                        )
                    )
    except Exception as e:
        logger.critical(f'total crash\n'
                        f'{traceback.format_exc()}')

