import asyncio
import collections
import math
import time
import traceback
from typing import List, Tuple, Type

import uvloop
from MySQLdb.cursors import BaseCursor, CursorTupleRowsMixIn, CursorUseResultMixIn, SSDictCursor
from MySQLdb import Connection

import dates
from crawling_models.generic_methods import wait_threads_to_shutdown, validate_url, \
    remainder_query, upload_all_urls_to_database, get_different_listings_two_tables
from dates import TodayIso
from http_lib.session_pool import UnboundedSessionPool
from http_lib.session_pool_no_proxy import UnboundedNoProxySessionPool
from http_lib.token_manager import SessionManager, NoProxySessionManager
from parsing.seek import SeekQuery
from sql.CRUD import mysql_connection, table_exists, DATABASE_NAME
from sql.table_naming import create_table_name
from src.log import container
from sql.batch_holder import BatcHolder
from sql.pool import Pool
import http_lib.ProxyManager as proxy_manager


logger = container.get_logger(f'DetailCrawler', f'crawl.log')
DETAIL_PAGE_COLUMN = 'detail_page'



def database_split_queue(maxi: int, table_name: str, fields: list, step_size: int, id_col: str,
                         start_at: int = 0)\
        -> List[str]:
    seek = SeekQuery(table_name=table_name, maxi=maxi, fields=fields, step_size=step_size,id_name=id_col,
                     start_at=start_at)
    table_list: List[str] = []
    while True:
        if seek.is_threshold_surpassed():
            break
        table_list.append(seek.get_next_batch())
    return table_list

def create_unique_table_definition(co: Connection, cu: BaseCursor, table_name: str, select_table_name: str)\
        -> list[str]:
    database_fields: list[str] = []
    cu.execute(f"DESCRIBE {select_table_name};")
    data_base_definition = cu.fetchall()
    base_create = f"CREATE TABLE IF NOT EXISTS {table_name} ( "
    for table_column_definition in data_base_definition:
        field: str = table_column_definition[0]
        type: str = table_column_definition[1]
        nullable: str = table_column_definition[2]
        nullable_str: str = 'NOT NULL'
        if nullable == 'YES' and field != 'listing_id':
            nullable_str = 'NULL'
        base_create += f"{field} {type} {nullable_str}, "
        database_fields.append(field)
    base_create += f" INDEX (id), PRIMARY KEY (listing_id))"
    print(base_create)
    cu.execute(base_create)
    co.commit()
    return database_fields



def start_unique(target: str, select_table_name: str,
          total_url_step_size: int = 100000,
                upload_batch_size: int = 1024,
                table_name: str = None):
    # DESCRIBE propdad_serp_internal_parsed_2021w34
    c_pool: Pool = Pool(1)
    if table_name is None:
        table_name = select_table_name.replace(target, f'{target}_unique')
    database_fields: list[str]
    with mysql_connection() as co:
        with co.cursor() as cu:
            print("selecting count")
            cu.execute(f"SELECT count(*) FROM {select_table_name}")
            MAX = cu.fetchone()[0]
            if total_url_step_size > MAX:
                total_url_step_size = MAX
            # ASSUMES id and listing_id as fields
            database_fields: list[str] = create_unique_table_definition(co,cu,table_name=table_name,
                                                                        select_table_name=select_table_name,
                                                                        )
    db_uploader = BatcHolder(fields=database_fields,
                             batch_size=upload_batch_size,
                             table_name=table_name,
                             allow_duplicate=False)
    try:
        _start = time.time()
        database_split_queries: List[str] = database_split_queue(fields=['*'],
                                                                 id_col='id',
                                                                 maxi=MAX,
                                                                 step_size=total_url_step_size,
                                                                 table_name=select_table_name,
                                                                 start_at=0)
        unique_listings: set = set()
        for data_query in database_split_queries:
            print(data_query)
            conn = c_pool.get_connection()
            cursor = conn.cursor(cursorclass=SSDictCursor)
            cursor.execute(data_query)
            logger.debug("rows are being fetched")
            all_rows: tuple = cursor.fetchall()
            cursor.close()
            c_pool.release_connection(conn)
            for listing in all_rows:
                listing_id = listing['listing_id']
                if not (listing_id in unique_listings) and listing_id is not None:
                    unique_listings.add(listing_id)
                    db_uploader.insert_listing_in_database_bounded(listing=listing,
                                                                   conn_pool=c_pool)
        db_uploader.shutdown_async(c_pool, wait=True)
        c_pool.close()
        logger.debug(f"run took seconds :: {(time.time() - _start)}")

    except Exception as e:
        logger.debug(f"crash during start \n"
                     f"traceback = \n"
                     f"{traceback.format_exc()}")
        db_uploader.shutdown_async(c_pool, wait=True)
        c_pool.close()
# target = 'im24'
# week = dates.TodayIso.get()
# start_unique(target=target,select_table_name=create_table_name(target=target,
#                                                         type='serp',
#                                                         parsed=True,
#                                                         week=week)
#       )