import asyncio
import collections
import math
import traceback
from time import time
from typing import List, Callable, Union, Deque
import uvloop
from cysimdjson import JSONObject
import xml.etree.ElementTree as etree

from crawling_models.generic_methods import validate_url, logger, compute_total_count, upload_all_urls_to_database, \
    wait_threads_to_shutdown
from http_lib.session_pool_no_proxy import UnboundedNoProxySessionPool
from http_lib.ProxyManager import ProxyInterface, get_packet_stream_proxy_manager, get_packet_stream_europe_manager_random
from http_lib.session_pool import UnboundedSessionPool
from http_lib.token_manager import SessionManager, NoProxySessionManager
from sql.batch_holder import BatcHolder
from sql.pool import Pool

# Proxy Library -> Standardizing all the proxies to use.
# Proxyinteface.get_proxy()
# RetrySession -> Tracks proxy statistics, Proxy failure rate, Retries when proxy fails. Automatically retries X attempts, Maintains cookies
# RetrySession.get(url, headers)
# Session_ppol -> Pool of Retrysession. makes sure Session connects not faster than 1 second, Limits total SInglie IP usage by X calls to website.
# SESSION_POOL.GIVE_ME_SESSION.
# SQL part: Uploads to Database, creates the query, bulk upload for speed, handles multithreading.

# Crawling Model: Implement 3 functions to crawl, usage SQL part, Session_pool part.

# text_preprocessing and HTML/ JSON: Handle extracting information from HTML and text.

# Parsing library: Concurrently parses data from DB.

class WildWest:

    def get_iteration_pages(self, start: int, count: int, results_per_serp: int) -> range:
        total: int = math.ceil((count / results_per_serp)) + 1
        print(total)
        return range(start, total)

    async def descent_region(self, data, semaphore: asyncio.BoundedSemaphore,
                             session_manager: SessionManager, final_list: collections.deque,
                             cities: list, headers: dict):
        pass


    def create_initial_url_list(self) -> list:
        pass


    def create_pagination(self, item, all_urls: Deque,
                          batch_holder: BatcHolder = None) -> None:
        pass

    async def descent_regions_iteratively_non_unique(self,initial_cities: list,
                                          session_pool: SessionManager,
                                          semaphore: asyncio.BoundedSemaphore,
                                          headers: dict):
        logger.debug("descent_regions_iteratively_non_unique is called")
        stop = len(initial_cities)
        temp_cities: list = []

        final_list: collections.deque = collections.deque([])
        while stop > 0:
            for i in range(stop):
                city = initial_cities.pop()
                temp_cities.append(
                    asyncio.create_task(
                        self.descent_region(data=city, semaphore=semaphore, session_manager=session_pool,
                                            final_list=final_list, cities=initial_cities, headers=headers)
                    )
                )
            await asyncio.gather(*temp_cities)
            temp_cities = []
            stop = len(initial_cities)
        return final_list

    async def descent_regions_iteratively(self,initial_cities: list,
                                          session_pool: SessionManager,
                                          semaphore: asyncio.BoundedSemaphore,
                                          headers: dict):
        logger.debug("descent_regions_iteratively is called")
        url_set: set = set()
        stop = len(initial_cities)
        temp_cities: list = []
        final_list: collections.deque = collections.deque([])
        while stop > 0 :
            for i in range(stop):
                city = initial_cities.pop()
                if not (city.url in url_set):
                    url_set.add(city.url)
                    temp_cities.append(
                        asyncio.create_task(
                            self.descent_region(data=city, semaphore=semaphore, session_manager=session_pool,
                                                final_list=final_list, cities=initial_cities, headers=headers)
                        )
                    )
            await asyncio.gather(*temp_cities)
            temp_cities = []
            stop = len(initial_cities)
        return final_list

    def construct_and_validate_all_urls(self, final_list: Deque,
                                        invalid_urls: list) -> Deque:
        all_urls: Deque = collections.deque([])
        l_final_list: int = len(final_list)
        for i in range(l_final_list):
            item = final_list.pop()
            if not validate_url(item.url):
                invalid_urls.append(item)
            else:
                self.create_pagination(item, all_urls)
        return all_urls

    async def start(self, conn_pool: Pool,
                    api_headers: dict,
                    database_insert_batch_size: int,
                    database_fields: list,
                    database_table_name: str,
                    concurrent_boundary: int,
                    total_count_is_available: bool,
                    session_pool: SessionManager,
                    is_post_http_request: bool,
                    allow_redirects = True,
                    minimum_file_size_in_kb: int =0
                    ) -> None:
        batch_holder = BatcHolder(fields=database_fields,
                                  table_name=database_table_name,
                                  batch_size=database_insert_batch_size)
        try:
            semaphore = asyncio.BoundedSemaphore(concurrent_boundary)
            _start = time()
            initial_urls = self.create_initial_url_list()

            if is_post_http_request:
                final_list: collections.deque = await self.descent_regions_iteratively_non_unique(initial_cities=initial_urls,
                                                                                       semaphore=semaphore,
                                                                                       session_pool=session_pool,
                                                                                       headers=api_headers)
            else:
                final_list: collections.deque = await self.descent_regions_iteratively(initial_cities=initial_urls,
                                                                              semaphore=semaphore,
                                                                              session_pool=session_pool,
                                                                              headers=api_headers)
            logger.debug(f"length of final list equals {len(final_list)}")
            invalid_urls = []
            total_count: int = 0
            if total_count_is_available:
                total_count = compute_total_count(final_list)
            logger.debug(f"TOTAL COUNT = {total_count}")
            logger.debug(f"length final list = {len(final_list)}")
            all_urls = self.construct_and_validate_all_urls(final_list=final_list,
                                                                                       invalid_urls=invalid_urls)

            logger.debug(f"length of all_urls = {len(all_urls)}")
            await upload_all_urls_to_database(semaphore=semaphore,
                                              batch_holder=batch_holder,
                                              headers=api_headers,
                                              conn_pool=conn_pool,
                                              session_pool=session_pool,
                                              all_urls=all_urls,
                                              is_post_http_request=is_post_http_request,
                                              allow_redirects=allow_redirects,
                                              minimum_file_size_in_kb=minimum_file_size_in_kb
                                              )

            await wait_threads_to_shutdown(batch_holder, session_pool, conn_pool)
            logger.debug(f"run took seconds :: {(time() - _start)}\n"
                         f"run total count, available = {total_count_is_available}, count = {total_count}\n"
                         f"run connection rate :: {session_pool.get_connection_rate()},\n"
                         f"run successful connections per failure = {session_pool.successful_connections_per_failure()}\n,"
                         f"run average connection failure speed = {session_pool.get_average_error_connections_speed()}\n,"
                         f"run average connection success speed = {session_pool.get_average_success_connections_speed()}\n,"
                         f"run average connection speed = {session_pool.get_average_connection_speed()},\n"
                         f"run total_errors = {session_pool.total_errors},\n"
                         f"total_connections = {session_pool.total_connections()}")
            if len(invalid_urls) > 0:
                start_str: str = "invalid_urls below:\n"
                for d_data in invalid_urls:
                    start_str += f"invalid url = {d_data.url}\n"
        except Exception as e:
            logger.critical(f'asyncio total crash\n'
                            f'{traceback.format_exc()}')
            logger.debug(f'connection rate = {session_pool.get_connection_rate()}\n'
                         f'total connections = {session_pool.total_connections()}')
            batch_holder.shutdown_async(conn_pool, wait=True)
            await session_pool.close()
            conn_pool.close()

def run_with_proxy(api_headers: dict,
                   database_insert_batch_size: int,
                   database_fields: list,
                   database_table_name: str,
                   single_session_total_allowed_connections: int,
                   http_response_retry_error_codes: list,
                   proxy_manager: ProxyInterface,
                   session_parameter_data: bool,
                   concurrent_boundary: int,
                   create_table_query: str,
                   crawler: WildWest,
                   is_post_http_request,
                   total_count_is_available: bool = True,
                   allow_redirects = True,
                   handle_success: Callable[[bytes], Union[JSONObject, etree.Element]] = None,
                   minimum_file_size_in_kb = 0
                   ):

    try:
        asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
        pool: Pool = Pool(1)
        conn = pool.get_connection()
        cursor = conn.cursor()
        cursor.execute(create_table_query)
        conn.commit()
        pool.release_connection(conn)
        s_pool: UnboundedSessionPool = UnboundedSessionPool(boundary=single_session_total_allowed_connections,
                                                            proxy_manager=proxy_manager)
        session_pool: SessionManager = SessionManager(err_codes=http_response_retry_error_codes,
                                                      session_pool=s_pool, param=session_parameter_data,
                                                      handle_success=handle_success)
        asyncio.run(crawler.start(conn_pool=pool,
                          api_headers=api_headers,
                          database_insert_batch_size=database_insert_batch_size,
                          database_fields=database_fields,
                          database_table_name=database_table_name,
                          concurrent_boundary=concurrent_boundary,
                          total_count_is_available=total_count_is_available,
                          session_pool=session_pool,
                          is_post_http_request=is_post_http_request,
                          allow_redirects=allow_redirects,
                          minimum_file_size_in_kb=minimum_file_size_in_kb
                        )
                    )
    except Exception as e:
        logger.critical(f'total crash\n'
                        f'{traceback.format_exc()}')


def run_without_proxy(api_headers: dict,
                   database_insert_batch_size: int,
                   database_fields: list,
                   database_table_name: str,
                   single_session_total_allowed_connections: int,
                   http_response_retry_error_codes: list,
                   session_parameter_data: bool,
                   concurrent_boundary: int,
                   create_table_query: str,
                   crawler: WildWest,
                   is_post_http_request: bool,
                   total_count_is_available: bool = True):
    try:
        asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
        pool: Pool = Pool(1)
        conn = pool.get_connection()
        cursor = conn.cursor()
        cursor.execute(create_table_query)
        conn.commit()
        pool.release_connection(conn)
        s_pool: UnboundedNoProxySessionPool = UnboundedNoProxySessionPool(boundary=single_session_total_allowed_connections)
        session_pool: NoProxySessionManager = NoProxySessionManager(err_codes=http_response_retry_error_codes,
                                                                    session_pool=s_pool,
                                                                    param=session_parameter_data)
        asyncio.run(crawler.start(conn_pool=pool,
                          api_headers=api_headers,
                          database_insert_batch_size=database_insert_batch_size,
                          database_fields=database_fields,
                          database_table_name=database_table_name,
                          concurrent_boundary=concurrent_boundary,
                          total_count_is_available=total_count_is_available,
                          session_pool=session_pool,
                          is_post_http_request=is_post_http_request
                        )
                    )
    except Exception as e:
        logger.critical(f'total crash\n'
                        f'{traceback.format_exc()}')


def run_pre_setup_packet_stream(api_headers,
                               database_fields: list,
                               database_table_name: str,
                               http_response_retry_error_codes: list,
                               create_table_query: str,
                               crawler: WildWest,
                               is_post_http_request: bool,
                               total_count_is_available: bool = True,
                               session_parameter_data: bool = False
                               ):

    # proxy_manager: ProxyInterface =  get_packet_stream_proxy_manager()
    proxy_manager: ProxyInterface = get_packet_stream_europe_manager_random()
    database_insert_batch_size = 128
    single_session_total_allowed_connections = 100
    concurrent_boundary = 150
    run_with_proxy(api_headers=api_headers,
                   database_insert_batch_size=database_insert_batch_size,
                   database_fields=database_fields,
                   database_table_name=database_table_name,
                   concurrent_boundary=concurrent_boundary,
                   create_table_query=create_table_query,
                   total_count_is_available=total_count_is_available,
                   session_parameter_data=session_parameter_data,
                   http_response_retry_error_codes=http_response_retry_error_codes,
                   single_session_total_allowed_connections=single_session_total_allowed_connections,
                   proxy_manager=proxy_manager,
                   crawler=crawler,
                   is_post_http_request=is_post_http_request)