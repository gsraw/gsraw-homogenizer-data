from typing import Tuple

class FixedCrawlData:

    def append_to_list(self, append_to_list: list):
        for field in self.__slots__:
            append_to_list.append(self.__getattribute__(field))
        # print(append_to_list)

    def get_field_list(self, append_to_list: list):
        for field in self.__slots__:
            # print(field)
            append_to_list.append(field)

    def get_test_values(self,  append_to_list: list):
        for field in self.__slots__:
            # print(f"field fixed = {field}")
            append_to_list.append((field, self.__getattribute__(field)))


class CrawlDataModel:
    __slots__ = ('c_parse_date', 'data_bytes', 'url', 'response_status', 'fixed')
    # excludes 'fixed'
    fields: Tuple[str] = ('c_parse_date', 'data_bytes',  'url', 'response_status')
    c_parse_date: str
    data_bytes: bytes
    url: str
    response_status: int
    fixed: FixedCrawlData

    def __init__(self, url: str, fixed: FixedCrawlData):
        self.url = url
        self.c_parse_date = ''
        self.data_bytes = b''
        self.fixed = fixed
        self.response_status = -1

    def get_new(self):
        pass

    def _append_data_model_fields(self, append_to_list: list):
        self.fixed.append_to_list(append_to_list)
        for f in self.fields:
            append_to_list.append((f,self.__getattribute__(f)),)
        for field in self.__slots__:
            append_to_list.append((field,self.__getattribute__(field)),)
        print(append_to_list)

    def append_to_list(self, append_to_list: list):
        self.fixed.append_to_list(append_to_list)
        for f in self.fields:
            append_to_list.append(self.__getattribute__(f))
        for field in self.__slots__:
            append_to_list.append(self.__getattribute__(field))
        # print(append_to_list)

    def get_field_list(self, append_to_list: list) -> None:
        self.fixed.get_field_list(append_to_list)
        for f in self.fields:
            append_to_list.append(f)
        for f_2 in self.__slots__:
            append_to_list.append(f_2)


class IndependentVariable:
    __slots__ = ('fixed',)
    fields: tuple = ()
    fixed: FixedCrawlData

    def __init__(self, fixed: FixedCrawlData):
        self.fixed = fixed

    def append_to_list(self, append_to_list: list):
        self.fixed.append_to_list(append_to_list)
        for f in self.fields:
            append_to_list.append(self.__getattribute__(f))
        for field in self.__slots__:
            append_to_list.append(self.__getattribute__(field))
        # print(append_to_list)

    def get_field_list(self, append_to_list: list) -> None:
        self.fixed.get_field_list(append_to_list)
        for f in self.fields:
            append_to_list.append(f)
        for f_2 in self.__slots__:
            append_to_list.append(f_2)

    def get_new(self):
        pass

    def get_test_values(self, append_to_list: list):
        self.fixed.get_test_values(append_to_list)
        for f in self.fields:
            # print(f"field = {f}")
            append_to_list.append((f, self.__getattribute__(f),))
        for field in self.__slots__:
            # print(f"field = {field}")
            append_to_list.append((field, self.__getattribute__(field)))
