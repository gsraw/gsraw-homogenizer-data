import datetime
import re
import calendar
from typing import List, Tuple

import numpy as np


class TodayIso:
    def __init__(self):
        today = datetime.date.today()
        self.year, self.week_num, self.day_of_week = today.isocalendar()

    @staticmethod
    def get() -> str:
        year, week_num, day_of_week = datetime.date.today().isocalendar()
        return str(year) + "w" + str(week_num)

    @staticmethod
    def get_previous_week(iso_daybat_format: str):
        arr = iso_daybat_format.split('w')
        year = int(arr[0])
        week = int(arr[1])
        curr_week = datetime.date.fromisocalendar(year, week, 1)
        week_monday = datetime.date.fromisocalendar(year, week, 1).weekday()
        start_delta = datetime.timedelta(days=week_monday, weeks=1)
        previous_week = curr_week - start_delta
        year, week_num, day_of_week = previous_week.isocalendar()
        return str(year) + "w" + str(week_num)

    @staticmethod
    def get_previous_week_table_name(table_name: str):
        curr_week_daybat_format: str = get_iso_daybat_format_from_table_name(table_name)
        previous_week_daybat_format: str = TodayIso.get_previous_week(curr_week_daybat_format)
        return table_name.replace(curr_week_daybat_format,previous_week_daybat_format)


def get_now():
    now = datetime.datetime.now()
    return now.strftime("%Y/%m/%d %H:%M:%S")


def utc_integer_time_to_readable_date(time_slot: int) -> str:
    return datetime.datetime.utcfromtimestamp(time_slot).strftime('%Y-%m-%d %H:%M:%S')


def get_iso_daybat_format_from_table_name(table_name: str) -> str:
    return re.findall(r'\d+[w]\d+', table_name)[0]


def get_week_of_month(year, month, day):
    x = np.array(calendar.monthcalendar(year, month))
    week_of_month = np.where(x==day)[0][0] + 1
    return(week_of_month)


def get_random_days_month_of_each_week(year, month) -> List[Tuple[int, int]]:
    """EXCLUDES FINAL DAY OF MONTH"""
    x = np.array(calendar.monthcalendar(year, month))
    days = []
    x_len = len(x)
    x_l = x_len - 1
    for i in range(0, x_len):
        y = x[i]
        y = y[y != 0]
        if i == x_l:
            y = y[0:len(y)-1]
        day = np.random.choice(y)
        days.append((i + 1, day),)
    return days

# get_random_days_month_of_each_week(2021,12)

