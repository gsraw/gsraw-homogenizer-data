import math
import itertools
import copy
from typing import Tuple


def ceil_two_decimals(num: float) -> float:
    return math.ceil(num * 100) / 100


def floor_two_decimals(num: float) -> float:
    return math.floor(num * 100) / 100


def split_integer_interval(lower_and_upper: tuple) -> Tuple[Tuple[int,int], Tuple[int,int]]:
    lower,upper = lower_and_upper
    diff = upper - lower
    assert diff > 0
    if diff == 1:
        return (lower, lower), (upper, upper)
    n_bound = math.floor ( (upper + lower) / 2)
    return (lower, n_bound), (n_bound, upper)



def split_interval(lower_and_upper: tuple):
    lower, upper = lower_and_upper
    max_min = upper - lower
    if max_min <= 0.011:
        difference = floor_two_decimals(max_min)
        print(f"difference = {difference}")
        if difference <= 0.01:
            return (lower, lower), (upper, upper)
    n_bound = (lower + upper) / 2
    print(f"n_bound before decimal = {n_bound}")
    n_bound_2 = floor_two_decimals(n_bound)
    print(f"n_bound = {n_bound_2}")
    if n_bound_2 == lower:
        n_bound_2 = ceil_two_decimals(n_bound)
    return (lower, n_bound_2), (n_bound_2, upper)


def create_filters(list_of_lists: list) -> list:
    return list(itertools.product(*list_of_lists))


def add_filter(list_of_lists: list, filter_list: list):
    new_filters = []
    for element in list_of_lists:
        for new_filter in filter_list:
            old_filters = copy.deepcopy(element)
            old_filters.append(new_filter)
            new_filters.append(old_filters)
    return new_filters


class FilterContainer:
    def __init__(self,filter_type: str,filter_id):
        self.filter_type = filter_type
        self.filter_id = filter_id

    @staticmethod
    def filters_to_param(filter_container_list: list):
        parameters = {}
        for container in filter_container_list:
            parameters[container.filter_type] = container.filter_id


