import time
from typing import Callable, Dict, Tuple, Any, List
from lxml.etree import iterparse, _Element
from src.log import container
from data_model.crawling_data_model import IndependentVariable
from html_functionality.html_local import html_iterator
from sql.batch_holder import BatcHolder
from sql.pool import Pool

logger = container.get_logger('parse', 'parse.log')

def clear_tree(elem: _Element) -> None:
    """infinite lop in HMT html."""
    elem.clear()
    for ancestor in elem.xpath('ancestor-or-self::*'):
        while ancestor.getprevious() is not None:
            del ancestor.getparent()[0]


def _listings_parser(context: iterparse, list_element_criterion: Callable[[_Element], bool],
                     list_element_parser:  Callable[[_Element, IndependentVariable],None],
                     sub_list_elements_parser: Callable[[_Element, IndependentVariable],None],
                     independent_listing_variables: IndependentVariable,
                     ) -> IndependentVariable:
    """assumes event == t_start and list_element_criterion(elem):"""
    t_end = 'end'
    listing: IndependentVariable = independent_listing_variables.get_new()
    event, elem = next(context)
    while not (event == t_end and list_element_criterion(elem)):
        if event == t_end:
            sub_list_elements_parser(elem, listing)
        event, elem = next(context)
    list_element_parser(elem, listing)
    return listing


def speedy_iterative_parser(context: iterparse,
                            main_element_criterion: Callable[[_Element], bool],
                            list_element_criterion: Callable[[_Element], bool],
                            list_element_parser: Callable[[_Element, IndependentVariable],None],
                            sub_list_elements_parser: Callable[[_Element, IndependentVariable],None],
                            batch_holder: BatcHolder,
                            conn_pool: Pool,
                            independent_listing_variables: IndependentVariable
                            ) -> None:
    """context should have both 'start' and 'end' as events."""
    _start = time.time()
    t_start = 'start'
    t_end = 'end'
    count = 0
    while True:
        try:
            event, elem = next(context)
            # print(f"event={event},elem.tag={elem.tag}")
            if event == t_start and main_element_criterion(elem):
                event, elem = next(context)
                while not (event == t_end and main_element_criterion(elem)):
                    # print('found_main')
                    if event == t_start and list_element_criterion(elem):
                        # print('found listing')
                        count += 1
                        batch_holder.insert_data_model_in_database_bounded(
                            _listings_parser(context=context,
                                             list_element_criterion=list_element_criterion,
                                             list_element_parser=list_element_parser,
                                             sub_list_elements_parser=sub_list_elements_parser,
                                             independent_listing_variables=independent_listing_variables)
                                                                           , conn_pool)
                    event, elem = next(context)
                # clear_tree(elem=elem)
                break
        except StopIteration:
            logger.debug("stopped iteration by StopIteration Exception")
            break
    del context
    logger.debug(f"time :: {(time.time() - _start)}")
    logger.debug(f"count = {count}")

def test_speedy_iterative_parser(context: iterparse,
                            main_element_criterion: Callable[[_Element], bool],
                            list_element_criterion: Callable[[_Element], bool],
                            list_element_parser: Callable[[_Element, IndependentVariable],None],
                            sub_list_elements_parser: Callable[[_Element, IndependentVariable],None],
                            independent_listing_variables: IndependentVariable
                            ) -> None:
    """context should have both 'start' and 'end' as events."""
    _start = time.time()
    t_start = 'start'
    t_end = 'end'
    count = 1
    while True:
        try:
            event, elem = next(context)
            # print(f"event={event},elem.tag={elem.tag}")
            if event == t_start and main_element_criterion(elem):
                event, elem = next(context)
                while not (event == t_end and main_element_criterion(elem)):
                    if event == t_start and list_element_criterion(elem):
                        print('found listing')
                        listing = _listings_parser(context=context,
                                         list_element_criterion=list_element_criterion,
                                         list_element_parser=list_element_parser,
                                         sub_list_elements_parser=sub_list_elements_parser,
                                         independent_listing_variables=independent_listing_variables)

                        test_list = []
                        listing.get_test_values(test_list)
                        for item in test_list:
                            if item[0] != 'fixed' or item[0] != 'fixed':
                                print(f"{count},{item[0]} = {item[1]}")
                        count += 1
                    event, elem = next(context)
                # clear_tree(elem=elem)
                break
        except StopIteration:
            print("stopped iteration by StopIteration Exception")
            break
    del context
    print(f"time :: {(time.time() - _start)}")
    time.sleep(5)


def start_iterative_parsing(html: bytes,
                            main_element_criterion: Callable[[_Element], bool],
                            list_element_criterion: Callable[[_Element], bool],
                            list_element_parser: Callable[[_Element, dict],None],
                            sub_list_elements_parser: Callable[[_Element, dict], None],
                            all_listings: list[Dict],
                            independent_listing_variables: IndependentVariable
                            ) -> None:
    speedy_iterative_parser(context=html_iterator(html,events=('start','end')),
                            main_element_criterion=main_element_criterion,
                            list_element_criterion=list_element_criterion,
                            list_element_parser=list_element_parser,
                            sub_list_elements_parser=sub_list_elements_parser,
                            all_listings=all_listings,
                            independent_listing_variables=independent_listing_variables)


