import time
from io import StringIO, BytesIO
from typing import Dict, Any, Union
import lxml
from bs4 import BeautifulSoup
from lxml.cssselect import CSSSelector
from lxml import html
from lxml import etree
from lxml.etree import iterparse, _Element
from collections.abc import Callable

def has_attr(item : BeautifulSoup, attr : str):
    if item.has_attr(attr):
        return item[attr]
    return None


def has_text_css(item : BeautifulSoup, css_select : str):
    result = item.select(css_select)
    if len(result) > 0:
        return result[0].getText()
    return None

def has_inner_text_css(item : BeautifulSoup, css_select : str):
    result = item.select(css_select)
    if len(result) > 0:
        return result[0].text
    return None


def has_attribute_css(item : BeautifulSoup,css_select : str,attr : str):
    result = item.select(css_select)
    if len(result) > 0:
        return has_attr(result[0],attr)
    return None

def has_single_result_css(item : BeautifulSoup,css_select : str):
    result = item.select(css_select)
    if len(result) > 0:
        return result[0]


def read_into_html(html_str : str) -> etree.Element:
    return html.fromstring(html_str)


def read_into_html_bytes(html_bytes: bytes, encoding='utf-8') -> lxml.html.Element:
    # parser = etree.HTMLParser(encoding=encoding)
    # # get_root()
    # return etree.parse(BytesIO(html_bytes), parser)
    return html.fromstring(html_bytes.decode(encoding))


def lxml_el_to_bytes_to_string(el: _Element) -> str:
    # return html.unescape(etree.tostring(el).decode())
    return etree.tostring(el,encoding='UTF-8').decode('utf-8')


def unused_fast_iter(context,func,tag,attribute):
    for event, elem in context:
        if event == 'start' and elem.tag == 'section' and elem.get('class') == 'list-items':
            print("this works")
            test = elem.xpath('//article[contains(@data-href,"habitaclia")]')
            test2 = elem.iterfind(tag='article')
            print(lxml_el_to_bytes_to_string(elem))
            print(len(test))
            print(len(test2))
            # func(context, elem,)
        elem.clear()
        while elem.getprevious() is not None:
            del elem.getparent()[0]
    del context


def try_get(elem: html, tag: str):
    try:
        return elem.get(tag)
    except Exception as e:
        return None

def try_get_attrib(elem: _Element, tag: str):
    if tag in elem.attrib:
        return elem.get(tag)
    return None
    # try:
    #     return elem.get(tag)
    # except Exception as e:
    #     return None


def text_content(elem: _Element) -> str:
    return ''.join(elem.itertext())


def try_get_and_compare_attribute(elem: _Element, tag: str, compare: Union[str, int]) -> bool:
    """Compare can be assumed to be either int or str"""
    if tag in elem.attrib:
        return elem.get(tag) == compare
    return False


def contains_attribute(elem: _Element, attribute: str):
    return attribute in elem.attrib


def try_get_and_contains_attribute(elem: _Element, tag: str, contains: str) -> bool:
    # print(f'tag={tag},attrib={elem.attrib},try={try_get(elem,tag)}')
    if tag in elem.attrib:
        # print('True')
        return contains in elem.get(tag)
    return False


def clear_tree(elem: _Element) -> None:
    elem.clear()
    for ancestor in elem.xpath('ancestor-or-self::*'):
        while ancestor.getprevious() is not None:
            del ancestor.getparent()[0]


def fast_iter(context: iterparse,
              _continue_func: Callable[[str, _Element, iterparse, list[Dict]], None],
              eval_func: Callable[[str, _Element], bool]) -> None:
    print("fast iter called")
    _start = time.time()
    all_listings: list[Dict] = []
    while True:
        try:
            event, elem = next(context)
            # print(f"event={event},elem.tag={elem.tag}")
            if eval_func(event, elem):
                event, elem = next(context)
                while not eval_func(event, elem):
                    _continue_func(event,elem,context, all_listings)
                    event, elem = next(context)
                print(all_listings)
                clear_tree(elem=elem)
                break
        except StopIteration:
            print("stopped iteration")
            break
    del context
    print(f"time :: {(time.time() - _start)}")
    time.sleep(90)


def fast_iter_2(context, func, *args, **kwargs):
    """
    http://lxml.de/parsing.html#modifying-the-tree
    Based on Liza Daly's fast_iter
    http://www.ibm.com/developerworks/xml/library/x-hiperfparse/
    See also http://effbot.org/zone/element-iterparse.htm
    """
    for event, elem in context:
        func(elem, *args, **kwargs)
        # It's safe to call clear() here because no descendants will be
        # accessed
        elem.clear()
        # Also eliminate now-empty references from the root node to elem
        for ancestor in elem.xpath('ancestor-or-self::*'):
            while ancestor.getprevious() is not None:
                del ancestor.getparent()[0]
    del context


def html_iterator(html: bytes, events: tuple) -> iterparse:
    """events = 'start' or 'end' or both"""
    return etree.iterparse(BytesIO(html), html=True, events=events)