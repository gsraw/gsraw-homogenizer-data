import string
from typing import List

import src.http_lib.proxies as proxy
import src.http_lib.countries as region
import random

def packet_stream_test_curl(country: region.PacketStreamCountries):
    h_pw = 'unai.bastida:xiQizjQBeU8Vubtf'
    N = 8
    session = ''.join(random.choice(string.ascii_uppercase + string.digits + string.ascii_lowercase) for _ in range(N))
    return f'http://{h_pw}_country-{country.value}_session-{session}@proxy.packetstream.io:31112'

def packet_stream_sticky(country: region.PacketStreamCountries):
    h_pw = 'unai.bastida:xiQizjQBeU8Vubtf'
    N = 8
    session = ''.join(random.choice(string.ascii_uppercase + string.digits + string.ascii_lowercase) for _ in range(N))
    return f'http://{h_pw}_country-{country.value}_session-{session}@proxy.packetstream.io:31112'

def packet_stream_random(country: region.PacketStreamCountries):
    h_pw = 'unai.bastida:xiQizjQBeU8Vubtf'
    return f'http://{h_pw}_country-{country.value}@proxy.packetstream.io:31112'

# print(f"curl -v -x {packet_stream_test_curl(region.PacketStreamCountries.spain)} https://www.idealista.com")


def get_packet_stream_proxy(zone: proxy.BrightDataZones = None):
    return 'http://unai.bastida:xiQizjQBeU8Vubtf@proxy.packetstream.io:31112'


def get_random_session() -> int:
    return random.randint(10000, 1000000)

def get_static_bright_data_proxy(zone: proxy.BrightDataZones):
    username = f'lum-customer-unai_bastida-zone-{zone.value}'
    password = 'ttahrplmmnmq'
    # password = 'g34xruei0uab'
    port = 22225
    session = get_random_session()
    super_proxy = 'zproxy.lum-superproxy.io'
    return f'http://{username}-session-{session}:{password}@{super_proxy}:{port}'


def get_bright_data_proxy_country(country: region.Countries, zone: proxy.BrightDataZones):
    username = f'lum-customer-unai_bastida-zone-{zone.value}'
    password = 'ttahrplmmnmq'
    port = 22225
    session = get_random_session()
    super_proxy = 'zproxy.lum-superproxy.io'
    return f'http://{username}-country-{country.value}-session-{session}:{password}@{super_proxy}:{port}'


def test_bright_data_proxy_country(country: region.Countries, zone: proxy.BrightDataZones,session: int):
    # username = f'lum-customer-c_3f203dd3-{zone.value}'
    username = f'lum-customer-unai_bastida-zone-{zone.value}'
    password = 'z9aen2zzi567'
    port = 22225
    super_proxy = 'zproxy.lum-superproxy.io'
    return f'http://{username}-country-{country.value}-session-{session}:{password}@{super_proxy}:{port}'


class ProxyInterface:

    def get_proxy(self):
        pass


class ProxyFactory:

    @staticmethod
    def get(p_type: proxy.Proxies, zone: proxy.BrightDataZones, country: region.Countries = None):
        if country is not None:
            print('isCountry')
            return CountryProxy(p_type, country, zone=zone)
        print("isRandomProxy")
        return RandomProxy(p_type, zone=zone)

    @staticmethod
    def get_region(country: tuple[region.PacketStreamCountries, ...], random: bool):
        if random:
            return MultipleCountyProxyRandom(country)
        return MultipleCountryProxy(country)


class RandomProxy(ProxyInterface):
    random_switch = {
        proxy.Proxies.bright_data.value: get_static_bright_data_proxy,
        proxy.Proxies.packet_stream.value: get_packet_stream_proxy
    }

    def __init__(self, p_type: proxy.Proxies, zone: proxy.BrightDataZones):
        self.proxy = self.random_switch.get(p_type.value)
        self.zone = zone

    def get_proxy(self):
        return self.proxy(self.zone)


class CountryProxy(ProxyInterface):
    country_switch = {
        proxy.Proxies.bright_data.value: get_bright_data_proxy_country,
    }

    def __init__(self, p_type: proxy.Proxies, country: region.Countries, zone: proxy.BrightDataZones):
        self.country = country
        self.zone = zone
        self.proxy = self.country_switch.get(p_type.value)

    def get_proxy(self):
        return self.proxy(self.country, zone=self.zone)


class MultipleCountryProxy(ProxyInterface):

    def __init__(self, country: tuple[region.PacketStreamCountries]):
        self.country = country

    def get_proxy(self):
        d = len(self.country) - 1
        return packet_stream_sticky(self.country[random.randint(0,d)])


class MultipleCountyProxyRandom(ProxyInterface):
    def __init__(self, country: tuple[region.PacketStreamCountries]):
        self.country = country

    def get_proxy(self):
        d = len(self.country) - 1
        return  packet_stream_random(self.country[random.randint(0, d)])


class MultipleCountryProxyBrightData(ProxyInterface):
    def __init__(self, country: tuple[region.Countries, ...]):
        self.country = country

    def get_proxy(self):
        d = len(self.country) - 1
        # random_session: int = get_random_session()
        return get_bright_data_proxy_country(self.country[random.randint(0, d)],
                                              zone=proxy.BrightDataZones.data_center)



def get_brightdata_random_proxy_manager() -> ProxyInterface:
    return ProxyFactory.get(p_type=proxy.Proxies.bright_data, zone=proxy.BrightDataZones.data_center,
                            country=None)


def get_packet_stream_proxy_manager() -> ProxyInterface:
    return ProxyFactory.get(p_type=proxy.Proxies.packet_stream, zone=None)


def get_packet_stream_europe_manager() -> ProxyInterface:
    return ProxyFactory.get_region(
        (
            region.PacketStreamCountries.italy,
            region.PacketStreamCountries.germany,
            region.PacketStreamCountries.netherlands,
            region.PacketStreamCountries.france,
            region.PacketStreamCountries.finland,
            region.PacketStreamCountries.austria,
            region.PacketStreamCountries.belgium,
            region.PacketStreamCountries.united_kingdom,
            region.PacketStreamCountries.sweden,
            region.PacketStreamCountries.norway,
            region.PacketStreamCountries.denmark,
        ), random=False
    )


def get_packet_stream_europe_manager_random() -> ProxyInterface:
    return ProxyFactory.get_region(
        (
            region.PacketStreamCountries.italy,
            region.PacketStreamCountries.germany,
            region.PacketStreamCountries.netherlands,
            region.PacketStreamCountries.france,
            region.PacketStreamCountries.finland,
            region.PacketStreamCountries.austria,
            region.PacketStreamCountries.belgium,
            region.PacketStreamCountries.united_kingdom,
            region.PacketStreamCountries.sweden,
            region.PacketStreamCountries.norway,
            region.PacketStreamCountries.denmark,
        ), random=True
    )

def get_packet_stream_us_sticky() -> ProxyInterface:
    return ProxyFactory.get_region(
        (
            region.PacketStreamCountries.united_states,
        ), random=False
    )

def get_packet_stream_brazil_sticky() -> ProxyInterface:
    return ProxyFactory.get_region(
        (
            region.PacketStreamCountries.brazil,
        ), random=False
    )

def get_packet_stream_argentinia_sticky() -> ProxyInterface:
    return ProxyFactory.get_region(
        (
            region.PacketStreamCountries.argentina,
        ), random=False
    )

def get_packet_stream_china_sticky() -> ProxyInterface:
    return ProxyFactory.get_region(
        (
            region.PacketStreamCountries.china,
        ), random=False
    )

def get_packet_stream_canada_sticky() -> ProxyInterface:
    return ProxyFactory.get_region(
        (
            region.PacketStreamCountries.canada,
        ), random=False
    )

def get_packet_stream_india_sticky() -> ProxyInterface:
    return ProxyFactory.get_region(
        (
            region.PacketStreamCountries.india,
        ), random=False
    )

def get_packet_stream_indonesia_sticky() -> ProxyInterface:
    return ProxyFactory.get_region(
        (
            region.PacketStreamCountries.indonesia,
        ), random=False
    )


def get_packet_stream_vietnam_sticky() -> ProxyInterface:
    return ProxyFactory.get_region(
        (
            region.PacketStreamCountries.vietnam,
        ), random=False
    )


def get_packet_stream_japan_sticky() -> ProxyInterface:
    return ProxyFactory.get_region(
        (
            region.PacketStreamCountries.japan,
        ), random=False
    )

def get_packet_stream_thailand_sticky() -> ProxyInterface:
    return ProxyFactory.get_region(
        (
            region.PacketStreamCountries.thailand,
        ), random=False
    )

def get_packet_stream_turkey_sticky() -> ProxyInterface:
    return ProxyFactory.get_region(
        (
            region.PacketStreamCountries.turkey,
        ), random=False
    )

def get_packet_stream_malaysia_sticky() -> ProxyInterface:
    return ProxyFactory.get_region(
        (
            region.PacketStreamCountries.malaysia,
        ), random=False
    )

def get_packet_stream_iran_sticky() -> ProxyInterface:
    return ProxyFactory.get_region(
        (
            region.PacketStreamCountries.iran,
        ), random=False
    )


def get_packet_stream_australia_sticky() -> ProxyInterface:
    return ProxyFactory.get_region(
        (
            region.PacketStreamCountries.australia,
        ), random=False
    )


def get_packet_stream_czechia_sticky() -> ProxyInterface:
    return ProxyFactory.get_region(
        (
            region.PacketStreamCountries.czechia,
        ), random=False
    )

def get_packet_stream_israel_sticky() -> ProxyInterface:
    return ProxyFactory.get_region(
        (
            region.PacketStreamCountries.israel,
        ), random=False
    )

def get_packet_stream_estonia_sticky() -> ProxyInterface:
    return ProxyFactory.get_region(
        (
            region.PacketStreamCountries.estonia ,
        ), random=False
    )

def get_packet_stream_greece_sticky() -> ProxyInterface:
    return ProxyFactory.get_region(
        (
            region.PacketStreamCountries.greece ,
        ), random=False
    )

def get_packet_stream_afghanistan_sticky() -> ProxyInterface:
    return ProxyFactory.get_region(
        (
            region.PacketStreamCountries.afghanistan,
        ), random=False
    )

def get_bright_data_residential_europe_manager() -> ProxyInterface:
    return MultipleCountryProxyBrightData(
        ( region.Countries.france,
        region.Countries.spain,
        )
    )

def get_bright_data_japan() -> ProxyInterface:
    return MultipleCountryProxyBrightData(
        (region.Countries.japan,
         )
    )

def get_bright_data_im24_web_unlocker() -> ProxyInterface:
    return ProxyFactory.get(p_type=proxy.Proxies.bright_data,
                            zone=proxy.BrightDataZones.web_unlocker_im24)