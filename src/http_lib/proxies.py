from enum import Enum


class Proxies(Enum):
    bright_data = 0
    packet_stream = 1


class TypeProxy(Enum):
    residential = 0
    data_center = 1


class BrightDataZones(Enum):
    data_center = 'static'
    residential = 'residential'
    web_unlocker_im24 = 'in24'
