import traceback
import asyncio
import time
from http_lib.session_pool_no_proxy import NoProxyState
from src.http_lib.ProxyManager import ProxyInterface
from src.log import container

c_logger = container.get_logger('crawl', 'crawl_session_pool.log')


class State(NoProxyState):
    def __init__(self, proxy_manager: ProxyInterface):
        super(State, self).__init__()
        self.proxy_manager = proxy_manager
        self.proxy = proxy_manager.get_proxy()

    def reset_proxy(self):
        self.proxy = self.proxy_manager.get_proxy()


class UnboundedSessionPool:
    def __init__(self, boundary: int, proxy_manager: ProxyInterface):
        self._available = []
        self._state = {}
        self.proxy_manager = proxy_manager
        self.boundary = boundary

    async def get_session(self) -> State:
        len_s = len(self._available)
        new_time = time.time()
        if len_s > 0:
            check = new_time - self._available[0].time
            if check > 1.0:
                new_state = self._available.pop(0)
                return new_state
        new_state = await self.create_new_token_state()
        self._state[new_state] = new_state
        return new_state

    async def close_token(self, st: State):
        try:
            await st.close()
        except Exception as e:
            c_logger.debug(str(e))
        try:
            del self._state[st]
        except Exception as e:
            c_logger.debug(str(e))


    async def put_back(self, st: State):
        st.update_state()
        if st.past_bound(self.boundary):
            await self.close_token(st)
        else:
            self._available.append(st)

    async def close(self):
        for key in self._state:
            try:
                await self._state[key].close()
            except Exception as e:
                c_logger.debug(str(e))

    async def create_new_token_state(self) -> State:
        """ Override it"""
        return State(proxy_manager=self.proxy_manager)


class TokenState(State):
    def __init__(self, proxy_manager: ProxyInterface):
        """

        :type proxy_manager: a ProxyInterface object created by ProxyFactory from ProxyManager.py
        """
        super().__init__(proxy_manager=proxy_manager)

    async def get_csrf_token(self, url: str, err_codes: list, headers: dict, pars_func) -> list:
        i = 0
        sleep = 0.03333333333
        max_tries = 100
        while i < max_tries:
            try:
                response = await self.session.get(url, headers=headers, proxy=self.proxy, allow_redirects=True)
                if response.status in err_codes:
                    c_logger.error(f"status_code = {response.status} with url {url}\nHTTP count is {i}")
                    await self.reset_csrf_proxy_ip()
                    if response.status == 503 or response.status == 504:
                        await asyncio.sleep(3)
                    await asyncio.sleep(sleep)
                    i += 1
                else:
                    self.tokens = await pars_func(response)
                    return self.tokens
            except Exception as e:
                tb = traceback.format_exc()
                c_logger.error(f"HTTP EXCEPTION URL {url}\nHTTP exception occurred:\n {tb}\nHTTP count is {i}")
                await self.reset_csrf_proxy_ip()
                i = i + 1
                await asyncio.sleep(sleep)
        c_logger.critical(f"{max_tries} times failure")
        raise Exception(f"{max_tries} times failure")

    async def reset_csrf_proxy_ip(self):
        self.reset_proxy()
        await self.reset_session()


class UnboundedSessionTokenPool(UnboundedSessionPool):
    def __init__(self, boundary: int, url: str,
                 err_codes: list, headers: dict, pars_func, proxy_manager: ProxyInterface):
        super().__init__(boundary=boundary, proxy_manager=proxy_manager)
        self.csrf_url = url
        self.err_codes = err_codes
        self.headers = headers
        self.pars_func = pars_func

    async def create_session(self):
        new_state = await self.create_new_token_state()
        self._state[new_state] = new_state
        self._available.append(new_state)

    async def create_new_token_state(self) -> State:
        """Overrides to handle token"""
        new_state = TokenState(proxy_manager=self.proxy_manager)
        await new_state.get_csrf_token(url=self.csrf_url,
                                       headers=self.headers,
                                       err_codes=self.err_codes,
                                       pars_func=self.pars_func)
        return new_state

    # async def put_back(self, st : TokenState):
    #     st.update_state()
    #     # print(f"len of state {len(self._state)}")
    #     # print(f"len of available {len(self._available)}")
    #     # print(f"self.state {self._state[st]}")
    #     # print(f"st count = {st.count}")
    #     if st.past_bound(self.boundary):
    #         # print("closing state ")
    #         del self._state[st]
    #         # print(f"len of state after closed {len(self._state)}")
    #         try:
    #             await st.close()
    #         except Exception as e:
    #             print(e.body)
    #     else:
    #         self._available.append(st)
