import traceback
import asyncio
import aiohttp
import time
from http_lib.token import Tokens
from src.log import container

c_logger = container.get_logger('crawl','crawl.log')

# TODO webunlocker connect verify_ssl=False
class NoProxyState:
    def __init__(self):
        self.count = 0
        self.time = time.time()
        self.session = aiohttp.ClientSession(connector=aiohttp.TCPConnector(verify_ssl=False))
        self.tokens: Tokens = None

    def _reset_time(self):
        self.time = time.time()

    def _increment(self):
        self.count += 1

    def update_state(self):
        self._reset_time()
        self._increment()

    def past_bound(self, boundary) -> bool:
        return self.count > boundary

    async def close(self):
        try:
            await self.session.close()
        except Exception as c:
            pass

    def get_token(self) -> Tokens:
        return self.tokens

    async def reset_session(self):
        await self.close()
        self.session = aiohttp.ClientSession()


class UnboundedNoProxySessionPool:
    def __init__(self, boundary: int):
        self._available = []
        self._state = {}
        self.boundary = boundary

    async def get_session(self) -> NoProxyState:
        len_s = len(self._available)
        new_time = time.time()
        if len_s > 0:
            check = new_time - self._available[0].time
            if check > 1.0:
                new_state = self._available.pop(0)
                return new_state
        new_state = await self.create_new_token_state()
        self._state[new_state] = new_state
        return new_state

    async def close_token(self, st: NoProxyState):
        try:
            await st.close()
        except Exception as e:
            c_logger.debug(str(e))
        try:
            del self._state[st]
        except Exception as e:
            c_logger.debug(str(e))



    async def put_back(self, st: NoProxyState):
        st.update_state()
        # c_logger.debug(f"len__state = {len(self._state.keys())},"
        #                f"len_available = {len(self._available)}")
        if st.past_bound(self.boundary):
            await self.close_token(st)
        else:
            self._available.append(st)

    async def close(self):
        for key in self._state:
            try:
                await self._state[key].close()
            except Exception as e:
                c_logger.debug(str(e))
        # for item in self._available:
        #     try:
        #         await self._state[key].close()
        #     except Exception as e:
        #         c_logger.debug(str(e))

    async def create_new_token_state(self):
        """ Override it"""
        return NoProxyState()


class TokenNoProxyState(NoProxyState):
    def __init__(self):
        super().__init__()

    async def get_csrf_token(self, url: str, err_codes: list, headers: dict, pars_func) -> list:
        i = 0
        sleep = 0.03333333333
        max_tries = 100
        while i < max_tries:
            try:
                response = await self.session.get(url=url, headers=headers, allow_redirects=True)
                if response.status in err_codes:
                    c_logger.error(f"status_code = {response.status} with url {url}\nHTTP count is {i}")
                    if response.status == 503 or response.status == 504:
                        await asyncio.sleep(3)
                    await asyncio.sleep(sleep)
                    i += 1
                else:
                    self.tokens = await pars_func(response)
                    return self.tokens
            except Exception as e:
                tb = traceback.format_exc()
                c_logger.error(f"HTTP EXCEPTION URL {url}\nHTTP exception occurred:\n {tb}\nHTTP count is {i}")
                i = i + 1
                await asyncio.sleep(sleep)
        c_logger.critical(f"{max_tries} times failure")
        raise Exception(f"{max_tries} times failure")


class UnboundedSessionNoProxyTokenPool(UnboundedNoProxySessionPool):
    def __init__(self, boundary: int, url: str,
                 err_codes: list, headers: dict, pars_func):
        super().__init__(boundary=boundary)
        self.csrf_url = url
        self.err_codes = err_codes
        self.headers = headers
        self.pars_func = pars_func

    async def create_session(self):
        new_state = await self.create_new_token_state()
        self._state[new_state] = new_state
        self._available.append(new_state)

    async def create_new_token_state(self) -> NoProxyState:
        """Overrides to handle token"""
        new_state = TokenNoProxyState()
        await new_state.get_csrf_token(url=self.csrf_url,
                                       headers=self.headers,
                                       err_codes=self.err_codes,
                                       pars_func=self.pars_func)
        return new_state

