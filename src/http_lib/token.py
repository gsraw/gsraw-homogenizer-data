import copy


class Token:
    def __init__(self,param_key: str, token: str):
        self.param_key: str = param_key
        self.token: str = token


class Tokens:
    def __init__(self, t_list: list):
        self.s_list = t_list
        self.length = len(t_list)

    def get(self, index : int):
        return self.s_list[index]

    def immutable_add_to_dict(self,s_dict: dict) -> dict:
        n_dict = copy.deepcopy(s_dict)
        for i in range(self.length):
            token = self.get(i)
            n_dict[token.param_key] = token.token
        return n_dict


