import gzip
from time import time
from typing import Tuple, List, Type, Union, Dict, Callable
import traceback
import asyncio
# from random_user_agent.user_agent import UserAgent
# from random_user_agent.params import SoftwareName, OperatingSystem

import aiohttp
from aiohttp.typedefs import StrOrURL
from cysimdjson import JSONObject
import xml.etree.ElementTree as etree

from random_user_agent.user_agent import UserAgent

from http_lib.token import Tokens
# from sql.size_utility import get_size_of_bytes
from src.http_lib.session_pool import UnboundedSessionPool, State
from src.http_lib.session_pool_no_proxy import UnboundedNoProxySessionPool,NoProxyState
from src.log import container

c_logger = container.get_logger('crawl','crawl_token_manager.log')

often_used_err_codes = [401, 403, 503, 504, 100, 500]


async def get_url(a_session: State, url: str, headers: dict, proxy: str, params: dict):
    if params is not None:
        return a_session.session.get(url=url, headers=headers, allow_redirects=True,
                                     proxy=proxy, params=params)
    return a_session.session.get(url=url, headers=headers, allow_redirects=True,
                                 proxy=proxy)


class SessionManager:

    def __init__(self, err_codes: list, session_pool: UnboundedSessionPool, param: bool,
                 handle_success: Callable[[bytes], Union[JSONObject, etree.Element]] = None):
        self.err_codes = err_codes
        self.unbounded_token_pool = session_pool
        self.param = param
        self.total_errors: int = 0
        self.number_of_function_calls = 0
        self.total_time_success: List[float] = []
        self.total_time_error: List[float] = []
        self.handle_success: Callable[[bytes], Union[JSONObject, etree.Element]] = handle_success
    # TODO add division by zero check

    def total_connections(self) -> int:
        return self.number_of_function_calls + self.total_errors

    def successful_connections_per_failure(self) -> float:
        if self.total_errors == 0:
            return self.total_connections()
        return self.total_connections() / self.total_errors

    def get_connection_rate(self) -> float:
        total_connections: int = self.total_connections()
        if total_connections == 0:
            return 0
        return self.total_errors / self.total_connections()

    def get_average_success_connections_speed(self) -> float:
        if self.number_of_function_calls == 0:
            return 0
        return sum(self.total_time_success) / self.number_of_function_calls

    def get_average_error_connections_speed(self) -> float:
        if self.total_errors == 0:
            return 0
        return sum(self.total_time_error) / self.total_errors

    def get_average_connection_speed(self) -> float:
        if self.total_connections() == 0:
            return 0
        return ( sum(self.total_time_success) + sum(self.total_time_error) ) / self.total_connections()

    def add_token(self, a_session: State, s_dict: dict):
            tokens: Tokens = a_session.get_token()
            if tokens is not None:
                for i in range(tokens.length):
                    token = tokens.get(i)
                    # print(f"token = {token.token}")
                    s_dict[token.param_key] = token.token
        # que hoy no corresponde a ningún anuncio

    async def post_compress_and_status(self, url: str, headers: dict,data ,params: dict = None, allow_redirects= True,
                                       handle_success: Callable[[bytes], Union[JSONObject, etree.Element]] = None) \
            ->  Union[Tuple[bytes,int],Tuple[Union[JSONObject, etree.Element], int]]:
        self.number_of_function_calls += 1
        i = 0
        sleep = 0.03333333333
        max_tries = 150
        # software_names = [SoftwareName.CHROME.value]
        # operating_systems = [OperatingSystem.WINDOWS.value, OperatingSystem.LINUX.value]
        #
        # user_agent_rotator = UserAgent(software_names=software_names, operating_systems=operating_systems, limit=100)
        #
        # # # Get list of user agents.
        # # user_agents = user_agent_rotator.get_user_agents()
        #
        # # Get Random User Agent String.
        # user_agent = user_agent_rotator.get_random_user_agent()
        # headers['user-agent'] = user_agent
        while i < max_tries:
            a_session = await self.unbounded_token_pool.get_session()
            _start = time()
            try:
                proxy = a_session.proxy
                if self.param:
                    self.add_token(a_session=a_session, s_dict=params)
                else:
                    self.add_token(a_session=a_session, s_dict=headers)
                async with a_session.session.post(url=url, headers=headers, allow_redirects=allow_redirects,
                                                 proxy=proxy, data=data, params=params) as response:
                    if response.status in self.err_codes:
                        c_logger.debug(f"status_code {response.status} with url {url}\nHTTP count is {i}")
                        self.total_time_error.append(time() - _start)
                        await self.unbounded_token_pool.close_token(a_session)
                        await asyncio.sleep(sleep)
                        i += 1
                    else:
                        _bytes = await response.read()
                        self.total_time_success.append(time() - _start)
                        status = response.status
                        await self.unbounded_token_pool.put_back(a_session)
                        c_logger.debug(f"status_code {response.status} with url {url}\nHTTP count is {i}")
                        self.total_errors += i
                        if self.handle_success is not None:
                            # throws handle_success Exception
                            self.handle_success(_bytes)
                        return gzip.compress(_bytes), status


            except Exception as e:
                tb = traceback.format_exc()
                c_logger.error(f"HTTP EXCEPTION URL {url}\nHTTP exception occurred:\n{tb}\nHTTP count is {i}")
                self.total_time_error.append(time() - _start)
                await self.unbounded_token_pool.close_token(a_session)
                i = i + 1
                await asyncio.sleep(sleep)
        self.total_errors += max_tries
        c_logger.critical(f"{max_tries} times failure, error rate = {self.get_connection_rate()}")
        raise Exception(f"{max_tries} times failure, error rate = {self.get_connection_rate()}")

    async def post_bytes_and_status(self, url: str, headers: dict, data,
                                    params: Union[Tuple[Tuple[str, str]]] = None,
                                    allow_redirects=True,  handle_success: Callable[[bytes], Union[JSONObject, etree.Element]] = None) -> \
            Union[Tuple[bytes,int],Tuple[Union[JSONObject, etree.Element],int]]:
        self.number_of_function_calls += 1
        i = 0
        sleep = 0.03333333333
        max_tries = 150
        while i < max_tries:
            a_session = await self.unbounded_token_pool.get_session()
            _start = time()
            try:
                proxy = a_session.proxy
                if self.param:
                    self.add_token(a_session=a_session, s_dict=params)
                else:
                    self.add_token(a_session=a_session, s_dict=headers)
                async with a_session.session.post(url=url, headers=headers, allow_redirects=allow_redirects,
                                                  proxy=proxy, data=data, params=params) as response:
                    if response.status in self.err_codes:
                        c_logger.debug(f"status_code {response.status} with url {url}\nHTTP count is {i}")
                        self.total_time_error.append(time() - _start)
                        await self.unbounded_token_pool.close_token(a_session)
                        await asyncio.sleep(sleep)
                        i += 1
                    else:
                        _bytes = await response.read()
                        self.total_time_success.append(time() - _start)
                        status = response.status
                        await self.unbounded_token_pool.put_back(a_session)
                        c_logger.debug(f"status_code {response.status} with url {url}\nHTTP count is {i}")
                        self.total_errors += i
                        if self.handle_success is not None:
                            # throws handle_success Exception
                            n_result = self.handle_success(_bytes)
                            return n_result, status
                        else:
                            return _bytes, status
            except Exception as e:
                tb = traceback.format_exc()
                c_logger.error(f"HTTP EXCEPTION URL {url}\nHTTP exception occurred:\n{tb}\nHTTP count is {i}")
                self.total_time_error.append(time() - _start)
                await self.unbounded_token_pool.close_token(a_session)
                i = i + 1
                await asyncio.sleep(sleep)
        self.total_errors += max_tries
        c_logger.critical(f"{max_tries} times failure, error rate = {self.get_connection_rate()}")
        raise Exception(f"{max_tries} times failure, error rate = {self.get_connection_rate()}")


    async def compress_and_status(self,url: str, headers: dict, params: dict = None, allow_redirects = True) -> Tuple[bytes,int]:
        self.number_of_function_calls += 1
        i = 0
        sleep = 0.03333333333
        max_tries = 150
        # software_names = [SoftwareName.CHROME.value]
        # operating_systems = [OperatingSystem.WINDOWS.value, OperatingSystem.LINUX.value]

        # user_agent_rotator = UserAgent(software_names=software_names, operating_systems=operating_systems, limit=10000)
        #
        # # # Get list of user agents.
        # # user_agents = user_agent_rotator.get_user_agents()
        #
        # # Get Random User Agent String.
        # user_agent = user_agent_rotator.get_random_user_agent()
        # headers['user-agent'] = user_agent
        while i < max_tries:
            a_session = await self.unbounded_token_pool.get_session()
            _start = time()
            try:
                proxy = a_session.proxy
                if self.param:
                    self.add_token(a_session=a_session, s_dict=params)
                else:
                    self.add_token(a_session=a_session, s_dict=headers)
                async with a_session.session.get(url=url, headers=headers, allow_redirects=allow_redirects,
                                                 proxy=proxy, params=params) as response:
                    if response.status in self.err_codes:
                        c_logger.debug(f"status_code {response.status} with url {url}\nHTTP count is {i}")
                        self.total_time_error.append(time() - _start)
                        await self.unbounded_token_pool.close_token(a_session)
                        await asyncio.sleep(sleep)
                        i += 1
                    else:
                        _bytes = await response.read()
                        self.total_time_success.append(time() - _start)
                        status = response.status
                        # c_logger.debug(f"status_code {response.status} with url {url}\nHTTP count is {i}")
                        await self.unbounded_token_pool.put_back(a_session)
                        self.total_errors += i
                        return gzip.compress(_bytes), status
            except Exception as e:
                tb = traceback.format_exc()
                c_logger.error(f"HTTP EXCEPTION URL {url}\nHTTP exception occurred:\n{tb}\nHTTP count is {i}")
                self.total_time_error.append(time() - _start)
                await self.unbounded_token_pool.close_token(a_session)
                i = i + 1
                await asyncio.sleep(sleep)
        self.total_errors += max_tries
        c_logger.critical(f"{max_tries} times failure, error rate = {self.get_connection_rate()}")
        raise Exception(f"{max_tries} times failure, error rate = {self.get_connection_rate()}")

    async def get_bytes_and_status(self, url: str, headers: dict, params: dict = None, allow_redirects = True) -> Tuple[bytes, int]:
        self.number_of_function_calls += 1
        i = 0
        sleep = 0.03333333333
        max_tries = 150

        # you can also import SoftwareEngine, HardwareType, SoftwareType, Popularity from random_user_agent.params
        # you can also set number of user agents required by providing `limit` as parameter

        # software_names = [SoftwareName.CHROME.value]
        # operating_systems = [OperatingSystem.WINDOWS.value, OperatingSystem.LINUX.value]
        #
        # user_agent_rotator = UserAgent(software_names=software_names, operating_systems=operating_systems, limit=10000)
        #
        # # # Get list of user agents.
        # # user_agents = user_agent_rotator.get_user_agents()
        #
        # # Get Random User Agent String.
        # user_agent = user_agent_rotator.get_random_user_agent()
        # headers['user-agent'] = user_agent
        while i < max_tries:
            a_session = await self.unbounded_token_pool.get_session()
            _start = time()
            try:
                proxy = a_session.proxy
                if self.param:
                    self.add_token(a_session=a_session, s_dict=params)
                else:
                    self.add_token(a_session=a_session, s_dict=headers)
                async with a_session.session.get(url=url, headers=headers, allow_redirects=allow_redirects,
                                                 proxy=proxy, params=params) as response:
                    if response.status in self.err_codes:
                        c_logger.debug(f"status_code {response.status} with url {url}\n HTTP count is {i}\n")
                        self.total_time_error.append(time() - _start)
                        await self.unbounded_token_pool.close_token(a_session)
                        await asyncio.sleep(sleep)
                        i += 1
                    else:
                        _bytes = await response.read()
                        self.total_time_success.append(time() - _start)
                        status = response.status
                        c_logger.debug(f"status_code {response.status} with url {url}\nHTTP count is {i}")
                        await self.unbounded_token_pool.put_back(a_session)
                        self.total_errors += i
                        # c_logger.debug(f"FILE SIZE = {len(_bytes)}")
                        return _bytes, status
            except Exception as e:
                self.total_time_error.append(time() - _start)
                tb = traceback.format_exc()
                c_logger.error(f"HTTP EXCEPTION URL {url}\nHTTP exception occurred:\n{tb}\nHTTP count is {i}")
                await self.unbounded_token_pool.close_token(a_session)
                i = i + 1
                await asyncio.sleep(sleep)
        self.total_errors += max_tries
        c_logger.critical(f"{max_tries} times failure, error rate = {self.get_connection_rate()}")
        raise Exception(f"{max_tries} times failure, error rate = {self.get_connection_rate()}")

    async def pool_get(self, url: str, headers: dict, params: dict = None) -> bytes:
        self.number_of_function_calls += 1
        i = 0
        sleep = 0.03333333333
        max_tries = 150
        while i < max_tries:
            a_session = await self.unbounded_token_pool.get_session()
            _start = time()
            try:
                proxy = a_session.proxy
                if self.param:
                    self.add_token(a_session=a_session, s_dict=params)
                else:
                    self.add_token(a_session=a_session, s_dict=headers)
                async with a_session.session.get(url=url, headers=headers, allow_redirects=True,
                                     proxy=proxy, params=params) as response:
                    if response.status in self.err_codes:
                        c_logger.debug(f"status_code {response.status} with url {url}\nHTTP count is {i}")
                        # c_logger.debug(f"text =\n {await response.text('utf-8')}")
                        self.total_time_error.append(time() - _start)
                        await self.unbounded_token_pool.close_token(a_session)
                        await asyncio.sleep(sleep)
                        i += 1
                    else:
                        _bytes = await response.read()
                        self.total_time_success.append(time() - _start)
                        c_logger.debug(f"status_code {response.status} with url {url}\nHTTP count is {i}")

                        await self.unbounded_token_pool.put_back(a_session)
                        self.total_errors += i
                        return _bytes
            except Exception as e:
                self.total_time_error.append(time() - _start)
                tb = traceback.format_exc()
                c_logger.error(f"HTTP EXCEPTION URL {url}\nHTTP exception occurred:\n{tb}\nHTTP count is {i}")
                await self.unbounded_token_pool.close_token(a_session)
                i = i + 1
                await asyncio.sleep(sleep)
        self.total_errors += max_tries
        c_logger.critical(f"{max_tries} times failure, error rate = {self.get_connection_rate()}")
        raise Exception(f"{max_tries} times failure, error rate = {self.get_connection_rate()}")

    # async def pool_get_response(self, url: str, headers: dict, params: dict = None, allow_redirect = True) -> tuple:
    #     self.number_of_function_calls += 1
    #     i = 0
    #     sleep = 0.03333333333
    #     max_tries = 150
    #     while i < max_tries:
    #         a_session = await self.unbounded_token_pool.get_session()
    #         try:
    #             proxy = a_session.proxy
    #             if self.param:
    #                 self.add_token(a_session=a_session, s_dict=params)
    #             else:
    #                 self.add_token(a_session=a_session, s_dict=headers)
    #             async with a_session.session.get(url=url, headers=headers, allow_redirects=allow_redirect,
    #                                  proxy=proxy, params=params) as response:
    #                 if response.status in self.err_codes:
    #                     c_logger.debug(f"status_code {response.status} with url {url}\nHTTP count is {i}")
    #                     await self.unbounded_token_pool.close_token(a_session)
    #                     await asyncio.sleep(sleep)
    #                     i += 1
    #                 else:
    #                     _bytes = await response.read()
    #                     await self.unbounded_token_pool.put_back(a_session)
    #                     self.total_errors += i
    #                     return response, _bytes
    #         except Exception as e:
    #             tb = traceback.format_exc()
    #             c_logger.error(f"HTTP EXCEPTION URL {url}\nHTTP exception occurred:\n{tb}\nHTTP count is {i}")
    #             await self.unbounded_token_pool.close_token(a_session)
    #             i = i + 1
    #             await asyncio.sleep(sleep)
    #     self.total_errors += max_tries
    #     c_logger.critical(f"{max_tries} times failure, error rate = {self.get_connection_rate()}")
    #     raise Exception(f"{max_tries} times failure, error rate = {self.get_connection_rate()}")

    async def pool_post(self, url: str, headers: dict, data, params: dict = None) -> bytes:
        self.number_of_function_calls += 1
        i = 0
        sleep = 0.03333333333
        max_tries = 150
        while i < max_tries:
            a_session = await self.unbounded_token_pool.get_session()
            _start = time()
            try:
                proxy = a_session.proxy
                if self.param:
                    self.add_token(a_session=a_session, s_dict=params)
                else:
                    self.add_token(a_session=a_session, s_dict=headers)
                async with a_session.session.post(url=url, headers=headers, allow_redirects=True,
                                                 proxy=proxy, params=params, data=data) as response:
                    if response.status in self.err_codes:
                        self.total_time_error.append(time() - _start)
                        c_logger.debug(f"status_code {response.status} with url {url}\nHTTP count is {i}")
                        await self.unbounded_token_pool.close_token(a_session)
                        await asyncio.sleep(sleep)
                        i += 1
                    else:
                        _bytes = await response.read()
                        self.total_time_success.append(time() - _start)
                        await self.unbounded_token_pool.put_back(a_session)
                        print(f"status = {response.status}")
                        # return await response.text(encoding='utf-8')
                        self.total_errors += i
                        return _bytes
            except Exception as e:
                self.total_time_error.append(time() - _start)
                tb = traceback.format_exc()
                c_logger.error(f"HTTP EXCEPTION URL {url}\nHTTP exception occurred:\n{tb}\nHTTP count is {i}")
                await self.unbounded_token_pool.close_token(a_session)
                i = i + 1
                await asyncio.sleep(sleep)
        self.total_errors += max_tries
        c_logger.critical(f"{max_tries} times failure, error rate = {self.get_connection_rate()}")
        raise Exception(f"{max_tries} times failure, error rate = {self.get_connection_rate()}")

    async def close(self) -> None:
        await self.unbounded_token_pool.close()


class NoProxySessionManager:
    def __init__(self, err_codes: List[int], session_pool: UnboundedNoProxySessionPool, param: bool, return_exceptions: Tuple[Type[Exception], ...] = None ):
        if return_exceptions is None:
            return_exceptions: Tuple[Exception, ...] = ()
        self.err_codes = err_codes
        self.unbounded_no_proxy_pool = session_pool
        self.param = param
        self.return_exceptions: Tuple[Type[Exception], ...] = return_exceptions
        self.total_errors: int = 0
        self.number_of_function_calls = 0
        self.total_time_success: List[float] = []
        self.total_time_error: List[float] = []


    def total_connections(self) -> int:
        return self.number_of_function_calls + self.total_errors

    def successful_connections_per_failure(self) -> float:
        if self.total_errors == 0:
            return self.total_connections()
        return self.total_connections() / self.total_errors

    def get_connection_rate(self) -> float:
        total_connections: int = self.total_connections()
        if total_connections == 0:
            return 0
        return self.total_errors / total_connections

    def get_average_success_connections_speed(self) -> float:
        if self.number_of_function_calls == 0:
            return 0
        return sum(self.total_time_success) / self.number_of_function_calls

    def get_average_error_connections_speed(self) -> float:
        if self.total_errors == 0:
            return 0
        return sum(self.total_time_error) / self.total_errors

    def get_average_connection_speed(self) -> float:
        if self.total_connections() == 0:
            return 0
        return (sum(self.total_time_success) + sum(self.total_time_error)) / self.total_connections()


    def add_token(self, a_session: NoProxyState, s_dict: dict):
            tokens: Tokens = a_session.get_token()
            if tokens is not None:
                for i in range(tokens.length):
                    token = tokens.get(i)
                    # print(f"token = {token.token}")
                    s_dict[token.param_key] = token.token


    async def post_compress_and_status(self, url: str, headers: dict,data ,params: dict = None, allow_redirects=True) -> Tuple[bytes,int]:
        i = 0
        sleep = 0.03333333333
        max_tries = 150
        self.number_of_function_calls += 1
        while i < max_tries:
            a_session = await self.unbounded_no_proxy_pool.get_session()
            try:
                if self.param:
                    self.add_token(a_session=a_session, s_dict=params)
                else:
                    self.add_token(a_session=a_session, s_dict=headers)
                async with a_session.session.post(url=url, headers=headers, allow_redirects=allow_redirects,
                                                  data=data, params=params) as response:
                    if response.status in self.err_codes:
                        c_logger.debug(f"status_code {response.status} with url {url}\nHTTP count is {i}")
                        await self.unbounded_no_proxy_pool.close_token(a_session)
                        await asyncio.sleep(sleep)
                        i += 1
                    else:
                        _bytes = await response.read()
                        status = response.status
                        self.total_errors += i
                        await self.unbounded_no_proxy_pool.put_back(a_session)
                        c_logger.debug(f"status_code {response.status} with url {url}\nHTTP count is {i}")
                        return gzip.compress(_bytes), status
            except Exception as e:
                tb = traceback.format_exc()
                is_exc_valid = any([isinstance(e, exc) for exc in self.return_exceptions])
                c_logger.error(f"HTTP EXCEPTION URL {url}\n"
                               f"HTTP exception occurred:\n{tb}\n"
                               f"HTTP count is {i}\n"
                               f"type {type(e).__name__}, {type(self.return_exceptions[0]).__name__}")
                await self.unbounded_no_proxy_pool.close_token(a_session)
                if is_exc_valid:
                    return None, response.status
                i = i + 1
                await asyncio.sleep(sleep)
        c_logger.critical(f"{max_tries} times failure")
        raise Exception(f"{max_tries} times failure")

    async def compress_and_status(self, url: str, headers: dict, params: dict = None, allow_redirects=True) -> Tuple[
        bytes, int]:
        i = 0
        self.number_of_function_calls += 1
        sleep = 0.03333333333
        max_tries = 150
        status: int = -1
        while i < max_tries:
            a_session = await self.unbounded_no_proxy_pool.get_session()
            try:
                if self.param:
                    self.add_token(a_session=a_session, s_dict=params)
                else:
                    self.add_token(a_session=a_session, s_dict=headers)
                async with a_session.session.get(url=url, headers=headers, allow_redirects=allow_redirects,
                                                 params=params) as response:
                    status = response.status
                    if response.status in self.err_codes:
                        c_logger.debug(f"status_code {response.status} with url {url}\nHTTP count is {i}")
                        await self.unbounded_no_proxy_pool.close_token(a_session)
                        await asyncio.sleep(sleep)
                        i += 1
                    else:
                        _bytes = await response.read()
                        status = response.status
                        self.total_errors += i
                        await self.unbounded_no_proxy_pool.put_back(a_session)
                        c_logger.debug(f"status_code {status} with url {url}\nHTTP count is {i}")
                        return gzip.compress(_bytes), status
            except Exception as e:
                tb = traceback.format_exc()
                await self.unbounded_no_proxy_pool.close_token(a_session)
                i = i + 1
                is_exc_valid = any([isinstance(e, exc) for exc in self.return_exceptions])
                c_logger.error(f"HTTP EXCEPTION URL {url}\n"
                               f"HTTP exception occurred:\n{tb}\n"
                               f"status = {status}\n"
                               f"HTTP count is {i}\n")
                if is_exc_valid:
                    return None, -1
                await asyncio.sleep(sleep)
        c_logger.critical(f"{max_tries} times failure")
        raise Exception(f"{max_tries} times failure")

    async def pool_get(self, url: str, headers: dict ,params: dict = None) -> bytes:
        i = 0
        self.number_of_function_calls += 1
        sleep = 0.03333333333
        max_tries = 150
        while i < max_tries:
            a_session = await self.unbounded_no_proxy_pool.get_session()
            try:
                if self.param:
                    self.add_token(a_session=a_session, s_dict=params)
                else:
                    self.add_token(a_session=a_session, s_dict=headers)
                async with a_session.session.get(url=url, headers=headers, allow_redirects=True,
                                     params=params) as response:
                    if response.status in self.err_codes:
                        c_logger.debug(f"status_code {response.status} with url {url}\nHTTP count is {i}")
                        await self.unbounded_no_proxy_pool.close_token(a_session)
                        await asyncio.sleep(sleep)
                        i += 1
                    else:
                        _bytes = await response.read()
                        self.total_errors += i
                        c_logger.debug(f"status_code {response.status} with url {url}\nHTTP count is {i}")
                        await self.unbounded_no_proxy_pool.put_back(a_session)
                        return _bytes
            except Exception as e:
                tb = traceback.format_exc()
                c_logger.error(f"HTTP EXCEPTION URL {url}\nHTTP exception occurred:\n{tb}\nHTTP count is {i}")
                await self.unbounded_no_proxy_pool.close_token(a_session)
                i = i + 1
                await asyncio.sleep(sleep)
        c_logger.critical(f"{max_tries} times failure")
        raise Exception(f"{max_tries} times failure")

    async def pool_post(self, url: str, headers: dict, data , params: Union[dict,Tuple[tuple]] = None) -> bytes:
        i = 0
        sleep = 0.03333333333
        max_tries = 150
        self.number_of_function_calls += 1
        while i < max_tries:
            a_session = await self.unbounded_no_proxy_pool.get_session()
            try:
                if self.param:
                    self.add_token(a_session=a_session, s_dict=params)
                else:
                    self.add_token(a_session=a_session, s_dict=headers)
                async with a_session.session.post(url=url, headers=headers, allow_redirects=True,
                                                 params=params, data=data) as response:
                    if response.status in self.err_codes:
                        c_logger.debug(f"status_code {response.status} with url {url}\nHTTP count is {i}")
                        await self.unbounded_no_proxy_pool.close_token(a_session)
                        await asyncio.sleep(sleep)
                        i += 1
                    else:
                        _bytes = await response.read()
                        self.total_errors += i
                        print(f"status = {response.status}")
                        await self.unbounded_no_proxy_pool.put_back(a_session)
                        # return await response.text(encoding='utf-8')
                        return _bytes
            except Exception as e:
                tb = traceback.format_exc()
                c_logger.error(f"HTTP EXCEPTION URL {url}\nHTTP exception occurred:\n{tb}\nHTTP count is {i}")
                await self.unbounded_no_proxy_pool.close_token(a_session)
                i = i + 1
                await asyncio.sleep(sleep)
        c_logger.critical(f"{max_tries} times failure")
        raise Exception(f"{max_tries} times failure")

    async def close(self) -> None:
        await self.unbounded_no_proxy_pool.close()