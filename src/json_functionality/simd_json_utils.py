from typing import Union

from cysimdjson import JSONObject


def try_get(listing: JSONObject, path: str) -> Union[float, int, str]:
    try:
        return listing.at_pointer(path)
    except KeyError as e: pass
        # logger.debug(str(e))
    except ValueError as e:
        # logger.debug(str(e))
        pass
    return None
