import logging
import sys

FORMATTER = logging.Formatter("%(asctime)s — %(name)s — %(levelname)s — %(message)s")
all_loggers = {}


def get_console_handler():
    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setFormatter(FORMATTER)
    return console_handler


def get_file_handler(file):
    file_handler = logging.FileHandler(file, mode='w+')
    file_handler.setFormatter(FORMATTER)
    return file_handler


def get_logger(logger_name, file_name):
    if logger_name in all_loggers:
        return all_loggers[logger_name]
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(get_console_handler())
    logger.addHandler(get_file_handler(file_name))
    # with this pattern, it's rarely necessary to propagate the error up to parent
    logger.propagate = False
    all_loggers[logger_name] = logger
    return logger


class LoggerContainer():
    shared_loggers = {}
    shared_files = {}

    def get_logger(self, logger_name, file_name):
        if logger_name in self.shared_loggers:
            print("yes me")
            return self.shared_loggers[logger_name]
        logger = logging.getLogger(logger_name)
        logger.setLevel(logging.DEBUG)
        logger.addHandler(get_console_handler())
        if file_name in self.shared_files:
            file_handler = self.shared_files[file_name]
        else:
            file_handler = get_file_handler(file_name)
            self.shared_files[file_name] = file_handler
        logger.addHandler(file_handler)
        logger.propagate = False
        self.shared_loggers[logger_name] = logger
        return logger


container = LoggerContainer()
c_logger = container.get_logger('crawl', 'crawl.log')
