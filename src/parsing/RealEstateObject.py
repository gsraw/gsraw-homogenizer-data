from typing import List

# from data_homog.homogenizer.real_estate.table_definition import PRIMARY_KEY_FIELD, COLUMN_INFO, COLUMN_NULLIFY_KEY, \
#     COLUMN_TYPE_KEY
from parsing.normalized_columns import NormalizedRealEstateColumns


class RealEstateData:
    __slots__ = (
        'full_address',
        'build_status_id',
        'construction_year',
        'description',
        'original_admin1',
        'original_admin2',
        'original_admin3',
        'original_admin4',
        'original_admin5',
        'has_air_conditioner',
        'has_elevator',
        'has_garage',
        'has_garage_included',
        'has_storage',
        'has_terrace',
        'detail_page',
        'is_exact_address',
        'lat',
        'lon',
        'n_baths',
        'n_rooms',
        'operation_type_id',
        'listing_id',
        'last_appearance',
        'local_price',
        'portal_price_discount',
        'last_update',
        'property_type_id',
        'area',
        'area_util',
        'conservation_id',
        'title',
        'agency_url',
        'usage_id',
        'portal_reference',
        'contact_name',
        'energy_cert_id',
        'garage_price',
        'has_balcony',
        'has_racket_zone',
        'has_security',
        'has_pool',
        'has_common_zones',
        'has_developable',
        'pet_friendly',
        'cardinal_direction_id',
        'parse_date',
        'week',
        'additional_fields',
        # 'location_all',
        'floor',
        'contact_phone',
        'postcode',
        'vendor_type',
        'listing_created_at',
        'has_photo_report',
        'is_exterior',
        'agency_name',
        'parent_id',
        'n_images',
        'push_up',
        'highlighted',
        'has_virtual_tour',
        'has_three_dim_tour',
        'is_urgent',
        'plan_available',
        'agency_branch',
        'heating_type',
        'image_urls',
        # 'price_portal_discount',

    )
    def __init__(self):
        self.full_address = None,
        self.build_status_id = None
        self.construction_year = None
        self.description = None
        self.original_admin1 = None
        self.original_admin2 = None
        self.original_admin3 = None
        self.original_admin4 = None
        self.original_admin5 = None
        self.floor = None
        self.has_air_conditioner = None
        self.has_elevator = None
        self.has_garage = None
        self.has_garage_included = None
        self.has_storage = None
        self.has_terrace = None
        self.detail_page = None
        self.is_exact_address = None
        self.lat = None
        self.lon = None
        self.n_baths = None
        self.n_rooms = None
        self.operation_type_id = None
        self.listing_id = None
        self.last_appearance = None
        self.local_price = None
        self.last_update = None
        self.property_type_id = None
        self.area = None
        self.area_util = None
        self.conservation_id = None
        self.title = None
        self.agency_url = None
        self.usage_id = None
        self.portal_reference = None
        self.contact_name = None
        self.energy_cert_id = None
        self.garage_price = None
        self.has_balcony = None
        self.has_racket_zone = None
        self.has_security = None
        self.has_pool = None
        self.has_common_zones = None
        self.has_developable = None
        self.pet_friendly = None
        self.cardinal_direction_id = None
        self.parse_date = None
        self.week = None
        # self.location_all = None
        self.contact_phone = None
        self.portal_price_discount = None
        self.postcode = None
        self.vendor_type = None
        self.listing_created_at = None
        self.is_exterior = None
        self.parent_id = None
        self.agency_name = None
        self.n_images = None
        self.push_up = None
        self.highlighted = None
        self.has_virtual_tour = None
        self.additional_fields = None
        self.has_three_dim_tour = None
        self.is_urgent = None
        self.plan_available = None
        self.agency_branch = None
        self.heating_type = None
        self.image_urls = None
        # self.price_portal_discount = None

    def append_to_list(self, used_fields, append_to_list):
        for field in used_fields:
            append_to_list.append(getattr(self, field, None))

    def get_test_values(self, append_to_list):
        for field in self.__slots__:
            append_to_list.append((field, getattr(self, field)))


def create_table_test_real_estate(used_fields: List[str], table_name):
    table = f"create table if not exists {table_name} ("
    id_string = "id int NOT NULL AUTO_INCREMENT"
    primary_key_str = f"PRIMARY KEY (id));"
    columns = [id_string]
    for field in used_fields:
        if field == 'detail_page':
            columns.append(f'{field} varchar(800) NULL')
        else:
            columns.append(f'{field} varchar(255) NULL')
    columns.append(primary_key_str)
    table = table + ','.join(columns)
    print(f"table = {table}")
    return table


COLUMN_TYPE_KEY = 'type'
COLUMN_NULLIFY_KEY = 'nullify'
PRIMARY_KEY_FIELD = 'listing_id'
FUNCTION_KEY = 'transformer'

COLUMN_INFO = {
    NormalizedRealEstateColumns.garage_price.value: {
        COLUMN_TYPE_KEY: 'integer',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.parse_date.value: {
        COLUMN_TYPE_KEY: 'varchar(20)',
        COLUMN_NULLIFY_KEY: 'NOT NULL'
    },
    NormalizedRealEstateColumns.week.value: {
        COLUMN_TYPE_KEY: 'varchar(10)',
        COLUMN_NULLIFY_KEY: 'NOT NULL'
    },
    NormalizedRealEstateColumns.n_images.value: {
        COLUMN_TYPE_KEY: 'integer',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.local_price.value: {
        COLUMN_TYPE_KEY: 'integer',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.plan_available.value: {
        COLUMN_TYPE_KEY: 'bool',
        COLUMN_NULLIFY_KEY: 'NOT NULL'
    },
    NormalizedRealEstateColumns.has_three_dim_tour.value: {
        COLUMN_TYPE_KEY: 'bool',
        COLUMN_NULLIFY_KEY: 'NOT NULL'
    },
    NormalizedRealEstateColumns.has_virtual_tour.value: {
        COLUMN_TYPE_KEY: 'bool',
        COLUMN_NULLIFY_KEY: 'NOT NULL'
    },
    NormalizedRealEstateColumns.push_up.value: {
        COLUMN_TYPE_KEY: 'bool',
        COLUMN_NULLIFY_KEY: 'NOT NULL'
    },
    NormalizedRealEstateColumns.highlighted.value: {
        COLUMN_TYPE_KEY: 'bool',
        COLUMN_NULLIFY_KEY: 'NOT NULL'
    },
    NormalizedRealEstateColumns.agency_branch.value: {
            COLUMN_TYPE_KEY: 'varchar(80)',
            COLUMN_NULLIFY_KEY: 'NULL'
        },
    NormalizedRealEstateColumns.agency_url.value: {
            COLUMN_TYPE_KEY: 'varchar(800)',
            COLUMN_NULLIFY_KEY: 'NULL'
        },

    NormalizedRealEstateColumns.heating_type.value: {
        COLUMN_TYPE_KEY: 'varchar(50)',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.id.value: {
        COLUMN_TYPE_KEY: 'integer',
        COLUMN_NULLIFY_KEY: 'not NULL'
    },
    NormalizedRealEstateColumns.full_address.value: {
        COLUMN_TYPE_KEY: 'varchar(255)',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.construction_year.value: {
        COLUMN_TYPE_KEY: 'integer',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.additional_fields.value: {
        COLUMN_TYPE_KEY: 'TEXT',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.description.value: {
        COLUMN_TYPE_KEY: 'TEXT',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.energy_cert_id.value: {
        COLUMN_TYPE_KEY: 'varchar(50)',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.has_air_conditioner.value: {
        COLUMN_TYPE_KEY: 'BOOL',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.has_elevator.value: {
        COLUMN_TYPE_KEY: 'BOOL',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.has_garage.value: {
        COLUMN_TYPE_KEY: 'BOOL',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.has_developable.value: {
        COLUMN_TYPE_KEY: 'BOOL',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.has_garage_included.value: {
        COLUMN_TYPE_KEY: 'BOOL',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.has_common_zones.value: {
        COLUMN_TYPE_KEY: 'BOOL',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.has_storage.value: {
        COLUMN_TYPE_KEY: 'BOOL',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.has_pool.value: {
        COLUMN_TYPE_KEY: 'BOOL',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.has_balcony.value: {
        COLUMN_TYPE_KEY: 'BOOL',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.pet_friendly.value: {
        COLUMN_TYPE_KEY: 'BOOL',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.has_racket_zone.value: {
        COLUMN_TYPE_KEY: 'BOOL',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.has_security.value: {
        COLUMN_TYPE_KEY: 'BOOL',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.has_terrace.value: {
        COLUMN_TYPE_KEY: 'BOOL',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.is_exact_address.value: {
        COLUMN_TYPE_KEY: 'BOOL',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.is_exterior.value: {
        COLUMN_TYPE_KEY: 'BOOL',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.lat.value: {
        COLUMN_TYPE_KEY: 'float',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.lon.value: {
        COLUMN_TYPE_KEY: 'float',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.original_admin1.value: {
        COLUMN_TYPE_KEY: 'varchar(255)',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.original_admin2.value: {
        COLUMN_TYPE_KEY: 'varchar(255)',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.original_admin3.value: {
        COLUMN_TYPE_KEY: 'varchar(255)',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.original_admin4.value: {
        COLUMN_TYPE_KEY: 'varchar(255)',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.original_admin5.value: {
        COLUMN_TYPE_KEY: 'varchar(255)',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.n_baths.value: {
        COLUMN_TYPE_KEY: 'integer',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.n_floor.value: {
        COLUMN_TYPE_KEY: 'float',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.floor.value: {
        COLUMN_TYPE_KEY: 'float',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.n_rooms.value: {
        COLUMN_TYPE_KEY: 'integer',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.operation_type_id.value: {
        COLUMN_TYPE_KEY: 'integer',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.cardinal_direction_id.value: {
        COLUMN_TYPE_KEY: 'integer',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.listing_id.value: {
        COLUMN_TYPE_KEY: 'varchar(80)',
        COLUMN_NULLIFY_KEY: 'NOT NULL'
    },
    NormalizedRealEstateColumns.last_appearance.value: {
        COLUMN_TYPE_KEY: 'DATE',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.property_type_id.value: {
        COLUMN_TYPE_KEY: 'integer',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.usage_id.value: {
        COLUMN_TYPE_KEY: 'integer',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.last_update.value: {
        COLUMN_TYPE_KEY: 'varchar(15)',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.contact_name.value: {
        COLUMN_TYPE_KEY: 'varchar(255)',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.agency_name.value: {
        COLUMN_TYPE_KEY: 'varchar(255)',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.area.value: {
        COLUMN_TYPE_KEY: 'integer',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.area_util.value: {
        COLUMN_TYPE_KEY: 'integer',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.image_urls.value: {
        COLUMN_TYPE_KEY: 'TEXT',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.conservation_id.value: {
        COLUMN_TYPE_KEY: 'integer',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.build_status_id.value: {
        COLUMN_TYPE_KEY: 'integer',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.title.value: {
        COLUMN_TYPE_KEY: 'varchar(255)',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.detail_page.value: {
        COLUMN_TYPE_KEY: 'varchar(1000)',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.portal_reference.value: {
        COLUMN_TYPE_KEY: 'varchar(80)',
        COLUMN_NULLIFY_KEY: 'NULL'
    },
    NormalizedRealEstateColumns.contact_phone.value: {
        COLUMN_TYPE_KEY: 'varchar(30)',
        COLUMN_NULLIFY_KEY: 'NULL',
    },
    NormalizedRealEstateColumns.is_urgent.value : {
        COLUMN_TYPE_KEY: 'bool',
        COLUMN_NULLIFY_KEY: 'NULL',
    },
    NormalizedRealEstateColumns.parent_id.value : {
        COLUMN_TYPE_KEY: 'varchar(80)',
        COLUMN_NULLIFY_KEY: 'NULL',
    },
    NormalizedRealEstateColumns.portal_price_discount.value: {
        COLUMN_TYPE_KEY: 'varchar(80)',
        COLUMN_NULLIFY_KEY: 'NULL',
    },
    NormalizedRealEstateColumns.vendor_type.value: {
        COLUMN_TYPE_KEY: 'varchar(80)',
        COLUMN_NULLIFY_KEY: 'NULL',
    },
    NormalizedRealEstateColumns.postcode.value: {
        COLUMN_TYPE_KEY: 'varchar(25)',
        COLUMN_NULLIFY_KEY: 'NULL',
    },
    NormalizedRealEstateColumns.listing_created_at.value: {
        COLUMN_TYPE_KEY: 'varchar(30)',
        COLUMN_NULLIFY_KEY: 'NULL',
    },
    NormalizedRealEstateColumns.has_photo_report.value: {
        COLUMN_TYPE_KEY: 'bool',
        COLUMN_NULLIFY_KEY: 'NULL',
    },
}

def create_table_definition_real_estate(insert_table_name: str, database_fields: List[str]) -> str:
    table = f"create table if not exists {insert_table_name} ("
    id_string = "id int NOT NULL AUTO_INCREMENT"
    primary_key_str = f"PRIMARY KEY (id) );"
    columns = [id_string]
    for normalized_name in database_fields:
        print(f"normalized_name = {normalized_name}")
        normalized_column_info: dict = COLUMN_INFO[normalized_name]
        type_of_column: str = normalized_column_info[COLUMN_TYPE_KEY]
        is_null: str = normalized_column_info[COLUMN_NULLIFY_KEY]
        columns.append(f"{normalized_name} {type_of_column} {is_null}")
    columns.append(primary_key_str)
    table = table + ','.join(columns)
    print(f"table = {table}")
    return table
