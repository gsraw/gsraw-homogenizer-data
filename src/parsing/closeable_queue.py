from collections.abc import Iterator
import multiprocessing as mp
import time
import parsing.fmp_queue as fmq
from parsing.sentinel import Sentinel


class ClosableQueue(Iterator):
    SENTINEL = Sentinel()

    def __init__(self):
        self.queue = mp.Queue()

    def close(self):
        self.queue.put(self.SENTINEL)

    def put(self, item):
        self.queue.put(item)

    def __next__(self):
        _start = time.time()
        item = self.queue.get()
        # print(f"Time for self.queue.get() = {(time.time() - _start)}")
        if self.SENTINEL == item:
            raise StopIteration
        return item


class ClosableFMQueue(Iterator):
    SENTINEL = Sentinel()

    def __init__(self, mp_queue: mp.Queue):
        self.queue = fmq.Queue(mp_queue)

    def put(self, item):
        self.queue.put(item)

    def get(self):
        return self.queue.get()

    def close(self):
        self.queue.close()

    def __next__(self):
        _start = time.time()
        item = self.queue.get()
        print(f"closable FMQ get = {(time.time() - _start)}")
        if self.SENTINEL == item:
            print("reached sentinel")
            raise StopIteration
        return item
