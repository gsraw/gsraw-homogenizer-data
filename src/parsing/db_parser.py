import concurrent.futures
import math
import multiprocessing as mp
import os
from time import time
from MySQLdb.cursors import SSDictCursor
import src.parsing.fmp_queue as fmq2
import src.parsing.closeable_queue as cfmq

from src.log import c_logger
from src.sql.batch_holder import BatcHolder
from src.sql.pool import Pool
# should not be src.parsing.sentinel
from parsing.sentinel import Sentinel
from src.parsing.seek import SeekQuery, SeekQueryTimeStamp


def uniqueifier(inq, outq):
    sentinel = Sentinel()
    seen = set()
    while True:
        i = inq.get()
        if i not in seen or sentinel == i:
            seen.add(i)
            outq.put(i)

def database_split_queue(maxi: int, table_name: str, fields: list, step_size: int, num_of_readers: int, id_col: str,
                         timestamp: bool = False):
    if timestamp:
        seek = SeekQueryTimeStamp(table_name=table_name, maxi=maxi, fields=fields, step_size=step_size,id_name=id_col)
    else:
        seek = SeekQuery(table_name=table_name, maxi=maxi, fields=fields, step_size=step_size,id_name=id_col)
    queue = cfmq.ClosableQueue()
    # queue = ClosableFMQueue()
    while True:
        if seek.is_threshold_surpassed():
            break
        queue.put(seek.get_next_batch())
    for i in range(0, num_of_readers):
        queue.close()
    return queue


def create_daemon_processes(n_proc: int, target, args: tuple) -> list:
    processes = []
    for j in range(n_proc):
        w_proc = mp.Process(target=target, args=args)
        w_proc.daemon = True
        processes.append(w_proc)
        w_proc.start()
    return processes


def await_processes(processes: list) -> None:
    for process in processes:
        process.join()
        print('writer joined')


def writer(in_queue: mp.Queue, pars_func, fields: list, batch_size: int, table_name: str):
    sentinel = Sentinel()
    # import path must be same as fmq_queue for sentinel, otherwise
    # class type not equal, type(sentinel)
    # Then the name is only equal type(items).__name__ == 'Sentinel':
    f_queue = fmq2.Queue(mp_queue=in_queue, maxsize=70)
    conn_pool = Pool(1)
    batch = BatcHolder(fields=fields, batch_size=batch_size, table_name=table_name, allow_duplicate=False)
    while True:
        items = f_queue.get()
        if sentinel == items:
            print(f"writer {os.getpid()} got sentinel")
            break
        for item in items:
            pars_func(row=item, batch=batch, conn_pool=conn_pool)
    # batch.batch.exec_query(conn_pool)
    print(f"WRITER CLOSING {os.getpid()}, shutting down future")
    batch.shutdown_future(conn_pool)
    print("WRITER past shutdown future")
    conn_pool.close()
    print("WRITER CLOSED")


def read(item, conn_pool: Pool, chunk: int, out_queue: mp.Queue) -> None:
    # _db_time = time()
    conn = conn_pool.get_connection()
    cursor = conn.cursor(cursorclass=SSDictCursor)
    cursor.execute(item)
    data = cursor.fetchall()
    cursor.close()
    # c_logger.debug(f"database time = {(time() - _db_time)}")
    conn_pool.release_connection(conn)
    new_rows = [data[x:x + chunk] for x in range(0, len(data), chunk)]
    for row in new_rows:
        # print(f"len input row = {len(row)}")
        # a_size = sys.getsizeof(row)
        # print('%d bytes, %dKB, %dMB' % (a_size, a_size / 1024, a_size / 1024 / 1024))
        out_queue.put(row)


def reader(in_queue: cfmq.ClosableQueue, out_queue: mp.Queue, chunk: int) -> None:
    t_pool = concurrent.futures.ThreadPoolExecutor(5)
    conn_pool = Pool(1)
    futures = []
    for item in in_queue:
        futures.append(t_pool.submit(read(item=item, conn_pool=conn_pool, chunk=chunk, out_queue=out_queue)))
        if len(futures) == 3:
            for future in concurrent.futures.as_completed(futures):
                future.done()
            futures = []
    if len(futures) > 0:
        for future in concurrent.futures.as_completed(futures):
            future.done()
        futures = []
    print("shutting down reader")
    # t_pool shutdown is not completely reliable
    t_pool.shutdown(wait=True)
    conn_pool.close()
    print("READER FINISHED")

# def reader(func, in_queue, out_queue,chunk : int):
#     # print(func is mysql_connection)
#     # print(func is emergency_connect)
#     # chunk = math.floor(step_size / num_of_readers)
#     # t_pool = concurrent.futures.ThreadPoolExecutor(5)
#     # futures = []
#     with func() as connection:
#         with connection.cursor(cursorclass=SSDictCursor) as c:
#             for item in in_queue:
#                 # _db_time = time()
#                 # c.execute("SELECT table_name FROM information_schema.tables where table_schema = 'crawling';")
#                 # futures.append(t_pool.submit())
#                 c.execute(item)
#                 rows = c.fetchall()
#                 # print(f"database time = {(time() - _db_time)}")
#                 # out_queue.put(rows)
#                 new_rows = [rows[x:x+chunk] for x in range(0, len(rows), chunk)]
#                 for row in new_rows:
#                     # print(f"len input row = {len(row)}")
#                     # a_size = sys.getsizeof(row)
#                     # print('%d bytes, %dKB, %dMB' % (a_size, a_size / 1024, a_size / 1024 / 1024))
#                     out_queue.put(row)
#                 # for row in rows:
#                 #     out_queue.put(row)
#     print("READER FINISHED")

# Reader from DB -> Input queue -> Parser uploads to DB -> output_queue Write to file.


def start(n_readers: int, n_writers: int, step_size: int, max_id: int,
          table_name: str, insert_table_name: str,
          fields: list, insert_fields: list, batch_size: int, parse_func,
          queue_bound: int = 70,
          id_col: str = 'id',
          timestamp: bool = False ):
    reader_in_queue = database_split_queue(maxi=max_id, fields=fields, step_size=step_size,
                                           num_of_readers=n_readers, table_name=table_name,
                                           id_col=id_col, timestamp=timestamp)
    # reader_out_queue = mp.Manager().Queue(queue_bound)
    reader_out_queue = mp.Queue(queue_bound)
    chunk = math.floor(step_size / n_readers)
    writers = create_daemon_processes(n_proc=n_writers, target=writer,
                                      args=(reader_out_queue, parse_func, insert_fields, batch_size, insert_table_name))
    _start = time()
    readers = create_daemon_processes(n_proc=(n_readers - 1), target=reader, args=(reader_in_queue,
                                                                                   reader_out_queue, chunk))
    reader(reader_in_queue, reader_out_queue, chunk=chunk)
    print("JOINING READERS")
    await_processes(readers)
    print("READERS JOINED")
    for i in range(n_writers):
        print(f"n_sentinel = {i}")
        reader_out_queue.put(Sentinel())
    print("JOINING WRITERS")
    await_processes(writers)
    print("WRITERS JOINED")
    print(f"completed in {(time() - _start)} seconds")


def start_fast():
    cpus = mp.cpu_count()
    n_readers = cpus + 2
    n_writers = cpus
