import queue as queue_
from threading import Thread
import _multiprocessing as _mp
import weakref

from parsing.sentinel import Sentinel


class Queue(object):

    def __init__(self, mp_queue, maxsize=0, debug=False):
        if maxsize <= 0:
            maxsize = _mp.SemLock.SEM_VALUE_MAX
        self.mpq = mp_queue
        self.qq = queue_.Queue(maxsize=maxsize)
        self.maxsize = maxsize
        Queue._steal_daemon(self.mpq, self.qq, self)
        self.debug = debug

    # def __del__(self):
    #     if self.debug:
    #         print('del')

    def put(self, item, block=True, timeout=None):
        """
        :param item:
        :param bool block:
        :param int timeout:
        :return:
        """
        self.mpq.put(item, block, timeout)

    def put_nowait(self, item):
        self.mpq.put_nowait(item)

    def get(self, block=True, timeout=None):
        """
        TODO: maybe support "block" and "timeout"
        :param bool block:
        :param int timeout:
        :return:
        """
        # print(f"size qq = {self.qq.qsize()}, size mpq = {self.mpq.qsize()}")
        return self.qq.get(block, timeout)

    def get_nowait(self):
        return self.qq.get_nowait()

    def qqsize(self):
        return self.qq.qsize()

    def qsize(self):
        """
        can be 2*(maxsize), because this is the sum of qq.size and mpq.size
        """
        return self.qq.qsize() + self.mpq.qsize()

    def empty(self):
        return self.qq.empty() and self.mpq.empty()

    def full(self):
        return self.qq.full() and self.mpq.full()

    def close(self):
        self.mpq.put(Sentinel())

    # static for not referencing "self" strongly
    # but only weakly-referencing "me"
    @staticmethod
    def _steal_daemon(srcq, dstq, me):
        # sentinel = object()
        sentinel = Sentinel()
        # sentinel_2 = 'Sentinel'

        def steal(srcq, dstq):
            while True:
                # block here
                obj = srcq.get()
                dstq.put(obj)
                # print(f'name={type(obj).__name__},'
                #       f'name_sen = {type(sentinel).__name__}'
                #       f'comp ={sentinel == obj}, '
                #       f'isistance=={isinstance(obj, Sentinel)},'
                #       f'comp{obj == sentinel},'
                #       f'type is {type(obj) is type(sentinel)},'
                #       f'type is {type(obj) is type(Sentinel())},'
                #       f'type == {type(obj) == type(sentinel)}')
                if sentinel == obj:
                    print("reached daemon putting sentinel")
                    break
                # if type(obj).__name__ == sentinel_2:
                #     print("reached daemon putting sentinel")
                #     dstq.put(sentinel)
                #     break

        # def steal(srcq, dstq, me_ref):
        #     while me_ref():
        #         # block here
        #         obj = srcq.get()
        #         if sentinel == obj:
        #             print("reached daemon putting sentinel")
        #             dstq.put(sentinel)
        #             break
        #         dstq.put(obj)
        #     dstq.put(sentinel)

        # def stop(ref):
        #     print("putting sentinel in source\n potential bug causer")
        #     # srcq.put(sentinel)

        # when the FastMyQueue object is GCed, stop the thread
        # by the stop() callback
        # me1 = weakref.ref(me, stop)
        # stealer = Thread(target=steal, args=(srcq, dstq, me1,))
        stealer = Thread(target=steal, args=(srcq, dstq))
        stealer.daemon = True
        stealer.start()