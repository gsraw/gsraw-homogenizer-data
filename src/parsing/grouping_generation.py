import xlsxwriter

import dates
from sql.CRUD import mysql_connection




def select_columns_of_table_query(table_name: str, database: str = 'crawling') -> str:
    return f"SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '{database}' AND TABLE_NAME = '{table_name}';"


def column_most_frequent_values(limit : int, column: str, table_name: str):
    return f"SELECT `{column}`, COUNT(`{column}`) AS `value_occurrence` FROM" \
           f"`{table_name}` GROUP BY `{column}` ORDER BY `value_occurrence` DESC LIMIT {limit};"


def get_parse_statistics(table_name: str, col_name: str ,date: str):
    return f"select {col_name} from {table_name} where week='{date}';"

class ExColumn:
    def __init__(self, col_name: str, values: list):
        self.col_name = col_name
        self.values = values

    def write_worksheet_to_oolumn(self,start_row: int, col: int, worksheet: xlsxwriter.workbook.Worksheet) -> int:
        worksheet.write(start_row,col,self.col_name)
        start_row += 1
        for value in self.values:
            worksheet.write(start_row, col, value[0])
            start_row += 1
        return start_row


def construct_grouping(table_name: str, limit: int, statistics_table_name: str, iso_date = dates.TodayIso.get()):
    all_columns: list = []
    all_statistics: list = []
    row = 0
    column = 0
    with xlsxwriter.Workbook(f'/home/joris/local/pythonProject/{table_name}_grouping.xlsx') as workbook:
        worksheet = workbook.add_worksheet('grouping')
        with mysql_connection() as conn:
            with conn.cursor() as c:
                c.execute(select_columns_of_table_query(table_name=table_name))
                data = c.fetchall()
                for d in data:
                    col_name = d[0]
                    c.execute(column_most_frequent_values(limit=limit,column=col_name,table_name=table_name))
                    all_columns.append(
                        ExColumn(col_name,c.fetchall())
                        )
                # workbook = xlsxwriter.Workbook(f'{table_name}_grouping.xlsx')
                for ex_col in all_columns:
                    ex_col.write_worksheet_to_oolumn(start_row=row,col=column,worksheet=worksheet)
                    column += 1

                statistics_worksheet = workbook.add_worksheet('statistics')
                c.execute(select_columns_of_table_query(table_name=statistics_table_name))
                data = c.fetchall()
                for d in data:
                    col_name = d[0]
                    c.execute(get_parse_statistics(statistics_table_name,col_name=col_name,date=iso_date))
                    all_statistics.append( ExColumn(col_name,c.fetchall()))

                column = 0
                for ex_col in all_statistics:
                    ex_col.write_worksheet_to_oolumn(start_row=row, col=column, worksheet=statistics_worksheet)
                    column += 1


construct_grouping(table_name='selency_fr_parsed_sit', limit = 10,statistics_table_name='selency_category_total', iso_date=dates.TodayIso.get() )

# then for each column name, we select the 10 most occurrences

