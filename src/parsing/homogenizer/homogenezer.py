from parsing.RealEstateObject import RealEstateData
from parsing.normalized_columns import NormalizedRealEstateColumns

class RealEstateCleaner:
    __slots__ = ()

    def clean_parent_id(self, address_column_val) -> str:
        raise NotImplementedError


    def clean_full_address(self, address_column_val) -> str:
        raise NotImplementedError


    def clean_construction_year(self, column_val: str) -> int:
        raise NotImplementedError


    def add_to_additional_fields(self, column_val: str, additional_field_json: dict, originial_column: str) -> None:
        raise NotImplementedError


    def clean_description(self, column_val: str) -> int:
        raise NotImplementedError


    def clean_original_admin2(self, column_val) -> float:
        raise NotImplementedError


    def clean_original_admin3(self, column_val: str) -> str:
        raise NotImplementedError


    def clean_original_admin4(self, column_val: str) -> str:
        raise NotImplementedError


    def clean_original_admin5(self, column_val: str) -> str:
        raise NotImplementedError


    def clean_energy_cert_id(self, column_val: str) -> str:
        raise NotImplementedError


    def clean_has_air_conditioner(self, column_val: str) -> bool:
        raise NotImplementedError


    def clean_has_elevator(self, column_val: str) -> bool:
        raise NotImplementedError


    def clean_has_garage(self, column_val: str) -> bool:
        raise NotImplementedError


    def clean_has_garage_included(self, column_val: str) -> bool:
        raise NotImplementedError

    def clean_has_common_zones(self, column_val: str) -> bool:
        raise NotImplementedError


    def clean_has_storage(self, column_val: str) -> bool:
        raise NotImplementedError


    def clean_has_pool(self, column_val: str) -> bool:
        raise NotImplementedError


    def clean_has_security(self, column_val: str) -> bool:
        raise NotImplementedError


    def clean_has_balcony(self, column_val: str) -> bool:
        raise NotImplementedError


    def clean_has_racket_zone(self, column_val: str) -> bool:
        raise NotImplementedError


    def clean_has_terrace(self, column_val: str) -> bool:
        raise NotImplementedError


    def clean_is_exact_address(self, column_val: str) -> bool:
        raise NotImplementedError


    def clean_is_exterior(self, column_val: str) -> bool:
        raise NotImplementedError


    def clean_lat(self, column_val: str) -> float:
        raise NotImplementedError


    def clean_lon(self, column_val: str) -> float:
        raise NotImplementedError


    def clean_n_baths(self, column_val: str) -> int:
        raise NotImplementedError


    def clean_n_floor(self, column_val: str) -> int:
        raise NotImplementedError


    def clean_floor(self, column_val: str) -> float:
        raise NotImplementedError


    def clean_n_rooms(self, column_val: str) -> int:
        raise NotImplementedError


    def clean_operation_type_id(self, column_val: str) -> int:
        raise NotImplementedError


    def clean_cardinal_direction_id(self, column_val: str) -> int:
        raise NotImplementedError


    def clean_listing_id(self, column_val: str) -> str:
        raise NotImplementedError


    def clean_appearance(self, column_val: str) -> str:
        raise NotImplementedError


    def clean_property_type_id(self, column_val: str) -> int:
        raise NotImplementedError


    def clean_usage_id(self, column_val: str) -> int:
        raise NotImplementedError


    def clean_last_update(self, column_val: str) -> str:
        raise NotImplementedError


    def clean_local_price(self, column_val: str) -> str:
        raise NotImplementedError


    def clean_contact_name(self, column_val: str) -> str:
        raise NotImplementedError


    def clean_agency_name(self, column_val: str) -> str:
        raise NotImplementedError


    def clean_area(self, column_val: str) -> int:
        raise NotImplementedError


    def clean_area_util(self, column_val: str) -> int:
        raise NotImplementedError


    def clean_conservation_id(self, column_val: str) -> int:
        raise NotImplementedError


    def build_status_id(self, column_val: str) -> int:
        raise NotImplementedError


    def clean_garage_price(self, column_val: str) -> int:
        raise NotImplementedError


    def title(self, column_val: str) -> str:
        raise NotImplementedError


    def url(self, column_val: str) -> str:
        raise NotImplementedError


    def detail_page(self, column_val: str) -> str:
        raise NotImplementedError


    def image_urls(self, column_val: str) -> str:
        raise NotImplementedError


    def portal_reference(self, column_val: str) -> str:
        raise NotImplementedError


    def contact_phone(self, column_val: str) -> str:
        raise NotImplementedError


    def is_urgent(self, column_val: str) -> bool:
        raise NotImplementedError

    def heating_type(self, column_val: str) -> str:
        raise NotImplementedError


    def agency_branch(self, column_val: str) -> str:
        raise NotImplementedError


    def agency_url(self, column_val: str) -> str:
        raise NotImplementedError


    def local_price(self, column_val: str) -> int:
        raise NotImplementedError

    def clean_id(self, column_val: int) -> int:
        return column_val

    def plan_available(self, column_val: bool) -> bool:
        return column_val


    def has_virtual_tour(self, column_val: bool) -> bool:
        return column_val


    def has_three_dim_tour(self, column_val: bool) -> bool:
        return column_val


    def clean_n_images(self, column_val: str) -> int:
        raise NotImplementedError


    def clean_parse_date(self, column_val: str) -> str:
        return column_val


    def clean_week(self, column_val: str) -> str:
        return column_val


    def clean_push_up(self, column_val: str) -> str:
        raise NotImplementedError


    def clean_highlighted(self, column_val: str) -> str:
        raise NotImplementedError


    def clean_has_developable(self, column_val: str) -> bool:
        raise NotImplementedError


    def clean_pet_friendly(self, column_val: str) -> bool:
        raise NotImplementedError

    def clean_original_admin1(self, listing: RealEstateData) -> bool:
        raise NotImplementedError

    def clean(self, listing: RealEstateData):
        listing.has_developable = self.clean_has_developable(listing)
        listing.push_up = self.clean_push_up(listing)
        listing.highlighted = self.clean_highlighted(listing)
        listing.parent_id = self.clean_parent_id(listing)
        listing.garage_price = self.clean_garage_price(listing)
        listing.n_images = self.clean_n_images(listing)
        listing.week = self.clean_has_developable(listing)
        listing.has_virtual_tour = self.has_virtual_tour(listing)
        listing.has_three_dim_tour = self.has_three_dim_tour(listing)
        listing.plan_available = self.plan_available(listing)
        listing.description = self.clean_description(listing)
        listing.agency_branch = self.agency_branch(listing)
        listing.agency_url = self.agency_url(listing)
        listing.heating_type = self.heating_type(listing)
        listing.local_price = self.clean_local_price(listing)
        listing.full_address = self.clean_full_address(listing)
        # listing.additional_fields = self.clean_has_developable(listing)
        listing.energy_cert_id = self.clean_energy_cert_id(listing)
        listing.has_air_conditioner = self.clean_has_air_conditioner(listing)
        listing.has_elevator = self.clean_has_elevator(listing)
        listing.has_garage = self.clean_has_garage(listing)
        listing.has_garage_included = self.clean_has_garage_included(listing)
        listing.has_common_zones = self.clean_has_common_zones(listing)
        listing.has_storage = self.clean_has_storage(listing)
        listing.has_pool = self.clean_has_pool(listing)
        listing.has_balcony = self.clean_has_balcony(listing)
        listing.has_racket_zone = self.clean_has_racket_zone(listing)
        listing.has_security = self.clean_has_security(listing)
        listing.has_terrace = self.clean_has_terrace(listing)
        listing.is_exact_address = self.clean_is_exact_address(listing)
        listing.is_exterior = self.clean_is_exterior(listing)
        listing.lat = self.clean_lat(listing)
        listing.lon = self.clean_lon(listing)
        listing.original_admin1 = self.clean_original_admin1(listing)
        listing.original_admin2 = self.clean_original_admin2(listing)
        listing.original_admin3 = self.clean_original_admin3(listing)
        listing.original_admin4 = self.clean_original_admin4(listing)
        listing.original_admin5 = self.clean_original_admin5(listing)
        listing.n_baths = self.clean_n_baths(listing)
        listing.floor = self.clean_floor(listing)
        listing.n_rooms = self.clean_n_rooms(listing)
        listing.operation_type_id = self.clean_operation_type_id(listing)
        listing.cardinal_direction_id = self.clean_cardinal_direction_id(listing)
        listing.listing_id = self.clean_listing_id(listing)
        listing.last_appearance = self.clean_appearance(listing)
        listing.property_type_id = self.clean_property_type_id(listing)
        listing.usage_id = self.clean_usage_id(listing)
        listing.last_update = self.clean_last_update(listing)
        listing.contact_name = self.clean_contact_name(listing)
        listing.agency_name = self.clean_agency_name(listing)
        listing.area = self.clean_area(listing)
        listing.area_util = self.clean_area_util(listing)
        listing.conservation_id = self.clean_conservation_id(listing)
        listing.build_status_id = self.build_status_id(listing)
        listing.title = self.title(listing)
        listing.detail_page = self.detail_page(listing)
        listing.contact_phone = self.contact_phone(listing)
        listing.portal_reference = self.portal_reference(listing)
        listing.is_urgent = self.is_urgent(listing)
