from typing import List, Tuple, Set, Dict
from nltk.tokenize import TweetTokenizer, sent_tokenize
from nltk import pos_tag
import nltk
from parsing.normalized_columns import NormalizedRealEstateColumns
nltk.download('punkt')
#TODO non_exact comparison, comparing word length and contains

tokenizer_words = TweetTokenizer()




def sentence_as_string(sentence: List[str]) -> str:
    """Z. comunitaria -> [ 'Z', '.', 'comunitaria']"""
    new_sentence = []
    for word in sentence:
        if word == '.':
            new_sentence[len(new_sentence) -1] += '.'
        else:
            new_sentence.append(word)
    return " ".join(new_sentence)


def look_for_negation_previous_word(sentence: List[str], word_index: int, negation_words: Set[str]) -> bool:
    # TODO improve using POS taggign to check if previous word does not suck.
    prev_word = sentence[word_index - 1].lower()
    return prev_word in negation_words


def find_word_in_sentence(detection_words: Set[str], sentence: List[str]) -> int:
    number_of_words = len(sentence)
    for i in range(number_of_words):
        word = sentence[i].lower()
        # print(f"word={word},sentence = {sentence}")
        if word in detection_words:
            return i
    return -1


def loop_over_sentence(some_word, sentence: List[str]):
    number_of_words = len(sentence)
    for i in range(number_of_words):
        word = sentence[i].lower()
        # print(f"word={word},sentence = {detection_words}, {word in detection_words}")
        if word == some_word:
            return i
    return -1


def multiple_words_in_sentence(word: str, sentence: List[str]) -> List[int]:
    words = word.split(' ')
    word_found: bool = True
    indices = []
    for s_word in words:
        index: int = loop_over_sentence(s_word.lower(), sentence)
        if index == -1:
            word_found = False
            break
        # print(f"index = {index}. words = {words}, sentence={sentence}")
        indices.append(index)
    if word_found:
        return indices
    return []


def find_multiple_words_in_sentence(detection_words: Set[str], sentence: List[str]) -> List[List[int]]:
    all_indices = []
    for word in detection_words:
        word = word.lower()
        indices = multiple_words_in_sentence(word,sentence)
        if len(indices) > 0:
            all_indices.append(indices)
    return all_indices



def find_word_in_sentence_as_string(detection_words: Set[str], sentence: str) -> int:
    for word in detection_words:
        index: int = sentence.find(word)
        if index != -1:
            return index
    return -1



def find_word_and_determine_proper(detection_words: Set[str], sentence: List[str], negation_words: Set[str]):
    index = find_word_in_sentence(detection_words,sentence)
    if index > -1:
        word_lowered = sentence[index].lower()
        return word_lowered in negation_words
    return False


def get_text_sentence_tokenized(input_text: str) -> List[List[str]]:
    return [tokenizer_words.tokenize(t) for t in sent_tokenize(input_text)]


def pos_tag_tokens(sentence: List[str]) -> List[Tuple[str, str]]:
    return pos_tag(sentence)
