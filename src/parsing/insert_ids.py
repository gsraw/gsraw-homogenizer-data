from src.sql.CRUD import mysql_connection

maxi = 82539
start = 1
MAX = maxi + 1
conn = mysql_connection()
cursor = conn.cursor()

table_name = 'z_test_run_suumo'
create_table = f"CREATE TABLE IF NOT EXISTS `{table_name}` (id int NOT NULL AUTO_INCREMENT, " \
               f"sc varchar(255) NULL , url varchar(255) NULL , " \
               f"count varchar(255) NULL , location_level_3 varchar(255) NULL , location_level_4 varchar(255) NULL," \
               f"num_of_urls varchar(255) NULL, operation varchar(255) NULL, province varchar(255) NULL," \
               f"html_serp LONGTEXT,"\
               f"PRIMARY KEY (id) )"


def insert_query(i:int):
    return f"UPDATE  joris_suumo_display_room\n"\
           f"SET     id = {i}\n"\
           f"WHERE   id = 0\n"\
           f"ORDER BY id\n"\
           f"LIMIT 1;"

print(insert_query(1))
cursor.execute(create_table)
conn.commit()





