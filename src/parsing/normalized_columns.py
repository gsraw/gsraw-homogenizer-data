import enum


class NormalizedRealEstateColumns(enum.Enum):
    push_up = 'push_up'
    pet_friendly = 'pet_friendly'
    has_developable = 'has_developable'
    highlighted = 'highlighted'
    parent_id = 'parent_id'
    garage_price = 'garage_price'
    week= 'week'
    parse_date = 'parse_date'
    agency_url= 'agency_url'
    agency_branch= 'agency_branch'
    heating_type = 'heating_type'
    id = 'id'
    listing_id = 'listing_id'
    full_address = 'full_address'
    construction_year = 'construction_year'
    additional_fields = 'additional_fields'
    description = 'description'

    energy_cert_id ='energy_cert_id'
    has_air_conditioner ='has_air_conditioner'
    has_elevator ='has_elevator'
    has_garage ='has_garage'
    has_garage_included ='has_garage_included'
    has_common_zones ='has_common_zones'
    has_storage ='has_storage'
    has_pool ='has_pool'
    has_security ='has_security'
    has_balcony ='has_balcony'
    has_racket_zone ='has_racket_zone'
    has_terrace ='has_terrace'
    is_exact_address ='is_exact_address'
    is_exterior ='is_exterior'
    lat ='lat'
    lon ='lon'
    original_admin1 = 'original_admin1'
    original_admin2 ='original_admin2'
    original_admin3 ='original_admin3'
    original_admin4 = 'original_admin4'
    original_admin5 ='original_admin5'
    n_baths ='n_baths'
    n_floor ='n_floor'
    floor = 'floor'
    n_rooms ='n_rooms'
    operation_type_id ='operation_type_id'
    cardinal_direction_id ='cardinal_direction_id'
    origin_id ='origin_id'
    last_appearance ='last_appearance'
    property_type_id ='property_type_id'
    usage_id ='usage_id'
    last_update ='last_update'
    local_price = 'local_price'
    portal_price_discount = 'portal_price_discount'
    contact_name = 'contact_name'
    agency_name = 'agency_name'
    area= 'area'
    area_util = 'area_util'
    conservation_id = 'conservation_id'
    build_status_id = 'build_status_id'
    title ='title'
    url = 'url'
    detail_page = 'detail_page'
    image_urls = 'image_urls'
    n_images = 'n_images'
    portal_reference ='portal_reference'
    contact_phone = 'contact_phone'
    is_urgent = 'is_urgent'
    plan_available = 'plan_available'
    has_three_dim_tour = 'has_three_dim_tour'
    has_virtual_tour = 'has_virtual_tour'
    vendor_type = 'vendor_type'
    postcode = 'postcode'
    listing_created_at = 'listing_created_at'
    has_photo_report = 'has_photo_report'