from typing import List
from collections.abc import Callable

import dates
from crawling_models.uniquefer import start_unique
from parsing import RealEstateObject
from parsing.RealEstateObject import create_table_definition_real_estate, RealEstateData
from sql.CRUD import mysql_connection
import multiprocessing as mp
import src.parsing.db_parser as test_fmq
from sql.batch_holder import BatcHolder
from sql.pool import Pool
from sql.table_naming import create_table_name


def choose_week(initial_parse_week, final_week: str) -> List[str]:
    """assumed proper checking has been done"""
    print("Please wait while program define periods to parse")
    if final_week is None:
        return [initial_parse_week]
    else:
        print("Initial parse week is not empty. In this point, we construct weeks to process")
        temp_week = dates.TodayIso.get_previous_week(final_week)
        week_list: List[str] = [final_week]
        while temp_week != initial_parse_week:
            week_list.append(temp_week)
            temp_week = dates.TodayIso.get_previous_week(temp_week)
        week_list.append(temp_week)
        print("Week defined: " + str(week_list) + "\n")
        return week_list


# def test_choose_week():
#     assert choose_week(None) == [dates.TodayIso.get()]
#     assert choose_week('2021w47') == [dates.TodayIso.get()]
#     assert choose_week('2021w44') == [dates.TodayIso.get(), '2021w46', '2021w45', '2021w44']


def start_parsing_sequence_new(target: str, parse_type: str, week:str,
                               create_table_func,
                               insert_fields: List[str],
                               parser: Callable[[dict, BatcHolder, Pool], None],
                               NUM_OF_READERS: int = 3, STEP_SIZE = 100, fields=None,
                           debug=False,
                           timestamp: bool = False,
                           id_col: str = 'id',
                           ) -> None:
    table_name = create_table_name(target=target,
                                   type=parse_type,
                                   parsed=False,
                                   week=week)
    print("connecting database")
    with mysql_connection() as co:
        with co.cursor() as cu:
            cu.execute(f"SELECT count(*) FROM {table_name}")
            MAX = cu.fetchone()[0]
    insert_table_name = create_table_name(target=target,
                                          type=parse_type,
                                          parsed=True,
                                          week=week)
    create_parsed_table_query = create_table_func(insert_table_name)
    if fields is None:
        fields = ['*']
    cpus = mp.cpu_count()
    if cpus > 6:
        cpus = 6
    # cpus = 2
    # cpus = 1
    NUM_OF_WRITERS = cpus
    if debug:
        NUM_OF_READERS = 1
        NUM_OF_WRITERS = 1

    print("creating table")
    with mysql_connection() as temp_con:
        with temp_con.cursor() as temp_cursor:
            temp_cursor.execute(create_parsed_table_query)
            temp_con.commit()
    print("starting sequence")
    test_fmq.start(n_readers=NUM_OF_READERS,
                   n_writers=NUM_OF_WRITERS,
                   fields=fields,
                   insert_fields= insert_fields,
                   batch_size=1024,
                   table_name=table_name,
                   insert_table_name=insert_table_name,
                   step_size= STEP_SIZE,
                   max_id=MAX,
                   parse_func=parser,
                   timestamp=timestamp,
                   id_col=id_col)

def start_parsing_real_estate(target: str, parse_type: str,
                              parser: Callable[[dict, BatcHolder, Pool], None],
                              final_week: str,
                              initial_week: str,
                              unique: bool,
                              NUM_OF_READERS: int = 3,
                              STEP_SIZE = 100,
                              fields = None,
                              debug = False,
                              timestamp: bool = False,
                              id_col: str = 'id',
    ) -> None:

    used_fields = [s for s in RealEstateData.__slots__]
    def create_table_func(insert_table_name: str):

        return create_table_definition_real_estate(
            database_fields=used_fields,
            insert_table_name=insert_table_name
        )
    weeks = choose_week(initial_week,
                        final_week)

    for week in weeks:
        insert_table_name = create_table_name(target=target,
                                              type=parse_type,
                                              parsed=True,
                                              week=week)
        start_parsing_sequence_new(
            create_table_func=create_table_func,
            target=target,
            insert_fields=used_fields,
            parse_type=parse_type,
            NUM_OF_READERS=NUM_OF_READERS,
            STEP_SIZE=STEP_SIZE,
            fields=fields,
            debug=debug,
            timestamp=timestamp,
            id_col=id_col,
            parser=parser,
            week=week
        )
        if unique:
            start_unique(target=target, select_table_name=insert_table_name)


def start_parsing_sequence(table_name: str, insert_table_name: str, create_parsed_table_query: str,
                           insert_fields: List[str],
                           parser: Callable[[dict, BatcHolder, Pool], None],
                           NUM_OF_READERS: int = 3, STEP_SIZE = 100, fields=None,
                           debug=False,
                           timestamp: bool = False,
                           id_col: str = 'id'):
    if fields is None:
        fields = ['*']
    cpus = mp.cpu_count()
    if cpus > 6:
        cpus = 6
    # cpus = 2
    # cpus = 1
    NUM_OF_WRITERS = cpus
    if debug:
        NUM_OF_READERS = 1
        NUM_OF_WRITERS = 1


    print("connecting database")
    with mysql_connection() as co:
        with co.cursor() as cu:
            cu.execute(f"SELECT count(*) FROM {table_name}")
            MAX = cu.fetchone()[0]
    print("creating table")
    with mysql_connection() as temp_con:
        with temp_con.cursor() as temp_cursor:
            temp_cursor.execute(create_parsed_table_query)
            temp_con.commit()
    print("starting sequence")
    test_fmq.start(n_readers=NUM_OF_READERS,
                   n_writers=NUM_OF_WRITERS,
                   fields=fields,
                   insert_fields= insert_fields,
                   batch_size=1024,
                   table_name=table_name,
                   insert_table_name=insert_table_name,
                   step_size= STEP_SIZE,
                   max_id=MAX,
                   parse_func=parser,
                   timestamp=timestamp,
                   id_col=id_col)
