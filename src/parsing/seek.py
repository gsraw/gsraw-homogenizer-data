from src.sql.pool import Pool


class SeekQuery:
    def __init__(self, step_size: int, fields: list, table_name: str, maxi: int, conn_pool: Pool = None,
                 id_name: str = 'id', start_at: int = 0):
        self.step_size = step_size
        self.id_name = id_name
        self.boundary = step_size
        self.fields = fields
        self.table_name = table_name
        self.prev = start_at
        self.conn_pool = conn_pool
        self.maxi = maxi

    def _create_query(self, id_index: int, id_name: str) -> str:
        cols = ','.join(self.fields)
        return f"SELECT {cols} FROM {self.table_name} WHERE {id_name} > {id_index} LIMIT {self.boundary};"

    def get_next_batch(self) -> str:
        query = self._create_query(self.prev, self.id_name)
        self.prev += self.boundary
        return query

    def is_threshold_surpassed(self):
        return self.prev > self.maxi

    def query_next_page(self):
        conn = self.conn_pool.get_connection()
        cursor = conn.cursor()
        cursor.execute(self.get_next_batch())
        cursor.close()
        self.conn_pool.release_connection(conn)

class SeekQueryTimeStamp:
    def __init__(self, step_size: int, fields: list, table_name: str, maxi: int, conn_pool: Pool = None,
                 id_name: str = 'timestamp', start_at: int = 0):
        self.step_size = step_size
        self.id_name = id_name
        self.boundary = step_size
        self.fields = fields
        self.table_name = table_name
        self.prev = start_at
        self.conn_pool = conn_pool
        self.maxi = maxi

    def _create_query(self, id_index: int, id_name: str) -> str:
        cols = ','.join(self.fields)
        return f"SELECT {cols} FROM {self.table_name} ORDER BY {id_name} DESC LIMIT {id_index}, {self.boundary};"

    def get_next_batch(self) -> str:
        query = self._create_query(self.prev, self.id_name)
        self.prev += self.boundary
        return query

    def is_threshold_surpassed(self):
        return self.prev > self.maxi

    def query_next_page(self):
        conn = self.conn_pool.get_connection()
        cursor = conn.cursor()
        cursor.execute(self.get_next_batch())
        cursor.close()
        self.conn_pool.release_connection(conn)
