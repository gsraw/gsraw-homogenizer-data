# #!/usr/bin/env python3
# import sys
#
# sys.path = [
#     '/home/ec2-user/FTCA',
#     '/usr/lib64/python37.zip',
#     '/usr/lib64/python3.7',
#     '/usr/lib64/python3.7/lib-dynload',
#     '/home/ec2-user/FTCA/myenv/lib64/python3.7/site-packages',
#     '/home/ec2-user/FTCA/myenv/lib/python3.7/site-packages',
# ]

from cysimdjson import JSONParser
from src.crawler.vinted_fr.batch_holder import BatcHolder
from src.sql.CRUD import mysql_connection
from src.sql.pool import Pool
import multiprocessing as mp
from src.log import get_logger
import parsing.db_parser as test_fmq


def lazy_construct_sql(listing: dict, table_name: str):
    keys_cols = list(listing.keys())
    insert_query = f"INSERT INTO {table_name} ({','.join([key for key in keys_cols])}) VALUES"
    create_table = f"CREATE TABLE IF NOT EXISTS `{table_name}` ("
    next_part = ' varchar(255) NULL,'.join(keys_cols)
    create_table += next_part + ' varchar(255) NULL)'
    return insert_query,create_table,len(keys_cols)


def new_listing():
    return {
        'city': '',
        'id': '',
        'catalog_id': '',
        'city_id': '',
        'created_at': '',
        'created_at_ts': '',
        'price': '',
        'price_numeric': '',
        'promoted': '',
        'promoted_until': '',
        'updated_at_ts': '',
        'title': '',
        'url': '',
        'user_id': '',
        'view_count': '',
        'country_id': '',
        'parsed_date': ''
    }


def parser(row: dict, batch: BatcHolder, conn_pool: Pool):
    parser = JSONParser()
    # self.logger.debug(f"id = {row['id']}")
    # print(f"id = {row['id']}")
    try:
        item_json = parser.parse(row['item_json'])
        items = item_json.at_pointer("/items")

        for item in items:
            listing = {
                'city': item.at_pointer("/city"),
                'id': item.at_pointer("/id"),
                'catalog_id': item.at_pointer("/catalog_id"),
                'city_id': item.at_pointer("/city_id"),
                'created_at': item.at_pointer("/created_at"),
                'created_at_ts': item.at_pointer("/created_at_ts"),
                'price': item.at_pointer("/price"),
                'price_numeric': item.at_pointer("/price_numeric"),
                'promoted': item.at_pointer("/promoted"),
                'promoted_until': item.at_pointer("/promoted_until"),
                'updated_at_ts': item.at_pointer("/updated_at_ts"),
                'title': item.at_pointer("/title"),
                'url': item.at_pointer("/url"),
                'user_id': item.at_pointer("/user_id"),
                'view_count': item.at_pointer("/view_count"),
                'country_id': row['country_ids'],
                'parsed_date': row['c_created_time']
            }
            batch.insert_listing_in_database_bounded(listing=listing, conn_pool=conn_pool)
            #  batch.ins
    except Exception as e:
        pass
        # self.logger.debug(traceback.format_exc())

cpus = mp.cpu_count()
NUM_OF_READERS = 6
NUM_OF_WRITERS = 2
STEP_SIZE = 100
MAX = 67875
table_name =  'vinted_run_21w11_FR_men_shoes_new_second'
insert_query,create_table,len_col = lazy_construct_sql(new_listing(),'z_test11')
with mysql_connection() as temp_con:
    with temp_con.cursor() as temp_cursor:
        temp_cursor.execute(create_table)
        temp_con.commit()

fields = ['*']
logger = get_logger('test', "parse.log")

test_fmq.start(n_readers=NUM_OF_READERS,
               n_writers=NUM_OF_WRITERS,
               fields=fields,
               insert_fields= list(new_listing().keys()),
               batch_size=1024,
               table_name=table_name,
               step_size= STEP_SIZE,
               max_id=MAX,
               parse_func=parser)
