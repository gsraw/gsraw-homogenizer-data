# #!/usr/bin/env python3
# import sys
#
# sys.path = [
#     '/home/ec2-user/FTCA',
#     '/usr/lib64/python37.zip',
#     '/usr/lib64/python3.7',
#     '/usr/lib64/python3.7/lib-dynload',
#     '/home/ec2-user/FTCA/myenv/lib64/python3.7/site-packages',
#     '/home/ec2-user/FTCA/myenv/lib/python3.7/site-packages',
# ]
import html
from lxml.cssselect import CSSSelector
from src.crawler.vinted_fr.batch_holder import BatcHolder
from src.sql.pool import Pool
from html_functionality.html_local import read_into_html, lxml_el_to_bytes_to_string
from src.sql.CRUD import mysql_connection
import src.parsing.db_parser as test_fmq
from src.text_preprocessing import get_all_float, remove_all_identation


def empty_listing():
    return {'propertyType': '',
            'isNew': '',
            'price': '',
            'price_numeric': 0.0,
            'title': '',
            'rooms': '',
            'area': '',
            'detailPage': '',
            'sellerName': '',
            'sellerPhone': '',
            'sellerUrl': '',
            'c_operation': '',
            'c_prefecture': '',
            'c_location_level_3': '',
            'c_location_level_4': ''
            }

def get_element(str_el, tag, end_tag) -> str:
    index = str_el.find(end_tag)
    index_first = str_el.find(tag)
    if index_first != -1 and index != -1:
        ind = slice(index_first + len(tag), index)
        return str_el[ind]
    return 'not found'


def get_element_f(str_el, end_tag) -> str:
    index = str_el.find(end_tag)
    index_first = str_el.find('>')
    if index_first != -1 and index != -1:
        ind = slice(index_first + 1, index)
        return str_el[ind]
    return 'not found'


def parser(row: dict, batch : BatcHolder, conn_pool : Pool):
    operation = row.get('operation')
    url = row['url']
    sel_bukken = CSSSelector('div[id*="bukkenList"]  div[class*="cassetLink"]')
    sel_property = CSSSelector('td[class*="detailbox-property--col3"]')
    sel_div_property = CSSSelector('div')
    sel_title = CSSSelector('h2[class*="title"] a')
    sel_price = CSSSelector('div[class*="detailbox-property-poin"]')
    sel_rooms = CSSSelector('td[class*="detailbox-property--col3"] div')
    sel_detail_page = CSSSelector('h2[class*="title"] a')
    sel_seller_url = CSSSelector('div[class*="detailnote-box-item"] a')
    sel_seller_phone = CSSSelector('div[class*="detailnote-box-item"] div[class*="bold"]')

    if operation == 'ikkodate' or operation == 'chuko' or operation == 'chukoikkodate':

        sel_articles_ikko = CSSSelector('div[class*="property_unit "]')
        sel_title_ikko = CSSSelector('h2[class*="title"] a')
        sel_price_ikko = CSSSelector('dt[plaintext*="販売価格"]')
        sel_rooms_ikko = CSSSelector('dt[plaintext*="間取り"]')
        sel_area_ikko = CSSSelector('dt[plaintext*="建物面積"]')
        sel_prices_rooms_area_ikko = CSSSelector('dl')
        sel_detail_page_ikko = CSSSelector('h2[class*="title"] a')
        sel_seller_name_ikko = CSSSelector('div[class*="shopmore-title"]')
        sel_seller_phone_ikko = CSSSelector('span[class*="makermore-tel"]')
        ikkodate_elements_ikko = read_into_html(row['html_serp'])

        for el in sel_articles_ikko(ikkodate_elements_ikko):
            listing = empty_listing()
            if operation == 'ikkodate':
                listing['propertyType'] = 'house'
                listing['isNew'] = '1'
            elif operation == 'chukoikkodate':
                listing['propertyType'] = 'house'
                listing['isNew'] = '0'
            elif operation == 'chuko':
                listing['propertyType'] = 'flat'
                listing['isNew'] = '0'

            listing['price'] = ''
            listing['title'] = ''
            listing['rooms'] = ''
            listing['area'] = ''
            listing['detailPage'] = ''
            listing['sellerName'] = ''
            listing['sellerPhone'] = ''
            listing['c_operation'] = operation
            listing['c_prefecture'] = row['province']
            listing['c_location_level_3'] = row['location_level_3']
            listing['c_location_level_4'] = row['location_level_4']

            titles = sel_title_ikko(el)
            if len(titles) > 0:
                title_ikko = lxml_el_to_bytes_to_string(titles[0])
                listing['title'] = get_element_f(title_ikko, '</a>')

            container_rooms_area = sel_prices_rooms_area_ikko(el)

            price_japan = '販売価格'
            rooms_japan = '間取り'
            area_japan = '建物面積'
            area_2_japan = '専有面積'
            cel_dt = CSSSelector('dt')
            cel = CSSSelector('dd')
            cel_price = CSSSelector('dd span')
            for dl in container_rooms_area:
                dts = cel_dt(dl)
                if len(dts) > 0:
                    dt = dts[0]
                    temp_dt = lxml_el_to_bytes_to_string(dt)

                    if temp_dt.find(price_japan) != -1:
                        price_byte_el = lxml_el_to_bytes_to_string(cel_price(dl)[0])
                        listing['price'] = get_element_f(price_byte_el, '</span>')
                        if listing['price'].find('万円') != -1:
                            listing['price_numeric'] = float(get_all_float(listing['price'])[0]) * 10000

                    elif temp_dt.find(rooms_japan) != -1:

                        rooms_byte_el = lxml_el_to_bytes_to_string(cel(dl)[0])

                        listing['rooms'] = get_element_f(rooms_byte_el, '</dd>')
                    elif temp_dt.find(area_japan) != -1 or temp_dt.find(area_2_japan) != -1:

                        area_byte_el = lxml_el_to_bytes_to_string(cel(dl)[0])

                        listing['area'] = get_element_f(area_byte_el, '</dd>').split('m<sup>2</sup>')[0]

                    else:
                        pass
            detail_page_ar = sel_detail_page_ikko(el)
            if len(detail_page_ar) > 0:
                detail_page_ikko = detail_page_ar[0].get('href')
                listing['detailPage'] = detail_page_ikko

            seller_names_ikko = sel_seller_name_ikko(el)
            if len(seller_names_ikko) > 0:
                seller_name_ikko = lxml_el_to_bytes_to_string(seller_names_ikko[0])
                listing['sellerName'] = remove_all_identation(
                    html.unescape(get_element_f(seller_name_ikko, '</div>')))

            seller_phones_ikko = sel_seller_phone_ikko(el)
            if len(seller_phones_ikko) > 0:
                seller_phone_ikko = lxml_el_to_bytes_to_string(seller_phones_ikko[0])

                listing['sellerPhone'] = remove_all_identation(get_element_f(seller_phone_ikko, '</span>'))
            batch.insert_listing_in_database_bounded(listing=listing,conn_pool=conn_pool)

    if operation == 'chintai':
        # all_items = CSSSelector('div[class="cassetteitem"]')
        elements_html = read_into_html(row['html_serp'])
        elements = sel_bukken(elements_html)
        for el in elements:
            listing = empty_listing()
            listing['propertyType'] = ''
            property_type = sel_property(el)

            if len(property_type) > 0:
                property_type_el = property_type[0]
                property_type_el_arr = sel_div_property(property_type_el)
                if len(property_type_el_arr) > 0:
                    string = lxml_el_to_bytes_to_string(property_type_el_arr[0])
                    listing['propertyType'] = get_element(string, '<div>', '</div>')

            listing['title'] = ''
            titles = sel_title(el)
            if len(titles) > 0:
                title_el_text = lxml_el_to_bytes_to_string(titles[0])
                listing['title'] = get_element_f(title_el_text, '</a>')
            listing['price'] = ''
            listing['price_numeric'] = None
            price = sel_price(el)
            if len(price) > 0:
                price_el_text = lxml_el_to_bytes_to_string(price[0])
                # 万円
                listing['price'] = get_element_f(price_el_text, '</div>')
                if listing['price'].find('万円') != -1:
                    listing['price_numeric'] = float(get_all_float(listing['price'])[0]) * 10000

            listing['rooms'] = ''
            listing['area'] = ''
            rooms = sel_rooms(el)
            if len(rooms) > 0:
                room_el_text = lxml_el_to_bytes_to_string(rooms[0])
                listing['rooms'] = get_element(room_el_text, '<div>', '</div>')
                if len(rooms) > 1:
                    area_el_text = lxml_el_to_bytes_to_string(rooms[1])
                    listing['area'] = get_element(area_el_text, '<div>', '<sup>2</sup>')

            listing['detailPage'] = ''
            delail_page_arr = sel_detail_page(el)
            if len(el) > 0:
                listing['detailPage'] = delail_page_arr[0].get('href')

            listing['sellerUrl'] = ''
            listing['sellerName'] = ''
            seller_url_el = sel_seller_url(el)
            if len(seller_url_el) > 0:
                seller = seller_url_el[0]
                seller_el_text = lxml_el_to_bytes_to_string(seller)
                listing['sellerUrl'] = seller.get('href')
                listing['sellerName'] = get_element_f(seller_el_text, '</a>')

            listing['sellerPhone'] = ''
            seller_phone_arr = sel_seller_phone(el)
            if len(seller_phone_arr):
                seller_phone_el_text = lxml_el_to_bytes_to_string(seller_phone_arr[0])
                listing['sellerPhone'] = remove_all_identation(
                    get_element_f(html.unescape(seller_phone_el_text), '</div>'))

            listing['c_operation'] = operation
            listing['c_prefecture'] = row['province']
            listing['c_location_level_3'] = row['location_level_3']
            listing['c_location_level_4'] = row['location_level_4']
            batch.insert_listing_in_database_bounded(listing=listing,conn_pool=conn_pool)






columns_as_keys = empty_listing()

keys_cols = list(columns_as_keys.keys())
insert_table_name = 'z_test_suumo'

insert_query = f"INSERT INTO {insert_table_name} ({','.join([key for key in keys_cols])}) VALUES"

create_table = f"CREATE TABLE IF NOT EXISTS `{insert_table_name}` ( id int NOT NULL AUTO_INCREMENT," \
               f"propertyType varchar(255) NULL , title varchar(255) NULL , " \
               f"price varchar(255) NULL , price_numeric varchar(255) NULL , rooms varchar(255) NULL," \
               f"area varchar(255) NULL, detailPage varchar(255) NULL, sellerUrl varchar(255) NULL," \
               f"sellerName varchar(255) NULL, sellerPhone varchar(255) NULL, c_operation varchar(255) NULL," \
               f"c_prefecture varchar(255) NULL, c_location_level_3 varchar(255) NULL, c_location_level_4 varchar(255) NULL," \
               f"isNew varchar(255) NULL," \
               f"PRIMARY KEY (id) )"
NUM_OF_READERS = 6
NUM_OF_WRITERS = 2
STEP_SIZE = 20
MAX = 6911
table_name =  'z_test_run_suumo'
len_col = len(keys_cols)
with mysql_connection() as temp_con:
    with temp_con.cursor() as temp_cursor:
        temp_cursor.execute(create_table)
        temp_con.commit()
fields = ['*']
test_fmq.start(n_readers=NUM_OF_READERS,
               n_writers=NUM_OF_WRITERS,
               fields=fields,
               insert_fields=list(empty_listing().keys()),
               batch_size=1024,
               table_name=table_name,
               step_size= STEP_SIZE,
               max_id=MAX,
               parse_func=parser)



