import os
#from mysql.connector import connect, Error
import MySQLdb
from MySQLdb.cursors import SSDictCursor
# DATABASE_NAME = os.environ.get('DATABASE_NAME')
# DATABASE_USER =  os.environ.get('DATABASE_USER')
# DATABASE_PASS =  os.environ.get('DATABASE_PASS')
# DATABASE_HOST =  os.environ.get('DATABASE_HOST')
# DATABASE_PORT =  os.environ.get('DATABASE_PORT')

# DATABASE_NAME='crawling'
# DATABASE_USER='daybat'
# DATABASE_PASS='B3daybataws'
# DATABASE_HOST='daybat-pre.cl9ghfou7czj.eu-west-2.rds.amazonaws.com'
# DATABASE_PORT=3306

DATABASE_NAME='crawling'
DATABASE_USER='joris'
DATABASE_PASS='jorisjoris'
DATABASE_HOST='joris-emergency.cl9ghfou7czj.eu-west-2.rds.amazonaws.com'
DATABASE_PORT=3306


def mysql_connection() -> MySQLdb.Connection:
    return MySQLdb.connect(
        host=DATABASE_HOST,
        user=DATABASE_USER,
        password=DATABASE_PASS,
        database=DATABASE_NAME,
        charset='utf8',
        # use_unicode=True
    )


def get_dict_cursor(conn: MySQLdb.Connection) -> SSDictCursor:
    return conn.cursor(SSDictCursor)


def emergency_connect():
    print("emergency connect")
    dbname = 'crawling'
    db_us = 'joris'
    db_pass = 'jorisjoris'
    db_h = 'joris-emergency.cl9ghfou7czj.eu-west-2.rds.amazonaws.com'
    # DATABASE_PORT = 3306
    return MySQLdb.connect(
        host=db_h,
        user=db_us,
        password=db_pass,
        database=dbname,
        charset='utf8'
    )

def unai_connection():
    dbhost = 'daybat.cl9ghfou7czj.eu-west-2.rds.amazonaws.com'
    dbname = 'crawling'
    username = 'unai'
    password = 'B3daybataws'
    return MySQLdb.connect(
        host=dbhost,
        user=username,
        password=password,
        database=dbname,
        charset='utf8'
    )


get_database_connection = {
    "emergency": emergency_connect,
    "unai_prod": unai_connection,
    'environment_connection': mysql_connection
}


def table_exists(connection,table,schema):
    query = f"SELECT count(*) FROM information_schema.TABLES WHERE TABLE_SCHEMA = '{schema}' AND TABLE_NAME = '{table}'"
    cursor = connection.cursor()
    cursor.execute(query)
    data = cursor.fetchone()
    connection.commit()
    cursor.close()
    print(data)
    return data[0] > 0
