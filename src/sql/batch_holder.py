import time

from data_model.crawling_data_model import CrawlDataModel, IndependentVariable
from m_threading_async.bounded_executor import BoundedExecutorAsync,BoundedExecutor
from sql.pool import Pool
from src.sql.sql_batch_handler import SqlBatchHandler


class BatcHolder:
    def __init__(self, fields: list, table_name: str,batch_size: int, conn_pool: Pool = None, allow_duplicate: bool = True,
                 id_column: str = 'listing_id'):
        #TODO add futures list
        self.fields = fields
        self.batch_size = batch_size
        self.allow_duplicate = allow_duplicate
        self.table_name = table_name
        self.conn_pool = conn_pool
        self.batch = SqlBatchHandler(fields,batch_size=batch_size,table_name=table_name, allow_duplicate=allow_duplicate,
                                     id_column=id_column)
        self.bound_thread_pool = BoundedExecutorAsync(bound=10, max_workers=4)
        self.bound_future_pool = BoundedExecutor(bound=0, max_workers= 6)
        self.id_column = id_column

    def shutdown_future(self, pool: Pool, wait = True):
        self.bound_future_pool.shutdown(wait=wait)
        self.batch.exec_query(pool)

    def shutdown_async(self, pool: Pool, wait = True):
        self.bound_thread_pool.shutdown(wait=wait)
        self.batch.exec_query(pool)

    def insert_listing_in_database_bounded(self, listing: dict, conn_pool):
        self.batch.append_dict_row(row_dict=listing)
        if self.batch.is_batch_limit():
            #TODO add futures and explicited call as completed after threshold
            # self.batch.exec_query(conn_pool)
            self.bound_future_pool.submit(exec_wrapper, self.batch, conn_pool)
            self.batch = SqlBatchHandler(fields=self.fields, batch_size=self.batch_size,
                                         table_name=self.table_name, allow_duplicate=self.allow_duplicate,
                                         id_column=self.id_column)

    def insert_listing_in_database_future(self, listing: dict, conn_pool: Pool):
        self.batch.append_dict_row(row_dict=listing)
        if self.batch.is_batch_limit():
            self.batch.exec_query(conn_pool)
            self.batch = SqlBatchHandler(fields=self.fields, batch_size=self.batch_size,table_name=self.table_name,
                                         allow_duplicate=self.allow_duplicate)

    def insert_multiple_listings(self, listings: list, conn_pool):
        for listing in listings:
            self.batch.append_dict_row(row_dict=listing)
        if self.batch.is_batch_limit():
            self.batch.exec_query(conn_pool)
            self.batch = SqlBatchHandler(fields=self.fields, batch_size=self.batch_size,table_name=self.table_name
                                         , allow_duplicate=self.allow_duplicate,
                                         id_column=self.id_column)

    async def insert_listing_in_database_async(self, listing: dict, conn_pool):
        self.batch.append_dict_row(row_dict=listing)
        if self.batch.is_batch_limit():
            awaitable = self.bound_thread_pool.submit(exec_wrapper, self.batch, conn_pool)
            self.batch = SqlBatchHandler(fields=self.fields, batch_size=self.batch_size, table_name=self.table_name,
                                         allow_duplicate=self.allow_duplicate,
                                         id_column=self.id_column)
            return await awaitable

    async def insert_data_model_in_database_async(self, listing: CrawlDataModel, conn_pool):
        self.batch.append_data_model(row=listing)
        if self.batch.is_batch_limit():
            awaitable = self.bound_thread_pool.submit(exec_wrapper, self.batch, conn_pool)
            self.batch = SqlBatchHandler(fields=self.fields, batch_size=self.batch_size, table_name=self.table_name,
                                         allow_duplicate=self.allow_duplicate,
                                         id_column=self.id_column)
            return await awaitable

    async def insert_data_model_in_database_async_internal(self, listing: CrawlDataModel):
        self.batch.append_data_model(row=listing)
        if self.batch.is_batch_limit():
            awaitable = self.bound_thread_pool.submit(exec_wrapper, self.batch, self.conn_pool)
            self.batch = SqlBatchHandler(fields=self.fields, batch_size=self.batch_size, table_name=self.table_name,
                                         allow_duplicate=self.allow_duplicate,
                                         id_column=self.id_column)
            return await awaitable

    def insert_data_model_in_database_bounded(self, listing: IndependentVariable, conn_pool):
        self.batch.append_data_model_insert(row=listing)
        if self.batch.is_batch_limit():
            #TODO add futures and explicited call as completed after threshold
            # self.batch.exec_query(conn_pool)
            self.bound_future_pool.submit(exec_wrapper, self.batch, conn_pool)
            self.batch = SqlBatchHandler(fields=self.fields, batch_size=self.batch_size,
                                         table_name=self.table_name, allow_duplicate=self.allow_duplicate,
                                         id_column=self.id_column)

    def insert_data_model_in_new(self, listing, conn_pool):
        self.batch.append_data_model_new(row=listing)
        if self.batch.is_batch_limit():
            # TODO add futures and explicited call as completed after threshold
            # self.batch.exec_query(conn_pool)
            self.bound_future_pool.submit(exec_wrapper, self.batch, conn_pool)
            self.batch = SqlBatchHandler(fields=self.fields, batch_size=self.batch_size,
                                         table_name=self.table_name, allow_duplicate=self.allow_duplicate,
                                         id_column=self.id_column)

def exec_wrapper(batch: SqlBatchHandler, pool: Pool):
    batch.exec_query(pool)
