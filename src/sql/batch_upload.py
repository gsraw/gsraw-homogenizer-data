import asyncio
import threading
from queue import Queue
from typing import Union, List, Tuple
from concurrent.futures import ThreadPoolExecutor, Future
from data_model.crawling_data_model import CrawlDataModel, IndependentVariable
from m_threading_async.bounded_executor import BoundedExecutor
from src.sql.pool import Pool, AliveConnection, SimpleConnectionPoolExecutor, UnboundedSimpleConnectionPool


# Each thread has it's own threadLocal batchHolder
#

class BatchUpload:
    """Should be used only for a specific query that is repeated."""

    __slots__ = ('query_values',
                 'allow_duplicate',
                 'table_name',
                 'fields',
                 'num_cols',
                 'len_col',
                 'batch_size',
                 '__parametrized_query',
                 '__simple_pool'
                 )
    query_values: list
    allow_duplicate: bool
    table_name: str
    fields: List[str]
    num_cols: int
    len_col: int
    batch_size: int
    __parametrized_query: str
    __simple_pool: UnboundedSimpleConnectionPool

    def __init__(self,
                 dummy_dynamic_data: Union[CrawlDataModel, IndependentVariable],
                 table_name: str,
                 batch_size: int,
                 simple_pool: UnboundedSimpleConnectionPool,
                 allow_duplicate: bool = True):
        self.query_values = []
        self.allow_duplicate = allow_duplicate
        self.table_name = table_name
        self.fields = dummy_dynamic_data.append_to_list(self.query_values)
        self.len_col = len(self.fields)
        self.num_cols = 0
        self.batch_size = batch_size
        self.__parametrized_query = self.__create_parametrized_query()
        self.__simple_pool = simple_pool

    def __create_insert_query(self):
        cols = ",".join([s for s in self.fields])
        insert = f'INSERT INTO {self.table_name}'
        return insert + "(" + cols + ")" + "VALUES"

    def __create_parametrized_query(self) -> str:
        str_1 = ",".join([self.__parametrize_insert() for _ in range(0, self.batch_size)])
        query: str = self.__create_insert_query() + " " + str_1
        if not self.allow_duplicate:
            query += f' ON DUPLICATE KEY UPDATE {self.table_name}.listing_id = {self.table_name}.listing_id;'
        return query

    def __parametrize_insert(self) -> str:
        str_1 = ",".join(['%s' for _ in range(0, self.len_col)])
        return "(" + str_1 + ")"

    def append_dict_row(self, row_dict: dict) -> None:
        "Assumes row_dict has exactly same keys as self.fields, Order is not important."
        # if len(row_dict.keys()) != self.len_col:
        #     c_logger.debug(f'len(row_dict.keys()) != self.len_col\nrow_dict={row_dict}')
        assert len(row_dict.keys()) == self.len_col
        for key in self.fields:
            self.query_values.append(row_dict[key])
        self.num_cols += 1

    def append_data_model(self, row: Union[CrawlDataModel, IndependentVariable]) -> None:
        row.append_to_list(self.query_values)
        self.num_cols += 1

    def is_batch_limit(self) -> bool:
        return self.num_cols >= self.batch_size

    def exec_query(self) -> None:
        if self.num_cols != 0:
            a_conn = self.__simple_pool.get_connection()
            # print(f"query len = {len(self.query_values)}")
            # print(f"query values = {self.query_values}")
            a_conn.cursor.execute(self.__parametrized_query, self.query_values)
            a_conn.connection.commit()
            self.num_cols = 0
            self.query_values = []

    def exec_query_return_self(self) -> "BatchUpload":
        if self.num_cols != 0:
            a_conn = self.__simple_pool.get_connection()
            # print(f"query len = {len(self.query_values)}")
            # print(f"query values = {self.query_values}")
            a_conn.cursor.execute(self.__parametrized_query, self.query_values)
            a_conn.connection.commit()
            self.num_cols = 0
            self.query_values = []
        return self


class AsyncBatchUploader:
    """not thread-safe, should by called only in async environment's main thread."""
    __slots__ = ('__event_loop',
                 '__unbounded_simple_connection_pool',
                 '__semaphore',
                 '__thread_pool',
                 '__current_batch',
                 '__batches')
    __unbounded_simple_connection_pool: UnboundedSimpleConnectionPool
    __current_batch: BatchUpload
    __batches: List[BatchUpload]
    __thread_pool: ThreadPoolExecutor
    __semaphore: asyncio.BoundedSemaphore

    def __init__(self,
                 event_loop,  # asyncio.get_event_loop(),
                 max_connections: int,
                 dummy_dynamic_data: Union[CrawlDataModel, IndependentVariable],
                 table_name: str,
                 batch_size: int,
                 simple_pool: UnboundedSimpleConnectionPool,
                 allow_duplicate: bool = True):
        self.__event_loop = event_loop
        self.__unbounded_simple_connection_pool = simple_pool
        self.__batches = []
        self.__current_batch = BatchUpload(dummy_dynamic_data,
                                           table_name,
                                           batch_size,
                                           simple_pool,
                                           allow_duplicate)
        self.__semaphore = asyncio.BoundedSemaphore(max_connections)
        self.__thread_pool = ThreadPoolExecutor(max_connections)

        for i in range(1, max_connections):
            self.__batches.append(BatchUpload(dummy_dynamic_data,
                                              table_name,
                                              batch_size,
                                              simple_pool,
                                              allow_duplicate
                                              )
                                  )

    async def insert_data_model(self, data: Union[CrawlDataModel, IndependentVariable]):
        with self.__semaphore:
            self.__current_batch.append_data_model(data)
            if self.__current_batch.is_batch_limit():
                batch: BatchUpload = self.__current_batch
                # Always present, guaranteed by locking of semaphore.
                self.__current_batch = self.__batches.pop()
                await self.__event_loop.run_in_executor(self.__thread_pool,
                                                        batch.exec_query
                                                        )
                self.__batches.append(batch)


class BatchUploader:
    """not thread-safe, main thread should only call batchUploader"""
    __slots__ = ('__thread_pool',
                 '__current_batch',
                 '__batches')
    __current_batch: BatchUpload
    __batches: Queue[BatchUpload]
    __thread_pool: BoundedExecutor

    def __init__(self,
                 max_connections: int,
                 dummy_dynamic_data: Union[CrawlDataModel, IndependentVariable],
                 table_name: str,
                 batch_size: int,
                 simple_pool: UnboundedSimpleConnectionPool,
                 allow_duplicate: bool = True):
        self.__batches = Queue(max_connections)
        self.__current_batch = BatchUpload(dummy_dynamic_data,
                                           table_name,
                                           batch_size,
                                           simple_pool,
                                           allow_duplicate)
        self.__thread_pool = BoundedExecutor(bound=0, max_workers=max_connections)
        for i in range(1, max_connections):
            self.__batches.put(BatchUpload(dummy_dynamic_data,
                                           table_name,
                                           batch_size,
                                           simple_pool,
                                           allow_duplicate
                                           )
                               )

    async def insert_data_model(self, data: Union[CrawlDataModel, IndependentVariable]):
        self.__current_batch.append_data_model(data)
        if self.__current_batch.is_batch_limit():
            batch: BatchUpload = self.__current_batch
            self.__current_batch = self.__batches.get()
            future = self.__thread_pool.submit(batch.exec_query_return_self())
            future.add_done_callback(lambda fut: self.__batches.put(fut.result()))
