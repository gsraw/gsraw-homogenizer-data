import asyncio
import concurrent
import time
from typing import Dict, List, Any
import MySQLdb
from src.sql.CRUD import mysql_connection
from src.log import c_logger
import threading
from concurrent.futures import ThreadPoolExecutor

lock = threading.Lock()


class Pool:
    # TODO
    # Register to auto close on exit.
    def __init__(self, size: int):
        self._available = []
        self._in_use = {}
        self.size = size

    def get_connection(self):
        with lock:
            if len(self._available) != 0:
                conn = self._available.pop(0)
                self._in_use[conn] = conn
                return conn
            else:
                new_conn = mysql_connection()
                self._in_use[new_conn] = new_conn
                return new_conn

    def release_connection(self, conn):
        with lock:
            self._available.append(conn)
            del self._in_use[conn]

    def _create_connections(self):
        for i in range(0, self.size):
            self._available.append(mysql_connection())

    def close(self, max_wait_timeout = 100):
        # Threadpool shutdown does not seem to be so reliable regarding connections when waiting
        # for all the threads to finish.
        # TODO make exponential
        t_out: int = 0
        fixed_sleep: int = 5
        while t_out > max_wait_timeout:
            if (len(self._in_use)) > 0:
                c_logger.critical(f"SQL_pool close is called, however not empty yet, {len(self._in_use)}")
                t_out += fixed_sleep
                time.sleep(fixed_sleep)
            else:
                break
        if t_out > max_wait_timeout:
            raise Exception("time out exception waiting on getting all connections back")
        for conn in self._available:
            conn.close()


class AtomicInteger:
    """In compiled languages atomic actually exists........."""
    __slots__ = ('_lock', 'val')

    def __init__(self, value: int):
        """Initialize a new atomic counter to given initial value (default 0)."""
        self.val = value
        self._lock = threading.Lock()

    def get_and_set(self, value: int):
        """Atomically increment the counter by num (default 1) and return the
        new value.
        """
        with self._lock:
            self.val += value
            return self.val

    def get(self):
        with self._lock:
            return self.val


# class SynchronousQueue(object):
#
#     def __init__(self):
#         self.q = Queue(1)
#         self.put_lock = RLock()
#
#     def get(self):
#         value = self.q.get(block=True)
#         self.q.task_done()
#         return value
#
#     def put(self, item):
#         with self.put_lock:
#             self.q.put(item, block=True)
#             self.q.join()


class AliveConnection:
    __slots__ = ("cursor", 'connection')

    def __init__(self, connection: MySQLdb.Connection):
        self.connection = connection
        self.cursor = connection.cursor()

    def close(self):
        self.cursor.close()
        self.connection.close()


class UnboundedSimpleConnectionPool:

    __slots__ = ('_available',
                 '_in_use',
                 'lock')
    _available: List[AliveConnection]
    _in_use: Dict[int, AliveConnection]
    lock: threading.Lock

    def __init__(self):
        self._available = []
        self._in_use = {}
        self.lock = threading.Lock()

    def get_connection(self) -> AliveConnection:
        with lock:
            if len(self._available) != 0:
                conn: AliveConnection = self._available.pop()
                self._in_use[conn] = conn
                return conn
            else:
                new_conn = AliveConnection(mysql_connection())
                self._in_use[new_conn] = new_conn
                return new_conn

    def release_connection(self, conn) -> None:
        with lock:
            self._available.append(conn)
            del self._in_use[conn]

    def close(self, max_wait_timeout = 100) -> None:
        # Threadpool shutdown does not seem to be so reliable regarding connections when waiting
        # for all the threads to finish.
        # TODO make exponential
        t_out: int = 0
        fixed_sleep: int = 5
        while t_out > max_wait_timeout:
            if (len(self._in_use)) > 0:
                c_logger.critical(f"SQL_pool close is called, however not empty yet, {len(self._in_use)}")
                t_out += fixed_sleep
                time.sleep(fixed_sleep)
            else:
                break
        if t_out > max_wait_timeout:
            raise Exception("time out exception waiting on getting all connections back")
        for conn in self._available:
            conn.close()


class SimpleConnectionPoolExecutor:
    """The program runs in asyncio module, therefore in our program we can
        keep an internal size limited executor and restricts its access by
        using a semaphore bounded by max_size.

        GIL also prevents threads from running in parallel in Python. Furthermore, database connections cannot
        be shared between processes. It follows we will actually never use any form
        of connection pooling for more than a single core.
        """
    __slots__ = ('__semaphore',
                 '__in_use',
                 '__size',
                 '__available',
                 '__thread_pool',
                 '__max_size',
                 '__lock')
    __semaphore: threading.BoundedSemaphore
    __in_use: Dict[int, AliveConnection]
    __available: List[AliveConnection]
    size: AtomicInteger
    __max_size: int
    __thread_pool: ThreadPoolExecutor
    # class variables
    thread_local = threading.local()

    def __init__(self, max_size: int):
        self.__semaphore = threading.BoundedSemaphore(max_size)
        self.__available = []
        self.__in_use = {}
        # for debugging purposes
        self.__size = AtomicInteger(max_size)
        self.__max_size = max_size
        self.__thread_pool = ThreadPoolExecutor(max_workers=max_size)
        self.__create_initial_connections()
        self.__lock = threading.Lock()
        # initialize all connections

    def __create_initial_connections(self):
        for i in range(0, self.__max_size):
            self.__available.append(
                AliveConnection(
                    mysql_connection()
                )
            )

    async def submit(self, loop, fn, *args) -> Any:
        """First argument should MySqlDbConnection"""
        self.__semaphore.acquire()
        self.__lock.acquire()
        # Semaphore guarantees there is always an available.
        conn: AliveConnection = self.__available.pop()
        self.__lock.release()
        try:
            return await loop.run_in_executor(self.__thread_pool, fn, conn, *args)
        finally:
            self.__lock.acquire()
            self.__available.append(conn)
            self.__lock.release()
            self.__semaphore.release()

    """See concurrent.futures.Executor#shutdown"""
    def shutdown(self, wait=True):
        self.__thread_pool.shutdown(wait)

    def get_max_size(self) -> int:
        return self.__max_size

# class BoundedConnectionPool:
#     __slots__ = ('r_enq_lock', 'r_deq_lock', 'not_empty_condition',
#                  'not_full_condition', 'size', 'capacity')
#
#     state_in_use = ''
#     state_available = ''
#
#     # Hashmap store not needed, can contain the mark
#     # lazy synchronized list adaption, mark stays until outsider de_marks
#     # If connection fails outside, create update_function, new connection
#     # get a connection, stays in List but is marked
#
#     # TODO add thread_local list, first thread local afterwards others.
#
#     def __init__(self, _capacity: int):
#         self.r_enq_lock: threading.RLock = threading.RLock()
#         self.r_deq_lock: threading.RLock = threading.RLock()
#         self.not_empty_condition: threading.Condition = threading.Condition(lock=self.r_deq_lock)
#         self.not_full_condition: threading.Condition = threading.Condition(lock=self.r_enq_lock)
#         self.size: AtomicInteger = AtomicInteger(0)
#         self.capacity: int = _capacity
#         self.shared_list = []
#
#     def get(self):
#         l = 16
#         connections = []
#         # self.not_empty_condition.acquire()
#         self.r_enq_lock.acquire()
#         try:
#
#             while self.size == self.capacity:
#                 self.not_full_condition.wait()
#             for item in self.shared_list:
#               # getAndSet item.marked
#                 if not item.marked:
#                     return connections
#         finally:
#             self.r_enq_lock.release()
#
#     # def get(self, connection):
#     #     must_wake_dequeuers = False

