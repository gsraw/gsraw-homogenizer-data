import sys


def get_size_of_bytes_in_mb(_bytes: bytes) -> float:
    return sys.getsizeof(_bytes) / (1024*1024)

def get_size_of_bytes_in_kb(_bytes: bytes) -> float:
    return sys.getsizeof(_bytes) / 1024