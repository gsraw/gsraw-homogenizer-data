from data_model.crawling_data_model import CrawlDataModel, IndependentVariable
from src.sql.pool import Pool


class SqlBatchHandler:

    def __init__(self, fields: list, table_name: str,
                 batch_size: int,
                 allow_duplicate: bool = True,
                 id_column: str = 'listing_id'):
        self.query_values = []
        self.allow_duplicate = allow_duplicate
        self.table_name = table_name
        self.fields = fields
        self.insert_query = SqlBatchHandler.create_insert_query(fields=fields,table_name=table_name)
        self.len_col = len(fields)
        self.num_cols = 0
        self.batch_size = batch_size
        self.id_column = id_column

    @staticmethod
    def create_insert_query(fields: list, table_name: str):
        cols = ",".join([s for s in fields])
        insert = f'INSERT INTO {table_name}'
        return insert + "(" + cols + ")" + "VALUES"

    def _parametrize_insert(self) -> str:
        str_1 = ",".join(['%s' for _ in range(0, self.len_col)])
        return "(" + str_1 + ")"

    def append_dict_row(self, row_dict: dict) -> None:
        "Assumes row_dict has exactly same keys as self.fields, Order is not important."
        # if len(row_dict.keys()) != self.len_col:
        #     c_logger.debug(f'len(row_dict.keys()) != self.len_col\nrow_dict={row_dict}')
        # for key in self.fields:
        #     if not (key in row_dict):
        #         print(f"unmatched key = {key}")
        # print(f"len = {len(row_dict.keys())}")
        # print(f"len = {self.len_col}")
        assert len(row_dict.keys()) == self.len_col
        for key in self.fields:
            self.query_values.append(row_dict[key])
        self.num_cols += 1

    def append_data_model_insert(self, row: IndependentVariable):
        row.append_to_list(self.query_values)
        self.num_cols += 1

    def append_data_model_new(self, row):
        row.append_to_list(self.fields, self.query_values)
        self.num_cols += 1

    def append_data_model(self, row: CrawlDataModel):
        row.append_to_list(self.query_values)
        self.num_cols += 1

    def is_batch_limit(self) -> bool:
        return self.num_cols >= self.batch_size

    def exec_query(self, pool: Pool) -> None:
        if self.num_cols != 0:
            str_1 = ",".join([self._parametrize_insert() for _ in range(0, self.num_cols)])
            query = self.insert_query + " " + str_1
            if not self.allow_duplicate:
                query += f' ON DUPLICATE KEY UPDATE {self.table_name}.{self.id_column} = {self.table_name}.{self.id_column};'
            conn = pool.get_connection()
            # print(query)
            cursor = conn.cursor()
            # print(f"query len = {len(self.query_values)}")
            # print(f"query values = {self.query_values}")
            cursor.execute(query, self.query_values)
            conn.commit()
            self.num_cols = 0
            self.query_values = []
            pool.release_connection(conn)

    def exec_query_cursor(self, conn, cursor) -> None:
        if self.num_cols != 0:
            str_1 = ",".join([self._parametrize_insert() for _ in range(0, self.num_cols)])
            query = self.insert_query + " " + str_1
            cursor.execute(query, self.query_values)
            conn.commit()
            self.num_cols = 0
            self.query_values = []


def exec_wrapper(batch: SqlBatchHandler, pool: Pool) -> None:
    batch.exec_query(pool)
