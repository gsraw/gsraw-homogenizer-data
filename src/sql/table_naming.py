ALLOWED_TYPE_VALUES = frozenset(
    ('serp',
     'dp',
     'seller',
     'street',
     'tree',
     'clean'
     )
)


def create_table_name(target, week, type, parsed: bool):
    if type in ALLOWED_TYPE_VALUES:
        if parsed:
            return f"{target}_{type}_internal_parsed_{week}"
        return f"{target}_{type}_internal_{week}"
    raise Exception(f"create_table_name called with type = {type}\n"
                    f"allowed types = {ALLOWED_TYPE_VALUES}")