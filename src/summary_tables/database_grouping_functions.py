

def unique_table(table_name: str):
    return f"(select * from {table_name} group by listing_id) t1"

def select_columns_of_table_query(table_name: str, database: str = 'crawling') -> str:
    return f"SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '{database}' " \
           f"AND TABLE_NAME = '{table_name}';"


def column_most_frequent_values(limit : int, column: str, table_name: str,status: str) -> str:
    if status is not None:
        return f"SELECT `{column}`, COUNT(`{column}`) AS `value_occurrence` FROM" \
               f"{table_name} where status = '{status}' GROUP BY `{column}` ORDER BY `value_occurrence` DESC LIMIT {limit};"
    else:
        return f"SELECT `{column}`, COUNT(`{column}`) AS `value_occurrence` FROM" \
               f" `{table_name}` GROUP BY `{column}` ORDER BY `value_occurrence` DESC LIMIT {limit};"


def distinct_column_values(column: str, table_name: str, status: str) -> str:
    if status is not None:
        return f"SELECT COUNT(distinct({column})) FROM {table_name} where status = '{status}';"
    else:
        return f"SELECT COUNT(distinct({column})) FROM {table_name};"


def total_non_null_entries(column: str, table_name: str, status: str) -> str:
    if status is not None:
        return f"SELECT COUNT({column}) FROM {table_name} where status = '{status}';"
    else:
        return f"SELECT COUNT({column}) FROM {table_name};"

def total_listings(table_name: str, status: str = None) -> str:
    if status is not None:
        return f"SELECT COUNT(*) FROM {table_name} where status = '{status}';"
    else:
        return f"SELECT COUNT(*) FROM {table_name};"


def null_count(column: str, table_name: str, status: str) -> str:
    if status is not None:
        return f"SELECT COUNT(*) FROM {table_name} where ({column} IS NULL or CONVERT({column},NCHAR) = '') AND status = '{status}';"
    else:
        return f"SELECT COUNT(*) FROM {table_name} where ({column} IS NULL or CONVERT({column},NCHAR) = '');"


def zero_count(column: str, table_name: str, status: str) -> str:
    if status is not None:
        return f"SELECT COUNT(*) FROM {table_name} where CONVERT({column},NCHAR) = '0' and status = '{status}';"
    else:
        return f"SELECT COUNT(*) FROM {table_name} where CONVERT({column},NCHAR) = '0';"
