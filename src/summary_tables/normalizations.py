from sql.table_naming import create_table_name
from summary_tables.summary_normalization_grouping import summarize_table_to_excel

# target = 'hbca_unique'
# week= '2021w40'
# limit = 10
# table_name = create_table_name(target=target,
#                   parsed=True,
#                   type='serp',
#                   week=week)
#
# summarize_table_to_excel(table_name=table_name, limit=limit, file_path='hbca.xslx')

# target = 'ftca_unique'
# week= '2021w40'
# limit = 10
# table_name = create_table_name(target=target,
#                   parsed=True,
#                   type='serp',
#                   week=week)
#
# summarize_table_to_excel(table_name=table_name, limit=limit, file_path='ftca.xslx')
#
# target = 'idee_unique'
# week= '2021w40'
# limit = 10
# table_name = create_table_name(target=target,
#                   parsed=True,
#                   type='serp',
#                   week=week)
#
# summarize_table_to_excel(table_name=table_name, limit=limit, file_path='idee.xslx')


# target = 'booli_unique'
# week= '2021w40'
# limit = 10
# table_name = create_table_name(target=target,
#                   parsed=True,
#                   type='serp',
#                   week=week)
#
# summarize_table_to_excel(table_name=table_name, limit=limit, file_path='booli.xslx')

# target = 'hmnt'
# week= '2021w40'
# limit = 10
# table_name = create_table_name(target=target,
#                   parsed=True,
#                   type='serp',
#                   week=week)
#
# summarize_table_to_excel(table_name=table_name, limit=limit, file_path='hmnt.xslx')

# target = 'boneo_unique'
# week= '2021w40'
# limit = 10
# table_name = create_table_name(target=target,
#                   parsed=True,
#                   type='serp',
#                   week=week)
#
# summarize_table_to_excel(table_name=table_name, limit=limit, file_path='boneo.xslx')




# target = 'propdad_unique'
# week= '2021w40'
# limit = 10
# table_name = create_table_name(target=target,
#                   parsed=True,
#                   type='serp',
#                   week=week)
#
# summarize_table_to_excel(table_name=table_name, limit=limit, file_path='propiedades.xslx')


# im24

target = 'im24'
week= '2021w39'
limit = 10
table_name = create_table_name(target=target,
                               parsed=True,
                               type='serp',
                               week=week)

summarize_table_to_excel(table_name=table_name, limit=limit, file_path=f'{target}.xslx')
