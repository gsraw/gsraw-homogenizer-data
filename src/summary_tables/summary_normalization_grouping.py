import xlsxwriter
from sql.CRUD import mysql_connection
from summary_tables.database_grouping_functions import select_columns_of_table_query, total_listings, \
    column_most_frequent_values, distinct_column_values, total_non_null_entries, null_count, zero_count


def summarize_table_to_excel(table_name: str, limit: int, file_path: str, statuses = None):
    """
    :param target: The target
    :param table_name: the table name to group
    :param limit: the number of values shown from the table
    :param statuses: Optional, if you want to separate on Status column. I.E SELC had published vs sold_out.
    :param file_path: the file you want to write to, i.e example.xlsx
    :return: None
    """
    first_row = 0
    first_column = 0
    example_value_col_name = 'ex values'

    if statuses is None:
        statuses = [None]
    for status in statuses:
        all_columns = []
        with mysql_connection() as co:
            with co.cursor() as c:
                c.execute(select_columns_of_table_query(table_name=table_name))
                data = c.fetchall()
                c.execute(total_listings(table_name=table_name,status=status))
                t_listings = c.fetchone()[0]
                for d in data:
                    col_name = d[0]
                    print(f"col_name = {col_name}")
                    c.execute(column_most_frequent_values(limit=limit, column=col_name, table_name=table_name,status=status))
                    col_data = c.fetchall()
                    c.execute(distinct_column_values(table_name=table_name,column=col_name,status=status))
                    col_unique_count = c.fetchone()[0]

                    c.execute(total_non_null_entries(table_name=table_name, column=col_name,status=status))
                    col_total_entries = c.fetchone()[0]
                    c.execute(null_count(table_name=table_name, column=col_name,status=status))
                    null_count_items = c.fetchone()[0]

                    c.execute(zero_count(table_name=table_name, column=col_name,status=status))
                    zero_count_items = c.fetchone()[0]
                    all_columns.append(
                        {
                            'name' : col_name,
                            'data' : col_data,
                            't_listings' : t_listings,
                            'distinct_count': col_unique_count,
                            'total_count': col_total_entries,
                            'null_count': null_count_items,
                            'zero_count': zero_count_items
                        }
                    )
        workbook = xlsxwriter.Workbook(file_path)
        worksheet = workbook.add_worksheet()
        row = first_row
        column = first_column
        # iterating through content list
        for s_dict in all_columns:
            next_column = column + 1
            example_value_column = next_column + 1
            row = first_row
            for key in s_dict:
                row_value = s_dict[key]
                if key == 'name':
                    worksheet.write(row, column, row_value)
                    row += 1
                elif key == 'data':
                    t_row: int = 0
                    worksheet.write(t_row, example_value_column, example_value_col_name)
                    for d in row_value:
                        t_row += 1
                        worksheet.write(t_row, example_value_column, d[0])
                else:
                    worksheet.write(row, next_column, row_value)
                    worksheet.write(row, column, key)
                    row += 1
            column += 3
        workbook.close()