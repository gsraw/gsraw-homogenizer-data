import dates
from sql.CRUD import mysql_connection
from sql.table_naming import create_table_name
from summary_tables.database_grouping_functions import select_columns_of_table_query, total_listings, \
    column_most_frequent_values, distinct_column_values, total_non_null_entries, null_count, zero_count


def generate_grouping(table_name: str, limit: int, file_path: str, statuses = None):
    """

    :param target: The target
    :param table_name: the table name to group
    :param limit: the number of values shown from the table
    :param statuses: Optional, if you want to separate on Status column. I.E SELC had published vs sold_out.
    :param file_path: the file you want to write to.
    :return: None
    """
    if statuses is None:
        statuses = [None]
    for status in statuses:
        all_columns = []
        with mysql_connection() as co:
            with co.cursor() as c:
                c.execute(select_columns_of_table_query(table_name=table_name))
                data = c.fetchall()
                c.execute(total_listings(table_name=table_name,status=status))
                t_listings = c.fetchone()[0]
                for d in data:
                    col_name = d[0]
                    print(f"col_name = {col_name}")
                    c.execute(column_most_frequent_values(limit=limit, column=col_name, table_name=table_name,status=status))
                    col_data = c.fetchall()
                    c.execute(distinct_column_values(table_name=table_name,column=col_name,status=status))
                    col_unique_count = c.fetchone()[0]

                    c.execute(total_non_null_entries(table_name=table_name, column=col_name,status=status))
                    col_total_entries = c.fetchone()[0]
                    c.execute(null_count(table_name=table_name, column=col_name,status=status))
                    null_count_items = c.fetchone()[0]

                    c.execute(zero_count(table_name=table_name, column=col_name,status=status))
                    zero_count_items = c.fetchone()[0]
                    all_columns.append(
                        {
                            'name' : col_name,
                            'data' : col_data,
                            't_listings' : t_listings,
                            'distinct_count': col_unique_count,
                            'total_count': col_total_entries,
                            'null_count': null_count_items,
                            'zero_count': zero_count_items
                        }
                    )
        with open(file_path,'w+') as file:
            print('writing')
            file.write(f"{table_name}\n")
            file.write(f"----------------------------------\n")
            for s_dat in all_columns:
                file.write(f"COLUMN: {s_dat['name'].upper()}\n"
                           f"{s_dat['name']} unique items: {s_dat['distinct_count']}\n"
                           f"{s_dat['name']} total rows in db: {s_dat['t_listings']}\n"
                           f"{s_dat['name']} total items with value (not null): {(s_dat['total_count'])}\n"
                           f"{s_dat['name']} zero (0) items: {s_dat['zero_count']}\n"
                           f"{s_dat['name']} null or empty ('') items: {s_dat['null_count']}\n"
                           f"{s_dat['name']} {limit} values and count of occurrence\n"
                           f"---------------------------------------------\n"
                           )
                for d in s_dat['data']:
                    file.write(f"VALUE: {d[0]}, COUNT: {d[1]}\n")

                file.write(f"\n---------------------------------------------\n \n")

