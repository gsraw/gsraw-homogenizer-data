import json
import math
import re
import urllib
from typing import Union
from urllib.parse import parse_qs

from unidecode import unidecode

def get_single_digit_bounded_by_word(txt : str) -> int:
    digit = [int(s) for s in txt.split() if s.isdigit()]
    return digit[0]


def get_digits(txt : str) -> list:
    return re.findall(r'\d+', txt)


def has_digits(txt: str) -> bool:
    return len(get_digits(txt)) > 0

def concat_all_digits(txt: str) -> int:
    return int("".join(get_digits(txt)))


def get_single_digit(txt : str) -> int:
    return int(re.findall(r'\d+', txt)[0])


def remove_comma(txt : str) -> str:
    return txt.replace(',', '')


def remove_dot(txt : str) -> str:
    return txt.replace('.', '')


def remove_all_identation(txt : str) -> str:
    txt = txt.replace('\t', '')
    txt = txt.replace('\r', '')
    txt = txt.replace('  ', '')
    return txt.replace('\n', '')


def remove_all_whitespace(txt : str) -> str:
    txt = remove_all_identation(txt)
    return txt.replace(' ','')


def get_all_float(txt : str) -> list:
    return re.findall(r"[-+]?\d*\.\d+|\d+", txt)


def dict_to_url(key_params : Union[dict,tuple]) -> str:
    return urllib.parse.urlencode(key_params)


def dict_to_str(d: dict) -> str:
    return json.dumps(d)

def str_to_dict(s : str) -> dict:
    return json.loads(s)

def url_param_to_dict(url_query: str) -> dict:
    """Does not work out of the box for multiple values. However, there is no
    official standard for multiple parameter support. This is different per webframework that do allow it."""
    # print(url_param_to_dict('type=bostad&start=0&limit=20&sort=field_property_publish_date&order=desc'))
    return {k: v[0] for k, v in parse_qs(url_query).items()}
    # type=bostad&start=0&limit=20&sort=field_property_publish_date&order=desc


def ceil_two_decimals(num : float):
    return math.ceil(num * 100) / 100


def floor_two_decimals(num : float):
    return math.floor(num * 100) / 100

def json_string_replace_double_backward_slash(json_str : str):
    # According to json.org, only the following escape are valid
    #
    # \"
    # \\
    # /
    # \b
    # \f
    # \n
    # \r
    # \t
    # \u four-hex-digits
    # https://stackoverflow.com/questions/10480148/json-string-decoding-encountering-invalid-escape
    # []
    escape_validator = r'''\\[^bfnrtu\\]'''
    json_str = re.sub(escape_validator, '', json_str)
    escape_validator_2 = '''[\b\f\n\r\t]'''
    s_list = ['\b', '\f', '\n', '\r', '\t']
    all_ = re.findall(escape_validator_2,json_str )
    for str_ in all_:
        json_str = json_str.replace(str_, "")
    return json_str
    # return json_str.replace('\\', '\\\\')


def normalize_characters(txt: str):
    """ Björn, Łukasz and Σωκράτης.  ==> Bjorn, Lukasz and Sokrates."""
    return unidecode(txt)



if __name__ == "__main__":
    print(concat_all_digits('6.149'))