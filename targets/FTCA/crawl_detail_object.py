from data_model.crawling_data_model import CrawlDataModel, FixedCrawlData

class FixedData(FixedCrawlData):
    __slots__ = ('listing_id', 'week')
    listing_id: str
    week: str


    def __init__(self, listing_id: str, week:str):
        super(FixedData, self).__init__()
        self.listing_id: str = listing_id
        self.week: str = week



class DynamicData(CrawlDataModel):
    __slots__ = ()

    def __init__(self, url: str, fixed: FixedData):
        super().__init__(url,fixed)
