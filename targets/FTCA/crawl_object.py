from data_model.crawling_data_model import CrawlDataModel, FixedCrawlData


class FixedData(FixedCrawlData):
    __slots__ = ('c_province', 'rental', 'c_property_type', 'week')
    c_province: str
    rental: bool
    c_property_type: str
    week: str

    def __init__(self, province: str, c_isRental: bool, c_propertyType: str,
                 week: str):
        super().__init__()
        self.c_province = province
        self.rental = c_isRental
        self.c_property_type = c_propertyType
        self.week = week


class DynamicData(CrawlDataModel):
    __slots__ = ('c_municipality', 'page', 'count')
    c_municipality: str
    page: int
    count: int

    def __init__(self, municipality: str, url: str, page: int, fixed: FixedCrawlData, count: int):
        super().__init__(url, fixed)
        self.c_municipality = municipality
        self.page = page
        self.count = count