import asyncio
import copy
import gzip
import math
import traceback
from time import time

import MySQLdb
import uvloop
from cysimdjson import JSONParser
import json

import dates
from targets.FTCA.regions import ARRAY_OPERATIONS, get_provinces
from http_lib.session_pool_no_proxy import UnboundedNoProxySessionPool
from src.log import container
from sql.batch_holder import BatcHolder
from sql.pool import Pool
from src.http_lib.token_manager import NoProxySessionManager
import targets.FTCA.tables

logger = container.get_logger(targets.FTCA.tables.target, targets.FTCA.tables.table_name)

all_province_url = 'https://api.fotocasa.es/PropertySearch/Search?combinedLocationIds=724,0,0,0,0,0,0,0,0&culture=es-ES&hrefLangCultures=ca-ES%3Bes-ES%3Bde-DE%3Ben-GB&isMap=false&isNewConstruction=false&isNewConstructionPromotions=false&latitude=40&longitude=-4&pageNumber=1&platformId=1&sortOrderDesc=true&sortType=bumpdate&transactionTypeId=3&propertyTypeId=2'
# all_province_url = 'https://api.fotocasa.es/PropertySearch/Search?combinedLocationIds=724,0,15,0,0,0,0,0,0&culture=es-ES&hrefLangCultures=ca-ES%3Bes-ES%3Bde-DE%3Ben-GB&isMap=false&isNewConstruction=false&isNewConstructionPromotions=false&latitude=40&longitude=-4&pageNumber=1&platformId=1&sortOrderDesc=true&sortType=bumpdate&transactionTypeId=3&propertyTypeId=2'

class totalSema():
    def __init__(self):
        self.count = 0

class Zones:

    def __init__(self):
        self.zones = []

    def append(self,code,name, province):
        id_witout_geom = code.replace("geom_",'')
        final_id = id_witout_geom.replace("_",',')
        self.zones.append(
            {
            'combinedLocation': code,
            'name': name,
            'province': province,
            }
        )


def get_sub_regions_from_json(listings_json, zone: dict):
    logger.debug("get_sub_regions_from_json is called")
    sub_zones_array = listings_json['breadcrumb']
    # print(f"sub_zones_array :: {sub_zones_array}")
    # sub_zones_array.reverse()
    sub_zones = []
    is_prov = False
    # print("start len of el")
    logger.debug("at looping over array")
    for array in sub_zones_array:
        el = sub_zones_array[array]
        if len(el) > 0:
            # print(f"len of el {len(el)}")
            sub_zones = el
            if array == 'provinces':
                is_prov = True
    # print("stop len of el")
    final_zones: list = []
    for sub in sub_zones:
        n_zone = copy.deepcopy(zone)
        # print(f"sub loc = {sub['combinedLocation']}")
        n_zone['combinedLocation'] = sub['combinedLocation']
        if is_prov:
            n_zone['province'] = sub['literal']
            n_zone['name'] = sub['literal']
        else:
            n_zone['name'] = sub['literal']
        n_zone['count'] = sub['counter']
        final_zones.append(n_zone)
    return final_zones


def get_province_zones_url(geo_code) -> str:
        url: str = f"https://geom.fotocasa.es/v60/geom_{geo_code}_g.js"
        return url.replace(',', '_')

async def get_province_zones(session_pool: NoProxySessionManager, province: dict) -> Zones:
    headers = {
        'authority': 'geom.fotocasa.es',
        'pragma': 'no-cache',
        'cache-control': 'no-cache',
        'accept': 'application/json, text/plain, */*',
        'user-agent': 'Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36',
        'origin': 'https://www.fotocasa.es',
        'sec-fetch-site': 'same-site',
        'sec-fetch-mode': 'cors',
        'sec-fetch-dest': 'empty',
        'referer': 'https://www.fotocasa.es/',
        'accept-language': 'en-US,en;q=0.9,nl;q=0.8',
    }
    START_OF_JSON = '{"type": "Featu'
    url = get_province_zones_url(province['geo_code'])
    zones = Zones()
    javascript_file_response: bytes = await session_pool.pool_get(url=url, headers=headers)
    javascript_file = javascript_file_response.decode("utf-8")
    # index throws an exception if not found!
    json_index = javascript_file.index(f"{START_OF_JSON}")
    json_str = javascript_file[json_index:]
    json_object = json.loads(json_str)

    for zone_properties in json_object['features']:
        zones.append(
            code=zone_properties['properties']['Code'],
            name=zone_properties['properties']['LocationName'],
            province=province['name']
        )
    return zones


async def descent_region(session_pool: NoProxySessionManager, zone: dict, zone_geo_code, operation: list,
                         final_list: list, sub_zones: list, semaphore: asyncio.BoundedSemaphore, total: totalSema):
    total.count += 1
    await semaphore.acquire()
    print(f"sema count = {total.count}")
    url = (f"https://api.fotocasa.es/PropertySearch/Search?combinedLocationIds={zone_geo_code}" +
           "&culture=es-ES&hrefLangCultures=ca-ES%3Bes-ES%3Bde-DE%3Ben-GB&isMap=false&isNewConstruction=false&isNewConstructionPromotions=false&pageNumber=1&platformId=1&sortOrderDesc=true&sortType=price&transactionTypeId="
           f"{operation[2]}&propertyTypeId=2")
    headers = {
        "authority": "api.fotocasa.es",
        "content-length": "0",
        "accept": "application/json, text/plain, */*",
        "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36",
        # "content-type" : "application/json; charset=utf-8",
        "origin": "https://www.fotocasa.es",
        "sec-fetch-site": "same-site",
        "sec-fetch-mode": "cors",
        "sec-fetch-dest": "empty",
        "referer": "https://www.fotocasa.es/es/comprar/viviendas/alicante-provincia/alacanti/l?combinedLocationIds=724,19,3,361,0,0,0,0,0&gridType=3",
        "accept-language": "es-ES,es;q=0.9,en;q=0.8",
        "if-none-match": "bf61f884-c6e9-4cf2-9a96-c93309232587",
    }
    parser = JSONParser()
    print('before resp')
    listings_json_resp: bytes = await session_pool.pool_get(url, headers)
    print('resp')
    listings_json = parser.parse(listings_json_resp)
    print('items')
    num_of_items = listings_json.at_pointer('/count')
    logger.debug(f"number of items: {num_of_items}, geo={zone_geo_code},url={url}")
    # 724,0,40,0,0,0,0,0,0
    # 724,0,4,0,0,0,0,0,0

    # 724,14,28,173,0,28079,0,672,0
    if num_of_items > 9600:
        sub_zones.append(get_sub_regions_from_json(listings_json=listings_json, zone=zone))
        print("finished sub_zones")
    else:
        # logger.debug(f"appending final list: {num_of_items}")
        results_per_serp = 30
        for page in range(1, math.ceil(num_of_items / results_per_serp) + 1):
            url = (f"https://api.fotocasa.es/PropertySearch/Search?combinedLocationIds={zone_geo_code}" +
                   f"&culture=es-ES&hrefLangCultures=ca-ES%3Bes-ES%3Bde-DE%3Ben-GB&isMap=false&isNewConstruction=false&isNewConstructionPromotions=false&pageNumber={page}" +
                   f"&platformId=1&sortOrderDesc=true&sortType=price&transactionTypeId={operation[2]}&propertyTypeId=2")
            final_list.append({
                'url': url,
                'zone_geo_code': zone['combinedLocation'],
                'province' : zone['province'],
                'name' : zone['name'],
                'operation' : operation[0],
                'rental' : operation[1],
                'count': zone['count'],
                'page': page
            })
    semaphore.release()
    total.count -= 1
    print(f"sema count after = {total.count}")


async def descent_region_iteratively(sub_zones,operation : list, session_pool: NoProxySessionManager,
                                     semaphore: asyncio.BoundedSemaphore) -> list:
    final_list: list = []
    descent_list: list = []
    totalsema = totalSema()
    stop = len(sub_zones)
    while stop > 0:
        for i in range(stop):
            pop = sub_zones.pop(0)
            print(f"length of subzone = {len(pop)}")
            for i in range(0,len(pop)):
                # logger.debug(f"zone is {zone['combinedLocation']}")
                zone = pop[i]
                logger.debug(f"self descent region called with {zone['combinedLocation']}")
                descent_list.append(
                    asyncio.create_task(
                        descent_region(zone_geo_code=zone['combinedLocation'],
                                       operation=operation,
                                       session_pool=session_pool,
                                       final_list=final_list,
                                       sub_zones=sub_zones,
                                       semaphore=semaphore,
                                       zone=zone,
                                       total=totalsema)
                    )
                )
        # logger.debug(f"len descent list = {len(descent_list)}")
        print("descending list")
        await asyncio.gather(*descent_list,return_exceptions=True)
        logger.debug(f"PAST GATHERING DESCENT LIST, len sub_zones = {len(sub_zones)}\n"
                     f"len final_list = {len(final_list)}")
        descent_list = []
        stop = len(sub_zones)
    total_count = 0
    print("finished descending list")
    for item in final_list:
        if item['page'] == 1:
            total_count += item['count']
    logger.debug(f"total_count == {total_count}")
    return final_list


async def upload_to_database(zone: dict, headers:dict, session_pool: NoProxySessionManager,
                             semaphore, batch_holder: BatcHolder, conn_pool: Pool):
    await semaphore.acquire()
    url = zone['url']
    response_client: bytes = gzip.compress(await session_pool.pool_get(url=url, headers=headers))
    logger.debug(f'uploading {url}')
    upload_dict = {
        'url': url,
        'zone_geo_code': zone['zone_geo_code'],
        'province': zone['province'],
        'name': zone['name'],
        'operation': zone['operation'],
        'rental': zone['rental'],
        'page': zone['page'],
        'week': dates.TodayIso.get(),
        'html_bytes': response_client,
        'parse_date': dates.get_now()
    }
    _start = time()
    try:
        await batch_holder.insert_listing_in_database_async(listing=upload_dict, conn_pool=conn_pool)
    except MySQLdb.OperationalError as e:
        logger.critical(f"MySQLdb.OperationalError \n"
                        f"traceback = \n"
                        f"{traceback.format_exc()} \n"
                        f"with data = \n {upload_dict} \n"
                        f"especially url = {upload_dict['url']}\n,"
                        f"{batch_holder.batch.insert_query}\n"
                        f"{batch_holder.batch.query_values}\n")
    logger.debug(f"db upload time :: {(time() - _start)}")
    semaphore.release()


async def start(province, operations, conn_pool: Pool):

    api_headers = {
        "authority": "api.fotocasa.es",
        "content-length": "0",
        "accept": "application/json, text/plain, */*",
        "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36",
        # "content-type" : "application/json; charset=utf-8",
        "origin": "https://www.fotocasa.es",
        "sec-fetch-site": "same-site",
        "sec-fetch-mode": "cors",
        "sec-fetch-dest": "empty",
        "referer": "https://www.fotocasa.es/es/comprar/viviendas/alicante-provincia/alacanti/l?combinedLocationIds=724,19,3,361,0,0,0,0,0&gridType=3",
        "accept-language": "es-ES,es;q=0.9,en;q=0.8",
        "if-none-match": "bf61f884-c6e9-4cf2-9a96-c93309232587"
    }

    # has to be equal to upload_dict in upload to database
    insert_fields = {
        'url': '',
        'zone_geo_code': '',
        'province': '',
        'name': '',
        'operation': '',
        'rental': '',
        'page': '',
        'week': dates.TodayIso.get(),
        'html_bytes': 'response_client',
        'parse_date': dates.get_now()
    }
    table_name: str = targets.FTCA.tables.table_name
    batch_size = 50
    batch_holder = BatcHolder(fields= list(insert_fields.keys()),
                              table_name=table_name,
                              batch_size=batch_size)
    boundary = 50
    concurrent_boundary = 100
    s_pool: UnboundedNoProxySessionPool = UnboundedNoProxySessionPool(boundary=boundary)
    # create noProxyManager
    session_pool: NoProxySessionManager = NoProxySessionManager(err_codes=[404],session_pool=s_pool,param=False)
    semaphore = asyncio.BoundedSemaphore(concurrent_boundary)
    # start_zones: Zones = await get_province_zones(session_pool=session_pool,province=province)
    # sub_zones = [start_zones.zones]
    sub_zones = [[ {
        'combinedLocation' : '724,0,0,0,0,0,0,0,0',
        'name': 'all_provinces',
        'province': 'all_provinces'
    }]]
    final_list: list = await descent_region_iteratively(sub_zones=sub_zones,session_pool=session_pool,
                                                  operation=operations, semaphore=semaphore)
    logger.debug("created final_list")
    result_list: list = []
    logger.debug(f"final_list length = {len(final_list)}")
    for i in range(len(final_list)):
        zone = final_list.pop()
        result_list.append(
            asyncio.create_task(
                upload_to_database(zone=zone,
                                   headers=api_headers,
                                   session_pool=session_pool,
                                   semaphore=semaphore,
                                   batch_holder=batch_holder,
                                   conn_pool=conn_pool)
            )
        )
    await asyncio.gather(*result_list)
    batch_holder.shutdown_async(conn_pool,wait=True)
    await session_pool.close()


def run(target: str):
    try:
        pool: Pool = Pool(1)
        conn = pool.get_connection()
        cursor = conn.cursor()
        cursor.execute(targets.FTCA.tables.create_table)
        conn.commit()
        pool.release_connection(conn)
        _start = time()
        provinces = get_provinces()
        # for operation in ARRAY_OPERATIONS:
        #     for province in provinces:
        #         asyncio.run(start(province=province, operations=operation,
        #
        #                           conn_pool=pool))
        count = 0
        try:
            for operation in ARRAY_OPERATIONS:
                asyncio.run(start(province=None, operations=operation, conn_pool=pool))
                count += 1
            conn.close()
            logger.debug(f"run took :: {(time() - _start)}")
        except Exception as e:
            logger.critical(f'total crash in {ARRAY_OPERATIONS[count]} \n'
                            f'{traceback.format_exc()}')
            count += 1
            conn.close()


    except Exception as e:
        logger.critical(f'total crash\n'
                        f'{traceback.format_exc()}')










