import traceback
from typing import Dict

import dates
from parsing.RealEstateObject import RealEstateData
from text_preprocessing import get_single_digit, concat_all_digits
from log import container

logger = container.get_logger("clean",'clean.log')

def clean_val(val: str) -> str:
    return val.strip().lower()

def find_in_dict(val: str, value_mapping: Dict) -> int:
    val = clean_val(val)
    for key in value_mapping:
        if key in val:
            return value_mapping[key]
    return None

def clean_n_baths(listing: RealEstateData) -> int:
    val = listing.n_baths
    if val is not None and val != 'suite - con baño':
        # print(f"N_Baths = {val}")
        try:
            return get_single_digit(val)
        except Exception as e:
            logger.debug(f"{traceback.format_exc()}\n"
                         f"bad_baths = {val}")
    return None

def clean_n_rooms(listing: RealEstateData) -> int:
    val = listing.n_rooms
    if val is not None:
        # print(f"N_ROOMS = {val}")
        try:
            return get_single_digit(val)
        except Exception as e:
            logger.debug(f"{traceback.format_exc()}\n"
                         f"bad_room = {val}")
    return None

def clean_local_price(listing: RealEstateData) -> int:
    val = listing.local_price
    if val is not None:
        # logger.debug(f"LOCAL_PRICE = {val}")
        val = clean_val((val))
        if val.find('a consultar') != -1:
            return None
        return concat_all_digits(val)
    return None
    # location_all

# Semi-sótano

def clean_floor(listing: RealEstateData) -> float:
    val = listing.floor
    # entresuelo
    # subsótano
    # principal
    if val is not None:
        lowered = clean_val(val)
        if lowered == 'bajos' or lowered == 'principal':
            return 0.0
        elif lowered == 'entresuelo':
            return 0.5
        elif lowered == 'sótano':
            return -1.0
        elif lowered == 'semi-sótano' or lowered == 'semisótano'\
                or lowered == 'subsótano':
            return  -0.5
        elif lowered == 'a consultar':
            return None
        else:
            return get_single_digit(val)
    return None

energy_cert = {
    'a' : 8,
    'b' : 9,
    'c' : 10,
    'd' : 11,
    'e' : 12,
    'f' : 13,
    'g' : 14,
    'exento': 15,
}

def clean_energy_cert(listing: RealEstateData) -> int:
    # exento
    val = listing.energy_cert_id
    if val is not None and val != 'en trámite':
        val = clean_val(val)
        return energy_cert[val]
    return None


conservation_id = {
    'obra nueva' : 9,
    'casi nuevo' : 10,
    'muy bien' : 11,
    'bien' : 12,
    'a reformar' : 13,
    'reformado' : 14,
}
# 8548 + 306 + 1919 = 8548 + 2, 10467

def clean_conservation_id(listing: RealEstateData) -> int:
    val = listing.conservation_id
    if val is not None:
        val = clean_val(val)
        return find_in_dict(val, conservation_id)
    return None


property_types = {
    'plantas intermedias' : 79,
    'apartamento' : 80,
    'Ático'.lower() : 81,
    'Dúplex'.lower() : 82,
    'loft' : 83,
    'planta baja' : 84,
    'piso' : 85,
    'estudio' : 86,
    'casa o chalet' : 87,
    'casa-chalet' : 87,
    'casa adosada' : 88,
    'finca rústica' : 89,
    'nave industrial' : 90,
    'local' : 91,
    'garaje' : 92,
    'oficina' : 93,
    'trastero' : 94,
    'urbanizable' : 95,
    'terreno' : 95,
    'edificio' : 96,
}

def clean_property_type(listing: RealEstateData) -> int:
    val = listing.property_type_id
    if val is not None:
        return find_in_dict(val,property_types)
    return None

def substract_date_current_year(years: int):
    iso_year = dates.TodayIso().year
    return iso_year - years

construction_year_mapping = {
    'menos de 1 año': 1,
    '1 a 5 años' : 3,
    '5 a 10 años': 8,
    '10 a 20 años': 15,
    '20 a 30 años': 25,
    '30 a 50 años': 40,
    '50 a 70 años': 60,
    '70 a 100 años': 85,
    '+ 100 años': 100,
}

def clean_construction_year(listing: RealEstateData) -> int:
    val = listing.construction_year
    if val is not None:
        # logger.debug(f"construction year = {val}")
        return substract_date_current_year(find_in_dict(val, construction_year_mapping))
    return None

def clean_has_pool(listing: RealEstateData) -> bool:
    return listing.has_pool is not None

def clean_has_common_zones(listing: RealEstateData) -> bool:
    return listing.has_common_zones is not None

def clean_has_racket_zone(listing: RealEstateData) -> bool:
    return listing.has_racket_zone is not None

def clean_has_storage(listing: RealEstateData) -> bool:
    return listing.has_storage is not None

def clean_pet_friendly(listing: RealEstateData) -> bool:
    val = listing.pet_friendly
    if listing.pet_friendly is not None:
        val = clean_val(val)
        return val.find('sí') != -1
    return None

def clean_is_exact_address(listing: RealEstateData) -> bool:
    return listing.is_exact_address

def has_air_conditioner(listing: RealEstateData) -> bool:
    return listing.has_air_conditioner is not None

def has_garage(listing: RealEstateData) -> bool:
    return listing.has_garage is not None

def has_garage_included(listing: RealEstateData) -> bool:
    return listing.has_garage_included

def has_terrace(listing: RealEstateData) -> bool:
    return listing.has_terrace is not None

def clean_area(listing: RealEstateData) -> int:
    val = listing.area
    if val is not None:
        return get_single_digit(val)
    return None


def clean_operation_type_id(listing: RealEstateData) -> int:
    val = listing.operation_type_id
    if val is not None:
        if val == 'alquiler':
            return 1
        elif val == 'venta':
            return 0

def clean_has_elevator(listing: RealEstateData) -> bool:
    # val = listing.has_elevator
    # if val is not None:
    #     val = clean_val(val)
    #     return val.find('sí') != -1
    # return None
    return listing.has_elevator is not None


def clean_build_status_id(listing: RealEstateData) -> int:
    # Only ones available
    val = listing.build_status_id
    if val is None:
        return 3
    elif val == 'obra nueva':
        return 6

def clean_has_balcony(listing: RealEstateData) -> bool:
    return listing.has_balcony is not None


def clean_cardinal_direction_id(listing: RealEstateData) -> int:
    CARDINAL_DIRECTION = {
        "este": 1,
        'norte': 2,
        'oeste': 3,
        'sur': 4,
        'noreste': 5,
        'sureste': 6,
        'suroeste': 7,
        'noroeste': 8,
    }
    val = listing.cardinal_direction_id
    if val is not None:
        val = clean_val(val)
        return CARDINAL_DIRECTION[val]
    return None

def clean_description(listing: RealEstateData) -> str:
    val = listing.description
    if val is not None:
        return clean_val(val)
    return None

def clean_title(listing: RealEstateData) -> str:
    val = listing.title
    if val is not None:
        return clean_val(val)
    return None

def clean_usage_id(listing: RealEstateData) -> int:
    USAGE_TYPES = {
        "obra nueva": 14,
        "vivienda": 1,
        "garaje": 1,
        "trastero": 3,
        "oficina": 3,
        "local": 2,
        "terreno": 6,
        "edificio": 2,
        "habitacion": 1
    }

    val = listing.usage_id
    if val is not None:
        val = clean_val(val)
        return USAGE_TYPES[val]
    return None

def clean_full_address(listing: RealEstateData) -> str:
    operation_one = 'alquiler en'
    operation_two = 'venta en'
    val = listing.full_address
    if listing.build_status_id == 6:
        return listing.full_address
    if val is not None:
      val = val.lower()
      if operation_one in val:
          new_val = clean_val(val.split(operation_one)[1])
          return new_val
      elif operation_two in val:
          new_val = clean_val(val.split(operation_two)[1])
          return new_val
      else:
          logger.debug(f"UNKNOWN FULL ADDRESS:\n"
                       f"full address = {val}")
        #obra nueva is property type in title split
    return None

def clean_listing(listing: RealEstateData) -> None:
    listing.n_baths = clean_n_baths(listing)
    listing.n_rooms = clean_n_rooms(listing)
    listing.local_price = clean_local_price(listing)
    listing.floor = clean_floor(listing)
    listing.energy_cert_id = clean_energy_cert(listing)
    listing.conservation_id = clean_conservation_id(listing)
    listing.property_type_id = clean_property_type(listing)
    listing.construction_year = clean_construction_year(listing)
    listing.has_pool = clean_has_pool(listing)
    listing.has_common_zones = clean_has_common_zones(listing)
    listing.has_racket_zone = clean_has_racket_zone(listing)
    listing.has_storage = clean_has_storage(listing)
    listing.pet_friendly = clean_pet_friendly(listing)
    listing.is_exact_address = clean_is_exact_address(listing)
    listing.has_air_conditioner = has_air_conditioner(listing)
    listing.has_garage = has_garage(listing)
    listing.has_garage_included = has_garage_included(listing)
    listing.has_terrace = has_terrace(listing)
    listing.area = clean_area(listing)
    listing.operation_type_id = clean_operation_type_id(listing)
    listing.has_elevator = clean_has_elevator(listing)
    listing.build_status_id = clean_build_status_id(listing)
    listing.has_balcony = clean_has_balcony(listing)
    listing.cardinal_direction_id = clean_cardinal_direction_id(listing)
    listing.description = clean_description(listing)
    listing.title = clean_title(listing)
    listing.usage_id = clean_usage_id(listing)
    listing.full_address = clean_full_address(listing)

# Depósito
#
# Sí
# Gastos de comunidad -> Sí
