import asyncio

import aiohttp

import dates
from crawling_models.detail_crawler import DetailCrawler, run_without_proxy
from targets.FTCA.crawl_detail_object import DynamicData, FixedData

week = dates.TodayIso.get()


class FtcaDetailCrawler(DetailCrawler):

    def create_dynamic_data(self, database_row: tuple) -> DynamicData:
        fixed: FixedData = FixedData(listing_id=database_row[0],
                                     week=week)
        url = 'https://www.fotocasa.es' + database_row[1]
        return DynamicData(url, fixed)


def run():
    headers = {
        'authority': 'www.fotocasa.es',
        'sec-ch-ua': '" Not;A Brand";v="99", "Google Chrome";v="91", "Chromium";v="91"',
        'sec-ch-ua-mobile': '?0',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.101 Safari/537.36',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'sec-fetch-site': 'none',
        'sec-fetch-mode': 'navigate',
        'sec-fetch-user': '?1',
        'sec-fetch-dest': 'document',
        'accept-language': 'en-US,en;q=0.9,nl;q=0.8'
    }
    crawler = FtcaDetailCrawler()
    sel_table_name: str = f'ftca_serp_internal_parsed_{week}'
    table_name: str = f'ftca_dp_internal_{week}'
    create_table_query: str = f"create table if not exists `{table_name}` (" \
                f"id INT NOT NULL AUTO_INCREMENT," \
                f"listing_id varchar(150) NULL, " \
                f"week varchar(12) NOT NULL, " \
                f"url varchar(800) NOT NULL," \
                f"data_bytes MEDIUMBLOB NULL," \
                f"response_status integer not null, " \
                f"c_parse_date varchar(15) NULL," \
                f"PRIMARY KEY (id))"
    insert_data_fields = []
    fixed: FixedData = FixedData(listing_id='', week='')
    DynamicData(url='', fixed=fixed).get_field_list(insert_data_fields)

    run_without_proxy(crawler=crawler,
                      create_table_query=create_table_query,
                      insert_data_base_fields=insert_data_fields,
                      headers=headers,
                      http_response_retry_error_codes=[403],
                      session_parameter_data=False,
                      single_session_total_allowed_connections=50,
                      table_name=table_name,
                      select_table_name=sel_table_name,
                      select_data_fields=['listing_id', 'detail_page'],
                      return_exceptions=(aiohttp.ClientPayloadError, asyncio.exceptions.TimeoutError,
                                         aiohttp.ClientConnectorError),
                      concurrent_boundary=200,
                      allow_redirects=True,
                      incremental=True,
                      remainder=False)
