import gzip
import json
import traceback
from typing import List, Tuple

from lxml.cssselect import CSSSelector

import dates
from crawling_models.uniquefer import start_unique
from html_functionality.html_local import read_into_html
from parsing.RealEstateObject import RealEstateData
from parsing.normalized_columns import NormalizedRealEstateColumns
from parsing.run_parser import start_parsing_sequence, start_parsing_real_estate
from sql.batch_holder import BatcHolder
from sql.pool import Pool
from src.log import container
from targets.FTCA.detail_cleaner import clean_listing
from targets.FTCA.detail_parser_obra import parser_alquiler, parser_venta
from targets.FTCA.word_detection import get_text_sentence_tokenized, find_word_in_sentence, words_for_room, \
    words_for_bathroom, \
    words_for_area, words_for_floor, find_word_and_determine_proper, negation_words_for_floor, get_properties, \
    sentence_as_string

logger = container.get_logger('error_parsing', "err_parse.log")

# time_stamp, seq_uuid()
# (listing_id, detail_page, week, timestamp, target) listing_id, week, target, timestamp as primary_key
# where not in previo
# Should be grouped per country
# rest to S3 compressed
#
def get_featured_details(_html_el, listing: RealEstateData):
    content = _html_el.text_content()
    sentence = get_text_sentence_tokenized(content)[0]
    # print(f"sentence = {sentence}\n"
    #       f"content={content}\n"
    #       f"-----")
    if find_word_in_sentence(words_for_room, sentence) > -1:
        listing.n_rooms = content.lower()
    elif find_word_in_sentence(words_for_bathroom, sentence) > -1:
        listing.n_baths = content.lower()
    elif find_word_in_sentence(words_for_area, sentence) > -1:
        if not ('terreno' in sentence):
            listing.area = content
    elif find_word_and_determine_proper(words_for_floor, sentence, negation_words_for_floor) > -1:
        listing.floor = content.lower()


def get_characteristics(_html_el, sel_label, sel_value, listing: RealEstateData):
    label = sel_label(_html_el)[0].text_content().lower()
    value = sel_value(_html_el)[0].text_content().lower()
    sentence = get_text_sentence_tokenized(label)[0]
    prop = get_properties(sentence)
    if prop is not None:
        index, normalized_name = prop
        #         if normalized_name ==
        if normalized_name != NormalizedRealEstateColumns.area.value:
            setattr(listing, normalized_name, value)
    # TODO check negations for certain fields.


def handle_tags(_html_el, listing: RealEstateData):
    value = _html_el.text_content().lower()
    sentence = get_text_sentence_tokenized(value)[0]
    prop = get_properties(sentence)
    if prop is not None:
        index, normalized_name = prop
        setattr(listing, normalized_name, sentence_as_string(sentence))


def parser(row: dict, batch: BatcHolder, conn_pool: Pool):
    """Row is database columns as a dict."""
    try:
        if row['response_status'] != 404:
            listing: RealEstateData = RealEstateData()
            listing.listing_id = row['listing_id']
            listing.week = row['week']
            listing.parse_date = row['c_parse_date']
            listing.detail_page = row['url']
            # print(f"detail = {listing.detail_page}")
            _html = read_into_html(gzip.decompress(row['data_bytes']).decode('utf-8'))

            if listing.detail_page.find('obra-nueva') != -1:
                is_alquilers = CSSSelector('#priceContainer')
                listing.build_status_id = 'obra nueva'
                is_alquiler = len(is_alquilers(_html)) > 0
                if is_alquiler:
                    parser_alquiler(listing, _html)
                else:
                    parser_venta(listing, _html)
                batch.insert_data_model_in_new(listing=listing, conn_pool=conn_pool)
            else:
                sel_featured_details = CSSSelector('li.re-DetailHeader-featuresItem')
                features = sel_featured_details(_html)
                for feat in features:
                    get_featured_details(feat, listing)
                sel_characteristics = CSSSelector(
                    'section.sui-SectionInfo div.re-RealestateDetail-featuresListWrapper > div.re-DetailFeaturesList div.re-DetailFeaturesList-featureContent')
                sel_characteristic_label = CSSSelector('p.re-DetailFeaturesList-featureLabel')
                sel_characteristic_value = CSSSelector('p.re-DetailFeaturesList-featureValue')
                for characteristic in sel_characteristics(_html):
                    get_characteristics(characteristic, sel_characteristic_label, sel_characteristic_value, listing)
                sel_tags = CSSSelector('div.re-DetailExtras > ul > li')
                for tag in sel_tags(_html):
                    handle_tags(tag, listing)

                # Dirección aproximada por deseo del anunciante
                sel_hidden_address = CSSSelector('p.re-DetailMap-hiddenAddress')
                listing.is_exact_address = len(sel_hidden_address(_html)) > 0
                # sel_location = CSSSelector('h2.re-DetailMap-address')
                # locations = sel_location(_html)
                # if len(locations) > 0:
                #     print(f"FOUDN LOCATION ALL")
                #     listing.location_all = locations[0].text_content().lower().strip()
                sel_price = CSSSelector('span.re-DetailHeader-price')
                price_els = sel_price(_html)
                if len(price_els) > 0:
                    listing.local_price = price_els[0].text_content()
                sel_price_discount = CSSSelector('div.re-DetailHeader-reducedPrice')
                prices_discount = sel_price_discount(_html)
                if len(prices_discount) > 0:
                    # listing.portal_price_discount = prices_discount[0].text_content()
                    listing.additional_fields = json.dumps({"price_portal_discount": prices_discount[0].text_content()})


                sel_title = CSSSelector('h1.re-DetailHeader-propertyTitle')
                listing.title = sel_title(_html)[0].text_content().lower()
                # listing.location_all = listing.title
                listing.full_address = listing.title
                sel_energy_label = CSSSelector(
                    'div[class*="re-DetailEnergyCertificate-item re-DetailEnergyCertificate-value"]')
                energy_labels = sel_energy_label(_html)
                if len(energy_labels) > 0:
                    listing.energy_cert_id = energy_labels[0].text_content().lower()
                sel_description = CSSSelector('p.fc-DetailDescription')
                descriptions = sel_description(_html)
                if len(descriptions) > 0:
                    listing.description = descriptions[0].text_content().strip().lower()
                # price discount
                if listing.property_type_id is None:
                    listing.property_type_id = listing.title

                # url = row['detail-page']
                # url = 'https://www.fotocasa.es/es/comprar/vivienda/zaragoza-capital/aire-acondicionado-calefaccion-parking-jardin-terraza-trastero-parking-internet/149853019/d'

                if listing.detail_page.find('alquiler') != -1:
                    listing.operation_type_id = 'alquiler'
                else:
                    listing.operation_type_id = 'venta'
                # TODO search URL but for now only vivienda
                listing.usage_id = 'vivienda'

                sel_contact_name = CSSSelector('img.re-ContactDetail-inmoLogo')
                c_names = sel_contact_name(_html)
                if len(c_names) > 0:
                    # something with exmple of excel, need ot verify
                    listing.contact_name = c_names[0].get('title').lower().split(', todas')[0]
                # new_build
                # sel_vendor_asset_id = CSSSelector('div.re-NcAdvertiserCard-reference')
                # sel_vendor_phone = CSSSelector('a.re-NcAdvertiserCard-phoneNumber')
                sel_vendor_phone = CSSSelector('div.re-ContactDetail-phone')
                vendor_phones = sel_vendor_phone(_html)
                if len(vendor_phones) > 0:
                    listing.contact_phone = vendor_phones[0].text_content().strip()
                sel_vendor_asset_id = CSSSelector('ul.re-ContactDetail-inmoContact')
                asset_ids = sel_vendor_asset_id(_html)
                if len(asset_ids) > 0:
                    listing.portal_reference = asset_ids[0].text_content().strip().lower()
                # LAT LON SERP
                # operation SERP
                clean_listing(listing)
                batch.insert_data_model_in_new(listing=listing, conn_pool=conn_pool)
    except Exception as e:
        logger.error(f'error parsing {row["id"]}\n'
                     f'traceback =\n'
                     f'{traceback.format_exc()}')

def run(start_week: str, end_week: str, target: str):
    logger.debug(f"run is called with {start_week},{end_week}")
    start_parsing_real_estate(
                target=target,
                parse_type='dp',
                initial_week=start_week,
                final_week=end_week,
                parser=parser,
                unique=True,
    )