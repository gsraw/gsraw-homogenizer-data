from lxml.cssselect import CSSSelector
from html_functionality.html_local import read_into_html
from src.log import container
from targets.FTCA.detail_cleaner import clean_listing
from parsing.normalized_columns import NormalizedRealEstateColumns
from targets.FTCA.word_detection import get_text_sentence_tokenized, find_word_in_sentence, words_for_room, \
    words_for_bathroom, \
    words_for_area, words_for_floor, find_word_and_determine_proper, negation_words_for_floor, get_properties, \
    sentence_as_string, spanish_column_mapper, find_word_in_sentence_as_string
from parsing.RealEstateObject import RealEstateData, create_table_test_real_estate, \
    create_table_definition_real_estate

logger = container.get_logger('error_parsing', "err_parse.log")

def get_featured_details_obra(_html_el, listing: RealEstateData):
    content = _html_el.text_content()
    sentence = get_text_sentence_tokenized(content)[0]
    # print(f"sentence = {sentence}\n"
    #       f"content={content}\n"
    #       f"-----")
    if find_word_in_sentence(words_for_room, sentence) > -1:
        listing.n_rooms = content.lower()
    elif find_word_in_sentence(words_for_bathroom, sentence) > -1:
        listing.n_baths = content.lower()
    elif find_word_in_sentence(words_for_area, sentence) > -1:
        if not ('terreno' in sentence):
            listing.area = content


def get_characteristics_obra(_html_el, sel_label, sel_value, listing: RealEstateData):
    labels = sel_label(_html_el)
    values = sel_value(_html_el)
    for i in range(0,len(labels)):
        label = labels[i].text_content().lower()
        value = values[i].text_content().lower()
        sentence = get_text_sentence_tokenized(label)[0]
        prop = get_properties(sentence)
        if prop is not None:
            index, normalized_name = prop
            #         if normalized_name ==
            if normalized_name != NormalizedRealEstateColumns.area.value:
                setattr(listing, normalized_name, value)
    # TODO check negations for certain fields.

def get_characteristics_obra_alq(_html_el, sel_label, sel_value, listing: RealEstateData):
    label = sel_label(_html_el)[0].text_content().lower()
    value = sel_value(_html_el)[0].text_content().lower()
    print(f"CHARACTERISTIC LABEL = {label}")
    sentence = get_text_sentence_tokenized(label)[0]
    print(f"CHARACTERISTIC LABEL = {label}")
    print(f"CHARACTERISTIC sentence = {sentence}")
    prop = get_properties(sentence)
    if prop is not None:
        index, normalized_name = prop
        #         if normalized_name ==
        if normalized_name != NormalizedRealEstateColumns.area.value:
            setattr(listing, normalized_name, value.split(': ')[1])

def handle_tags(_html_el, listing: RealEstateData):
    value = _html_el.text_content().lower()
    sentence = get_text_sentence_tokenized(value)[0]
    prop = get_properties(sentence)
    if prop is not None:
        index, normalized_name = prop
        setattr(listing, normalized_name, sentence_as_string(sentence))

# piscina comunitaria, zona comunitaria con piscina
# ascensor privado
# acceso directo a garaje subterráneo
# Incluye plaza de garaje
# Actualizado: 2021-11-11
# con plaza de garaje y trastero incluido
# todas con Terraza
# jardín privado
# garaje adicionales a las incluidas en el contrato de arrendamiento de la vivienda por 45€+IVA/MES.
#  Plaza de garaje incluida en el precio
# Dispone de aire acondicionado
# zonas verdes,  Piscinas infantil y de adultos, zonas verdes y gimnasio.
# COMMON AREAS JUST RENOVATED
def parser_venta(listing: RealEstateData, _html):
            listing.build_status_id = 'obra nueva'
            sel_title = CSSSelector('h1.re-NcCoverBasic-title')
            titles = sel_title(_html)
            if len(titles) > 0:
                listing.title = titles[0].text_content().strip().lower()

            sel_address_location = CSSSelector('div.re-NcPageUnit-locationAddress span[title]')
            addresses = sel_address_location(_html)
            if len(addresses) > 0:
                listing.full_address = addresses[0].text_content().lower().strip()
            sel_contact_name_and_url = CSSSelector('a.re-NcAdvertiserCard-detailsLink')
            contacts = sel_contact_name_and_url(_html)
            if len(contacts) > 0:
                contact = contacts[0]
                listing.contact_name = contact.text_content().lower().strip()
                listing.agency_url = contact.get('href')

            sel_contact_phone = CSSSelector('a.re-NcAdvertiserCard-phoneNumber')
            phones = sel_contact_phone(_html)
            if len(phones) > 0:
                listing.contact_phone = phones[0].text_content().replace(' ','').strip()
            sel_contact_reference = CSSSelector('div.re-NcAdvertiserCard-reference')
            contact_references = sel_contact_reference(_html)
            if len(contact_references) > 0:
                listing.portal_reference = contact_references[0].text_content().strip()
            sel_description = CSSSelector('div.re-NcPageUnit-contentMain div.sui-CollapsibleReadmore')
            descriptions = sel_description(_html)
            if len(descriptions) > 0:
                listing.description = descriptions[0].text_content().strip()
            sel_price = CSSSelector('h3.re-NcDetailHeadline-titleText')
            prices = sel_price(_html)
            if len(prices) > 0:
                listing.local_price = prices[0].text_content()

            sel_features = CSSSelector('ul.re-NcListTagcloud span.sui-AtomTag-label')
            features = sel_features(_html)
            for feat in features:
                get_featured_details_obra(feat, listing)

            sel_dt = CSSSelector('dl.re-NcUnitInfo-infoList > dt')
            sel_dd = CSSSelector('dl.re-NcUnitInfo-infoList > dd')
            get_characteristics_obra(_html, sel_dt, sel_dd, listing)

            sel_energy_label = CSSSelector('div.re-NcDetailEnergyCertificate-badge > span')
            energy_labels = sel_energy_label(_html)
            if len(energy_labels) > 0:
                listing.energy_cert_id = energy_labels[0].text_content().lower()

            if listing.property_type_id is None:
                listing.property_type_id = listing.title
            listing.operation_type_id = 'venta'
            clean_listing(listing)

# Parking comunitario
def parser_alquiler(listing: RealEstateData, _html):
    # a.agency-microsite
    listing.build_status_id = 'obra nueva'
    sel_title = CSSSelector('h1.property-title')
    titles = sel_title(_html)
    if len(titles) > 0:
        listing.title = titles[0].text_content().strip().lower()
        listing.full_address = listing.title

    sel_contact_name_and_url = CSSSelector('a.agency-microsite')
    contacts = sel_contact_name_and_url(_html)
    if len(contacts) > 0:
        contact = contacts[0]
        listing.contact_name = contact.text_content().lower().strip()
        listing.agency_url = contact.get('href')

    # sel_contact_phone = CSSSelector('a.re-NcAdvertiserCard-phoneNumber')
    # phones = sel_contact_phone(_html)
    # if len(phones) > 0:
    #     listing.contact_phone = phones[0].text_content().replace(' ', '').strip()
    sel_contact_reference = CSSSelector('span.agency-reference')
    contact_references = sel_contact_reference(_html)
    if len(contact_references) > 0:
        listing.portal_reference = contact_references[0].text_content().strip()
    sel_description = CSSSelector('#ctl00_ddDescription p.detail-description')
    descriptions = sel_description(_html)
    if len(_html) > 0:
        listing.description = descriptions[0].text_content().strip()
    sel_price = CSSSelector('#priceContainer')
    prices = sel_price(_html)
    if len(prices) > 0:
        listing.local_price = prices[0].text_content()

    sel_features = CSSSelector('ul.basic-info--list > li > span')
    features = sel_features(_html)
    for feat in features:
        get_featured_details_obra(feat, listing)

    # AVAILABLE IN ANOTHER URL
    # sel_characteristics = CSSSelector('div.detail-caracteristics--col > ul.detail-caracteristics--list > li')
    # sel_dt = CSSSelector('span')
    # sel_dd = CSSSelector('span')
    # for characteristic in sel_characteristics(_html):
    #     get_characteristics_obra_alq(characteristic, sel_dt, sel_dd, listing)
    # sel_extra = CSSSelector('ul.detail-extras > li')
    # for extra in sel_extra(_html):
    #     handle_tags(extra, listing)

    # get characteristic/ extras from URL
    for normalized_name in spanish_column_mapper:
        detection_words = spanish_column_mapper[normalized_name]
        index_new: int = find_word_in_sentence_as_string(detection_words, listing.detail_page)
        if index_new > -1:
            setattr(listing, normalized_name, 'sí')
    sel_energy_label = CSSSelector('#energySection div.certificate-value > span')
    energy_labels = sel_energy_label(_html)
    if len(energy_labels) > 0:
        listing.energy_cert_id = energy_labels[0].text_content().lower()

    if listing.property_type_id is None:
        listing.property_type_id = listing.title
    listing.operation_type_id = 'alquiler'
    clean_listing(listing)

def test():
    with open('test_dp_obra_alquiler_2.html') as file:
    # with open('test_dp_obra_comprar.html') as file:
        listing: RealEstateData = RealEstateData()
        # listing.week = row['week']
        # listing.parse_date = row['parse_date']
        listing.detail_page = 'https://www.fotocasa.es/vivienda/calpe-calp/calefaccion-ascensor-parking-calpe-calp-161493776?tti=3'
        _html = read_into_html(file.read())
        # parser_alquiler(listing,_html)
        parser_alquiler(listing,_html)
        test_values = []
        listing.get_test_values(test_values)
        print('---------------------------------------')
        for val in test_values:
            print(val)

if __name__ == "__main__":
    test()
