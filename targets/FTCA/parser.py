import gzip
import json
import traceback
from cysimdjson import JSONParser
from json_functionality.simd_json_utils import try_get
from parsing.RealEstateObject import RealEstateData
from parsing.run_parser import start_parsing_sequence, start_parsing_real_estate
from sql.batch_holder import BatcHolder
from sql.pool import Pool
from src.log import container



logger = container.get_logger('error_parsing', "err_parse.log")


def parser(row: dict, batch: BatcHolder, conn_pool: Pool):
    try:
        parser = JSONParser()
        item_json = parser.parse(gzip.decompress(row['html_bytes']))
        # c property type into c_property_type_id -> usage_type_id
        # fixed: FixedData = FixedData(province=row['province'],
        #                              parse_date=row['parse_date'],
        #                              c_isRental=row['rental'],
        #                              # c_propertyType=row['operation'],
        #                              week=row['week'])
        for arrayListing in item_json.at_pointer('/realEstates'):
            listing: RealEstateData = RealEstateData()
            listing.listing_id = arrayListing['id']
            listing.week = row['week']
            listing.lat = arrayListing['address']['coordinates']['latitude']
            listing.lon = arrayListing['address']['coordinates']['longitude']
            listing.original_admin1 = arrayListing['address']['location']['level1']  # Comunidad Autónoma CCAA
            listing.original_admin2 = arrayListing['address']['location']['level2']  # Province
            listing.original_admin3 = arrayListing['address']['location']['level3']  # Municipallity
            listing.original_admin4 = arrayListing['address']['location']['level4']  # District
            listing.original_admin5 = arrayListing['address']['location']['level5']  # Neighbourhood
            # listing.location_level6 = arrayListing['address']['location']['level6']
            # listing.location_level7 = arrayListing['address']['location']['level7']
            # listing.location_level8 = arrayListing['address']['location']['level8']
            listing.full_address = arrayListing['address']['ubication']
            listing.postcode = arrayListing['address']['zipCode']
            listing.agency_name = arrayListing['advertiser']['clientAlias']
            listing.agency_url = try_get(arrayListing, '/advertiser/logo/es')
            listing.description = try_get(arrayListing, '/description')
            listing.contact_phone = arrayListing['advertiser']['phone']
            listing.vendor_type = arrayListing['advertiser']['typeId']
            listing.portal_reference = arrayListing['advertiser']['clientId']
            listing.listing_created_at = arrayListing['date']
            arrayPics = arrayListing['multimedias']
            listing.n_images = len(arrayPics)
            listing.detail_page = arrayListing['detail']['es']
            for feature in arrayListing['features']:
                if feature['key'] == 'rooms':
                    listing.n_rooms = feature['value'][0]

                if feature['key'] == 'floor':
                    listing.floor = feature['value'][0]

                if feature['key'] == 'surface':
                    listing.area = feature['value'][0]

            # listing.products = ''
            # if 'products' in arrayListing:
            #     for product in arrayListing['products']:
            #         listing.products = str(listing.products) + "|" + str(product['id']) + "|"
            is_new = arrayListing['isNew']
            if is_new:
                listing.build_status_id = 6
            else:
                listing.build_status_id = 3
            listing.push_up = try_get(arrayListing, '/isMsAdvance')
            # listing.test_premium = arrayListing['isMsAdvance']
            listing.highlighted = arrayListing['isTop']
            listing.has_virtual_tour = arrayListing['isVirtualTour']
            # typeId is ??
            # listing.typeId = arrayListing['typeId']
            # subtypeid is property type, we need to map
            # listing.subtypeId = arrayListing['subtypeId']
            listing.local_price = arrayListing['transactions'][0]['value'][0]
            price_portal_discount = try_get(arrayListing['transactions'][0], '/reduced')
            if price_portal_discount is not None:
                listing.additional_fields = json.dumps({"price_portal_discount": price_portal_discount})

            listing.operation_type_id = arrayListing['transactions'][0]['transactionTypeId']
            if listing.operation_type_id == 1:
                listing.operation_type_id = 0
            elif listing.operation_type_id == 3:
                listing.operation_type_id = 1
            listing.has_photo_report = arrayListing['isPhotoReport']
            listing.parent_id = arrayListing['promotionId']
            batch.insert_data_model_in_new(listing=listing, conn_pool=conn_pool)
    except Exception as e:
        logger.error(f'error parsing {row["id"]}\n'
                     f'traceback =\n'
                     f'{traceback.format_exc()}')


def run(start_week: str, end_week: str, target: str):
    logger.debug(f"run is called with {start_week},{end_week}")
    start_parsing_real_estate(
        target=target,
        parse_type='serp',
        initial_week=start_week,
        final_week=end_week,
        parser=parser,
        unique=True,
    )
