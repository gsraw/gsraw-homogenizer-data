import dates

target = 'ftca'
table_name = f'{target}_serp_internal_{dates.TodayIso.get()}'
table_name_parsed = f'{target}_serp_internal_parsed_{dates.TodayIso.get()}'
create_parsed_table = f"CREATE TABLE IF NOT EXISTS {table_name_parsed} (id INTEGER AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) as " \
                      f"SELECT * FROM configurations.re_template_parsed"

create_table = f"CREATE TABLE IF NOT EXISTS {table_name} (id INTEGER AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) as " \
               f"SELECT * FROM configurations.re_template_rawdata"
