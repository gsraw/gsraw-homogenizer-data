from typing import List, Tuple, Set
from nltk.tokenize import TweetTokenizer, sent_tokenize
from nltk import pos_tag
import nltk
from parsing.normalized_columns import NormalizedRealEstateColumns
nltk.download('punkt')
#TODO non_exact comparison, comparing word length and contains

tokenizer_words = TweetTokenizer()


words_for_address = {}
words_for_description  = {}
words_for_room = {'habs.', 'habs', 'hab.', 'hab', 'habitaciones', 'habitación'}
words_for_bathroom = {'baño', 'baños'}
words_for_area = {'m²', '²'}
words_for_floor = {'planta'}
type_of_floors = {'bajos', 'sótano', 'semi-sótano'}
negation_words_for_floor = {'plantas'}
words_for_terrace = {'terraza'}
words_for_storage = {'trastero'}
# Estado Bien = propiedad semireformado
words_for_status = {'propiedad semireformado', 'estado'}
words_for_elevator = {'ascensor'}
words_for_construction_year = {'antigüedad'}
words_for_furnished = {'amueblado'}
words_for_energy_label = {'consumo energía', 'certificado energético'}
words_for_cardinal_orientation = {'orientación'}
words_for_property_type = {'tipo de inmueble'}
# words_for_parking = {'parking'}
words_for_garage = {'garaje incluido', 'garaje', 'parking'}
words_for_garage_included = {'garaje incluido',  'incluida en el precio',
    'puerta automática de',
    'incluidas en el precio',
    'tipo de garaje',
    'incluido'}
words_for_air_conditioning = {'aire acondicionado', 'air acondi', 'aire acondi'}
words_for_balcony = {'balcón'}
words_for_racketzone = {'zona deportiva'}
words_for_door_type = {'puerta blindada'}
words_for_has_common_zones = {'z. comunitaria'}
words_for_pet_friendly = {'mascotas'}
words_for_pool = {'piscina'}
positive_words_for_pet_friendly = {'sí'}
words_for_is_address_exact = {'Dirección aproximada por deseo del anunciante'}
words_for_exterior = {'exterior'}
words_for_usage_type = {}
words_for_construction_status = {'obra nueva', 'obra nuevo'}
spanish_column_mapper = \
    {
        NormalizedRealEstateColumns.full_address.value : words_for_address,
        NormalizedRealEstateColumns.construction_year.value : words_for_construction_year,
        NormalizedRealEstateColumns.description.value : words_for_description,
        NormalizedRealEstateColumns.has_racket_zone.value : words_for_racketzone,
        NormalizedRealEstateColumns.energy_cert_id.value : words_for_energy_label,
        NormalizedRealEstateColumns.has_elevator.value: words_for_elevator,
        NormalizedRealEstateColumns.has_garage.value: words_for_garage,
        NormalizedRealEstateColumns.has_garage_included.value: words_for_garage_included,
        NormalizedRealEstateColumns.has_common_zones.value: words_for_has_common_zones,
        NormalizedRealEstateColumns.has_storage.value: words_for_storage,
        NormalizedRealEstateColumns.has_pool.value: words_for_pool,
        NormalizedRealEstateColumns.has_balcony.value: words_for_balcony,
        NormalizedRealEstateColumns.has_terrace.value: words_for_terrace,
        NormalizedRealEstateColumns.is_exact_address.value: words_for_is_address_exact,
        NormalizedRealEstateColumns.is_exterior.value: words_for_exterior,
        NormalizedRealEstateColumns.cardinal_direction_id.value: words_for_cardinal_orientation,
        NormalizedRealEstateColumns.property_type_id.value: words_for_property_type,
        NormalizedRealEstateColumns.usage_id.value: words_for_usage_type,
        NormalizedRealEstateColumns.area.value: words_for_area,
        NormalizedRealEstateColumns.conservation_id.value: words_for_status,
        NormalizedRealEstateColumns.build_status_id.value: words_for_construction_status,
        NormalizedRealEstateColumns.floor.value: words_for_floor,
        NormalizedRealEstateColumns.has_air_conditioner.value: words_for_air_conditioning,
        NormalizedRealEstateColumns.pet_friendly.value: words_for_pet_friendly,
    }



def create_characteristics_set() -> Set[str]:
    return set().union(*list(spanish_column_mapper.values()))


def sentence_as_string(sentence: List[str]) -> str:
    """Z. comunitaria -> [ 'Z', '.', 'comunitaria']"""
    new_sentence = []
    for word in sentence:
        if word == '.':
            new_sentence[len(new_sentence) -1] += '.'
        else:
            new_sentence.append(word)
    return " ".join(new_sentence)

def get_properties(sentence: List[str]) -> Tuple[int, str]:
    sentence_string = sentence_as_string(sentence)
    for normalized_name in spanish_column_mapper:
        detection_words = spanish_column_mapper[normalized_name]
        index: int = find_word_in_sentence(detection_words, sentence)
        if index > -1:
            return index, normalized_name
        index_new: int = find_word_in_sentence_as_string(detection_words, sentence_string)
        if index_new > -1:
            return index_new, normalized_name
    return None



def look_for_negation_previous_word(sentence: List[str], word_index: int, negation_words: Set[str]) -> bool:
    # TODO improve using POS taggign to check if previous word does not suck.
    prev_word = sentence[word_index - 1].lower()
    return prev_word in negation_words


def find_word_in_sentence(detection_words: Set[str], sentence: List[str]) -> int:
    number_of_words = len(sentence)
    for i in range(number_of_words):
        word = sentence[i].lower()
        # print(f"word={word},sentence = {sentence}")
        if word in detection_words:
            return i
    return -1

def find_word_in_sentence_as_string(detection_words: Set[str], sentence: str) -> int:
    for word in detection_words:
        index: int = sentence.find(word)
        if index != -1:
            return index
    return -1


def find_word_and_determine_proper(detection_words: Set[str], sentence: List[str], negation_words: Set[str]):
    index = find_word_in_sentence(detection_words,sentence)
    if index > -1:
        word_lowered = sentence[index].lower()
        return word_lowered in negation_words
    return False


def get_text_sentence_tokenized(input_text: str) -> List[List[str]]:
    return [tokenizer_words.tokenize(t) for t in sent_tokenize(input_text)]


def pos_tag_tokens(sentence: List[str]) -> List[Tuple[str, str]]:
    return pos_tag(sentence)
