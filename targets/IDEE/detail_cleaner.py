import traceback
from abc import ABC
from datetime import datetime
from typing import List

from parsing.RealEstateObject import RealEstateData
from parsing.homogenizer.homogenezer import RealEstateCleaner
from targets.IDEE.word_detection import find_negation_in_text, find_elevator_in_text, \
    find_garage_included_words, find_garage_not_included_words, find_balcony_in_text, find_terrace_in_text, \
    find_is_exterior_in_text, multiple_words_in_text, CONSERVATION_STATUS, word_in_text, NON_EXTERIOR_WORDS, \
    map_canonical_contains, HAS_AIR_CONDITIONING, BUILD_STATUS
from targets.IDEE.property_types import IDEE_PROPERTY_TYPES
from targets.IDEE.word_detection import map_canonical_exact_dict, CARDINAL_DIRECTION, \
    SPANISH_MONTHS, MONTH_MAPPING, map_canonical_contains_dict
from text_preprocessing import get_all_float, get_single_digit, concat_all_digits, has_digits
from log import container

# First Raw version -> Combine columns into one onto criteria
# write to file -> write ->
from parsing.homogenizer.word_detector import get_text_sentence_tokenized, multiple_words_in_sentence, \
    find_word_in_sentence
from parsing.normalized_columns import NormalizedRealEstateColumns
from targets.FTCA.word_detection import spanish_column_mapper

logger = container.get_logger('clean', 'clean.log')



def get_integer(val: str) -> int:
    if val is not None:
        return get_single_digit(val)
    return val


def contains_sub_string(o_str: str, s_str: str) -> bool:
    return o_str.find(s_str) != -1

def strip_region(address_column_val):
    if address_column_val is not None:
        return address_column_val.strip().replace(',', ' ').replace(';', '')
    return None


class Cleaner(RealEstateCleaner):

    def clean_full_address(self, listing: RealEstateData) -> str:
        address_column_val = listing.full_address
        return strip_region(address_column_val)

    def clean_construction_year(self, listing: RealEstateData) -> int:
        column_val = listing.construction_year
        return get_integer(column_val)

    def clean_description(self, listing: RealEstateData) -> int:
        column_val = listing.description
        return strip_region(column_val)

    def clean_original_admin4(self, listing: RealEstateData) -> str:
        column_val = listing.original_admin4
        return strip_region(column_val)

    def clean_energy_cert_id(self, listing: RealEstateData) -> str:
        column_val = listing.energy_cert_id
        if column_val is not None:
           return column_val.upper()
        return column_val

    def clean_has_elevator(self, listing: RealEstateData) -> bool:
        column_val = listing.has_elevator

        if column_val is None:
            return None
        neg = find_negation_in_text(column_val)
        has_elevator = find_elevator_in_text(column_val)
        if neg:
            return not has_elevator
        return has_elevator

    def clean_has_garage(self, listing: RealEstateData) -> bool:
        column_val = listing.has_garage
        if column_val is not None:
            is_not_included = find_garage_not_included_words(column_val)
            is_included = find_garage_included_words(column_val) or column_val == 'garajes'
            return is_not_included or is_included
                # raise Exception('garage included, something went wrong in parsing\n'
                #             f'column_value = {column_val}\n'
                #             f'with column = {NormalizedRealEstateColumns.has_garage.value}')
        return None

    def clean_has_garage_included(self, listing: RealEstateData) -> bool:
        column_val = listing.has_garage_included
        if column_val is not None:
            is_included = find_garage_included_words(column_val) or column_val == 'garajes'
            return is_included
                # logger.debug('garage not included, something went wrong in parsing\n'
                #             f'column_value = {column_val}, \n'
                #             f'with column = {NormalizedRealEstateColumns.has_garage_included.value}')
        return None

    def clean_has_balcony(self, listing: RealEstateData) -> bool:
        column_val = listing.has_balcony
        if column_val is not None:
            if find_balcony_in_text(column_val):
                return True
            raise Exception(f'Cannot find balcony in non None\n'
                            f'column_value = {column_val}\n'
                            f'with column = {NormalizedRealEstateColumns.has_garage_included.value}')
        return None

    def clean_has_terrace(self, listing: RealEstateData) -> bool:
        column_val = listing.has_terrace
        if column_val is not None:
            if find_terrace_in_text(column_val):
                return True
            raise Exception(f'Cannot find balcony in non None\n'
                            f'column_value = {column_val}\n'
                            f'with column = {NormalizedRealEstateColumns.has_garage_included.value}')
        return None

    def clean_is_exact_address(self, listing: RealEstateData) -> bool:
        column_val = listing.is_exact_address
        return column_val is not None

    def clean_is_exterior(self, listing: RealEstateData) -> bool:
        column_val = listing.is_exterior
        if column_val is not None:
            if find_is_exterior_in_text(column_val):
                return True
            elif word_in_text(NON_EXTERIOR_WORDS, column_val):
                return False
        return None

    def clean_lat(self, listing: RealEstateData) -> float:
        column_val = listing.lat
        if column_val is not None:
            try:
                return get_all_float(column_val)[0]
            except Exception as e:
                logger.debug(f"expection with float:\n"
                             f"{traceback.format_exc()}\n"
                             f"value = {column_val}")
        return None

    def clean_lon(self, listing: RealEstateData) -> float:
        column_val = listing.lon
        return self.clean_lat(column_val)

    def clean_original_admin2(self, listing: RealEstateData) -> float:
        column_val = listing.original_admin2
        return strip_region(column_val)

    def clean_original_admin3(self, listing: RealEstateData) -> str:
        column_val = listing.original_admin3
        return strip_region(column_val)

    def clean_n_baths(self, listing: RealEstateData) -> int:
        column_val = listing.n_baths
        if column_val is not None and (not has_digits(column_val)):
            return None
        return get_integer(column_val)

    def clean_floor(self, listing: RealEstateData) -> float:
        column_val = listing.floor
        if column_val is not None:
            if column_val == 'Entreplanta':
                return 0.5
            if not has_digits(column_val):
                return None
        return get_integer(column_val)

    def clean_n_rooms(self, listing: RealEstateData) -> int:
        column_val = listing.n_rooms
        if column_val is not None and (not has_digits(column_val)):
            return None
        return get_integer(column_val)

    def clean_original_admin5(self, listing: RealEstateData) -> str:
        column_val = listing.original_admin5
        return strip_region(column_val)

    def clean_cardinal_direction_id(self, listing: RealEstateData) -> int:
        column_val = listing.cardinal_direction_id
        canonical =  map_canonical_contains_dict(CARDINAL_DIRECTION, column_val)
        if canonical is not None:
            return int(canonical)
        return canonical

    def clean_listing_id(self, listing: RealEstateData) -> str:
        column_val = listing.listing_id
        return column_val

    def clean_property_type_id(self, listing: RealEstateData) -> int:
        # return map_canonical_contains_dict(IDEE_PROPERTY_TYPES, column_val)
        column_val = listing.property_type_id
        if column_val is not None:
            sentence = get_text_sentence_tokenized(column_val.lower())[0]
            for prop_type in spanish_column_mapper:
                prop_type_lower = prop_type.lower()
                if prop_type_lower.find(' ') != -1:
                    # print(f"lower = {prop_type_lower}, {sentence}")
                    indices = multiple_words_in_sentence(prop_type_lower, sentence)
                    if len(indices) > 0:
                        return spanish_column_mapper[prop_type]
                else:
                    index = find_word_in_sentence(prop_type_lower, sentence)
                    if index > -1:
                        return spanish_column_mapper[prop_type]
                    if prop_type.find(' ') != -1:
                        indices = multiple_words_in_sentence(prop_type_lower, sentence)
                        if len(indices) > 0:
                            return IDEE_PROPERTY_TYPES[prop_type]
        print(f"column_val: {column_val}, {sentence}")
        return None

    def clean_last_update(self, listing: RealEstateData) -> str:
        column_val = listing.last_update
        if column_val is not None:
            lowered = column_val.lower()
            for spanish_month in SPANISH_MONTHS:
                if spanish_month in lowered:
                    month = MONTH_MAPPING[SPANISH_MONTHS[spanish_month]]
                    day = get_single_digit(lowered)
                    now = datetime.now()
                    try:
                        date = datetime(year=now.year,month=month,day=day)
                    except Exception as e:
                        logger.debug(f"date exception occured for \n"
                                     f'date value = {column_val}')
                        day -= 1
                        date = datetime(year=now.year,month=month,day=day)
                    # print(f"date_col = {column_val}\n"
                    #       f'date = {date.strftime("%m/%d/%Y")}')
                    return date.strftime("%m-%d-%Y")
        return None

    def clean_garage_price(self, listing: RealEstateData) -> int:
        column_val = listing.garage_price
        if column_val is not None and has_digits(column_val):
            return get_single_digit(column_val)
        return None

    def clean_contact_name(self, listing: RealEstateData) -> str:
        column_val = listing.contact_name
        return strip_region(column_val)

    def helper_area(self, column_val: str, index: int) -> int:
        """1.000 m² construidos, 320 m² útiles
           1.089 m² construidos
        """
        if column_val is not None:
            size_string = column_val.split('construidos')[index]
            try:
                return concat_all_digits(size_string)
            except Exception as e:
                pass
                # print(f'area = "{column_val}"')
        return None

    def heating_type(self, listing: RealEstateData) -> str:
        column_val = listing.heating_type
        return column_val

    def clean_area(self, listing: RealEstateData) -> int:
        column_val = listing.area
        return self.helper_area(column_val, 0)

    def clean_area_util(self, listing: RealEstateData) -> int:
        column_val = listing.area_util
        return self.helper_area(column_val, 1)

    def image_urls(self, listing: RealEstateData) -> str:
        column_val = listing.image_urls
        return column_val

    def clean_parent_id(self, listing: RealEstateData) -> str:
        column_val = listing.parent_id
        return column_val

    def clean_conservation_id(self, listing: RealEstateData) -> int:
        column_val = listing.conservation_id
        words = multiple_words_in_text(CONSERVATION_STATUS, column_val)
        if len(words) == 0:
            return 0
        if 'buen estado' in words:
            return 5
        elif 'para reformar' in words:
            return 4
        elif 'construcción' in words:
            return 7
        elif 'terminada' in words:
            return 8
        else:
            raise Exception(f"for convervation_id the following unknown value was found: "
                            f"{column_val} ")

    def title(self, listing: RealEstateData) -> str:
        column_val = listing.title
        if column_val is not None:
            return column_val.strip()
        return None

    def detail_page(self, listing: RealEstateData) -> str:
        return listing.detail_page

    def portal_reference(self, listing: RealEstateData) -> str:
        return listing.portal_reference.strip()

    def contact_phone(self, listing: RealEstateData) -> str:
        column_val = listing.contact_phone
        if column_val is not None:
            return column_val.strip().replace(' ', '')
        return None

    def is_urgent(self, listing: RealEstateData) -> bool:
        column_val = listing.is_urgent
        return column_val

    def clean_agency_name(self, listing: RealEstateData) -> str:
        column_val = listing.agency_name
        if column_val is not None:
            return column_val.strip()
        return None

    def agency_branch(self, listing: RealEstateData) -> str:
        return listing.agency_branch

    def agency_url(self, listing: RealEstateData) -> str:
        return listing.agency_url

    def local_price(self, listing: RealEstateData) -> int:
        column_val = listing.local_price
        if column_val is not None and column_val.strip() != '':
            try:
                return concat_all_digits(column_val)
            except Exception as e:
                logger.debug(f"local_price_error = {column_val}")
        return None

    def add_to_additional_fields(self, column_val: str, additional_field_json: dict, originial_column: str) -> None:
        if originial_column == 'price_portal_discount':
            if column_val is None:
                return None
            val =  column_val.split('€')[1]
        elif originial_column == 'location_all':
            val = column_val
        else:
            raise Exception(f"add_to_aditional_fields from unexpected column = {originial_column}")
        additional_field_json[originial_column] = val

    def n_images(self, listing: RealEstateData) -> int:
        column_val = listing.n_images
        if column_val is not None:
            return get_single_digit(column_val)
        return None

    def clean_has_pool(self, listing: RealEstateData) -> bool:
        return listing.has_pool is not None

    def clean_has_common_zones(self, listing: RealEstateData) -> bool:
        return listing.has_common_zones is not None

    def clean_has_racket_zone(self, listing: RealEstateData) -> bool:
        column_val = listing.description
        if column_val is not None:
            return column_val.find('pista de pádel') != -1 or column_val.find('pista de tenis') != -1
        return None

    def clean_has_storage(self, listing: RealEstateData) -> bool:
        column_val = listing.has_storage
        if column_val is not None:
            return True
        return None

    def clean_has_developable(self, listing: RealEstateData) -> bool:
        column_val = listing.property_type_id
        if column_val is not None:
            new_val = column_val.lower()
            if 'urbanizable' in new_val and not ('no' in new_val):
                return True
            elif 'no urbanizable' in new_val:
                return False
        return None

    def clean_pet_friendly(self, listing: RealEstateData) -> bool:
        column_val = listing.pet_friendly
        if column_val is not None:
            new_val = column_val.lower()
            return not (find_negation_in_text(new_val))
        return None

    def clean_has_air_conditioner(self, listing: RealEstateData) -> bool:
        column_val = listing.has_air_conditioner
        canonical = map_canonical_contains(HAS_AIR_CONDITIONING, column_val)
        if canonical is not None:
            return int(canonical) == 1
        return canonical

    def build_status_id(self, listing: RealEstateData) -> int:
        column_val = listing.build_status_id
        canonical = map_canonical_contains(BUILD_STATUS, column_val)
        if canonical is not None:
            return int(canonical)
        return canonical

    def plan_available(self, listing: RealEstateData) -> bool:
        return listing.plan_available

    def has_virtual_tour(self, listing: RealEstateData) -> bool:
        return listing.has_virtual_tour

    def has_three_dim_tour(self, listing: RealEstateData) -> bool:
        return listing.has_three_dim_tour

