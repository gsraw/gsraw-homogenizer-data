# div.revip-general-details div.category-container div.category
# title element below:
# span.pri-props-name
# value element below:
# span.pri-props-value ,, contains Vendedor

# creation_date span.creation-date
# view_count span.view-count

# span.breadcrumbs a span, all locations

# location_name = div.location-name
import gzip
import json
import traceback
from typing import Set
from chompjs import chompjs
from lxml.cssselect import CSSSelector
from html_functionality.html_local import read_into_html, lxml_el_to_bytes_to_string
from parsing.RealEstateObject import RealEstateData
from parsing.homogenizer.word_detector import get_text_sentence_tokenized, find_word_in_sentence
from parsing.normalized_columns import NormalizedRealEstateColumns
from parsing.run_parser import start_parsing_real_estate
from sql.batch_holder import BatcHolder
from sql.pool import Pool
from targets.IDEE.detail_cleaner import Cleaner
from targets.IDEE.property_types import IDEE_PROPERTY_TYPES
from targets.IDEE.word_detection import look_up_characteristics, words_for_garage
from text_preprocessing import concat_all_digits, get_single_digit, remove_all_identation
from src.log import container

logger = container.get_logger('err_parse','err_parse.log')

def get_item_not_equal_to(str_set, list):
    for item in list:
        if not (item in str_set):
            return item
    return None

def has_property_type(content: str, invalid_set: Set[str]) -> str:
    keys = []
    lowered_content: str = content.lower()
    for key in IDEE_PROPERTY_TYPES:
       lowered_key = key.lower()
       if lowered_key == 'no urbanizable' and 'terreno urbanizable' in lowered_content:
           pass
        # print(f'{lowered_key in lowered_content},{not (key in invalid_set)}')
       elif lowered_key in lowered_content and not (key in invalid_set):
           # print(f"returning key = {key}")
           return key
    return None


def new_pages():
    CSSSelector('div.table__new-dev-typologies a.table__row')
    CSSSelector('span.table__cell')

# <div class="details-property_features">
# <ul>
# <li>Se admiten mascotas</li>



def parser(row: dict, batch: BatcHolder, conn_pool: Pool):
    # info-data
    status = row['response_status']
    if not (status == 404 or status == 503):
        # print('new item')
        try:
            result_str = gzip.decompress(row['data_bytes']).decode('utf-8')
            result = read_into_html(result_str)
            sel_title = CSSSelector('h1 > span.main-info__title-main')
            sel_price = CSSSelector('span.info-data-price')
            sel_originial_price = CSSSelector('span.pricedown')
            # first is street, second district, third neighbourhood
            # fourth is municipo, 5th province
            sel_locations = CSSSelector('#headerMap > ul > li')
            sel_last_updated = CSSSelector('p.stats-text')
            sel_last_updated_text = CSSSelector('p.date-update-text')
            sel_tags = CSSSelector('strong.info-urgent')
            sel_description = CSSSelector('div.commentsContainer > div.comment')
            # sel_last_updated = CSSSelector('p.date-update-text')
            sel_price_2 = CSSSelector('strong.flex-feature-details')
            lat_string = """latitude"""
            long_string = "longitude"
            sel_agency_phone = CSSSelector('div.phone')
            sel_agency_href_and_name = CSSSelector('a.about-advertiser-name')
            sel_vendor_name = CSSSelector('div.professional-name > span')
            sel_agency_branch = CSSSelector('div.advertiser-name-container > span')
            # sel_plan_available = CSSSelector('button[data-button-type="plan"]')
            # sel_number_of_images = CSSSelector('button[data-button-type="no-pics"]')
            # sel_3d_visits = CSSSelector('button[data-button-type="3d-tour"]')
            # sel_virtual_tour = CSSSelector('button[data-button-type="virtual-tour"]')
            sel_is_accurate_address = CSSSelector('span.no-show-address-feedback-text')
            sel_all_property_info = CSSSelector('div.details-property_features')
            sel_vendor_asset_id = CSSSelector('p.txt-ref')
            # get src from images.
            # sel_main_image_url = CSSSelector('div.main-image_first > img')
            # sel_other_image_url = CSSSelector('#main-multimedia div.placeholder-multimedia.image img')
            images_urls_string = 'fullScreenGalleryPics'
            sel_parent_id = CSSSelector('a[data-parent-id]')
            listing: RealEstateData = RealEstateData()
            listing.week = row['week']
            listing.parse_date = row['c_parse_date']
            listing.listing_id = row['listing_id']
            all_property_info_containers = sel_all_property_info(result)
            listing.conservation_id = ''
            listing.title = sel_title(result)[0].text_content()
            is_trastero = 'Trastero' in listing.title
            is_edificio = 'Edificio' in listing.title
            is_garaje = 'Garaje' in listing.title
            is_terreno = 'Terreno' in listing.title
            # invalid_set = {'Garaje', 'Edificio', 'Trastero', 'Local', 'Nave',
            #                'Oficina','Terreno', 'Obra nueva'}
            invalid_set = {'garaje', 'edificio', 'trastero', 'local', 'nave',
                           'oficina', 'terreno', 'obra nueva'}
            # Should develop find multiple values found and then determine which one to choose.
            additional_fields = {}
            for property_info_container in all_property_info_containers:
                all_li = CSSSelector('ul > li')
                for property_info in all_li(property_info_container):
                    content_no_modified: str = property_info.text_content()
                    content: str = property_info.text_content().lower()
                    sentence = get_text_sentence_tokenized(content)[0]
                    if content.find('última actividad:') == -1:
                        if (not is_edificio) and (not is_trastero):
                            property_type = has_property_type(content_no_modified, invalid_set)
                            if property_type is not None:
                                if is_terreno and (property_type == 'Unifamiliar'):
                                    pass
                                else:
                                    listing.property_type_id = property_type
                        else:
                            characteristic = look_up_characteristics(sentence)
                            if characteristic is not None:
                                word, normalized_key = characteristic
                                setattr(listing, normalized_key, word)
                            elif find_word_in_sentence(words_for_garage, sentence) != -1:
                                   listing.has_garage_included = content
                                   listing.has_garage = content
                                   if content.find('€/') != -1:
                                       listing.garage_price = content
            # In case not found in characteristics, title contains canonical ID.
            if listing.property_type_id is None:
                listing.property_type_id = listing.title

            if 'Entreplanta' in listing.property_type_id:
                listing.floor = 'Entreplanta'

            all_locations = sel_locations(result)
            if len(all_locations) > 0:
                try:
                    listing.full_address = all_locations[0].text_content()
                    listing.original_admin5 = all_locations[1].text_content()
                    listing.original_admin4 = all_locations[2].text_content()
                    listing.original_admin3 = all_locations[3].text_content()
                    listing.original_admin2 = all_locations[4].text_content()
                except Exception as e:
                    sel_header = CSSSelector('#headerMap > ul')
                    additional_fields['location_all'] = lxml_el_to_bytes_to_string(sel_header(result)[0])
            accuracy_of_addresses = sel_is_accurate_address(result)
            listing.is_exact_address = len(accuracy_of_addresses) == 0
            # try:
            # except Exception as e:
            #     with open('debug_title.html', 'w+') as file:
            #         file.write(lxml_el_to_bytes_to_string(result))

            prices = sel_price(result)
            if len(prices) > 0:
                listing.local_price = prices[0].text_content()
            else:
                sel_price =  CSSSelector('p.info-data.txt-big')
                prices = sel_price(result)
                if len(prices) > 0:
                    listing.local_price = prices[0].text_content()
            # except Exception as e:
            #     with open('debug_price.html','w+') as file:
            #         file.write(lxml_el_to_bytes_to_string(result))
            discounts = sel_originial_price(result)
            if len(discounts) > 0:
                column_val = discounts[0].text_content()
                if column_val is None:
                    additional_fields[NormalizedRealEstateColumns.portal_price_discount] = None
                additional_fields[NormalizedRealEstateColumns.portal_price_discount] = column_val.split('€')[1]
            is_updated = sel_last_updated(result)
            if len(is_updated) > 0:
                listing.last_update = is_updated[0].text_content()
            is_updated_year = sel_last_updated_text(result)
            if len(is_updated_year) > 0:
                # Anuncio actualizado hace más de un año
                year_text = is_updated_year[0].text_content()
                if 'hace más de un año' in year_text:
                    # We don't know, i.e longer than a year.
                    listing.last_update = None

            tag = ''
            for tag_el in sel_tags(result):
                tag += tag_el.text_content().lower() + '|+|'
            listing.is_urgent = tag.find('urge') != -1
            if tag.find('obra nueva') != -1:
                listing.build_status_id = 'obra nueva'
            if tag.find('terminada') != -1:
                listing.conservation_id = 'terminada'

            has_description = sel_description(result)
            if len(has_description) > 0:
               listing.description = has_description[0].text_content()
            ag_phone = sel_agency_phone(result)
            if len(ag_phone) > 0:
                listing.contact_phone = ag_phone[0].text_content()

            agency_href = sel_agency_href_and_name(result)
            if len(agency_href) > 0:
                listing.agency_url = agency_href[0].get('href')
                listing.agency_name = agency_href[0].text_content()

            agency_name = sel_vendor_name(result)
            if len(agency_name) > 0:
                listing.contact_name = agency_name[0].text_content()

            agency_branch = sel_agency_branch(result)
            if len(agency_branch) > 0:
                listing.agency_branch = agency_branch[0].text_content()

            vendor_asset_id = sel_vendor_asset_id(result)
            if len(vendor_asset_id) > 0:
                listing.portal_reference = vendor_asset_id[0].text_content()

            # main_image_url = sel_main_image_url(result)
            # listing.image_urls = ''
            # if len(main_image_url) > 0:
            #     listing.main_image_url = main_image_url[0].get('src')


            index = result_str.find(images_urls_string)
            if index != -1:
                long_str_len = len(long_string) + index
                new_sub = result_str[long_str_len:]
                end_index = new_sub.find(']') + 1
                listing.image_urls = new_sub[:end_index].replace('nGalleryPics : ', '')

            index = result_str.find(long_string)
            if index != -1:
                long_str_len = len(long_string) + index
                new_sub = result_str[long_str_len:]
                end_index = new_sub.find(',')
                listing.lon = new_sub[:end_index]
            index = result_str.find(lat_string)
            if index != -1:
                long_str_len = len(lat_string) + index
                new_sub = result_str[long_str_len:]
                end_index = new_sub.find(',')
                listing.lat = new_sub[:end_index]
            index = result_str.find('fakeAnchorsObject')
            if index != -1:
                # long_str_len = len(lat_string) + index
                long_str_len = index
                new_sub = result_str[long_str_len:]
                end_index = new_sub.find('homeStaging')
                j_str = new_sub[:end_index]
                # print(f"j_str = {j_str}")
                a = chompjs.parse_js_object(j_str, json_params={'strict': False})
                listing.plan_available ='plan' in a
                listing.has_three_dim_tour = 'tour' in a
                listing.has_virtual_tour = 'virtual-tour' in a
                if 'no-pics' in a:
                    listing.n_images = a['no-pics']
                # TODO add video by checking if video
            parent_ids = sel_parent_id(result)
            if len(parent_ids) > 0:
                listing.parent_id = parent_ids[0].get('data-parent-id')
            # fakeAnchorsObject
            cleaner = Cleaner()
            cleaner.clean(listing)
            listing.additional_fields = json.dumps(additional_fields)
            batch.insert_data_model_in_new(listing=listing, conn_pool=conn_pool)
        except Exception as e:
            logger.critical(f"exception occcured ::\n"
                            f"{traceback.format_exc()}\n"
                            f"listing_id = {row['listing_id']}\n"
                            f"response code = {row['response_status']}")

def run(start_week: str, end_week: str, target: str):
    logger.debug(f"run is called with {start_week},{end_week}")
    start_parsing_real_estate(
                target=target,
                parse_type='dp',
                initial_week=start_week,
                final_week=end_week,
                parser=parser,
                unique=True,
    )

if __name__ == "__main__":
    run('2021w48', None, 'idee')
