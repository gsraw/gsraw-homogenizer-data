from sql.CRUD import mysql_connection


def get_idee_property_types():
    with mysql_connection() as conn:
        with conn.cursor() as cu:
            cu.execute(
                        f"select original_id, original_definition "
                        f"from configurations.co_canonical_relations "
                        f"where property_target = 'property_type' and source = 1")
            all_rows = cu.fetchall()
    result = {}
    for o_id, o_def in all_rows:
        result[o_def] = o_id
    return result

IDEE_PROPERTY_TYPES = get_idee_property_types()