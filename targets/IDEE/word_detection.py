from typing import Tuple, List, Dict, Set

from parsing.homogenizer.word_detector import multiple_words_in_sentence, find_word_in_sentence, sentence_as_string
from parsing.normalized_columns import NormalizedRealEstateColumns
from targets.IDEE.property_types import IDEE_PROPERTY_TYPES
from targets.IDEE.usage_types import USAGE_TYPES

NEGATION_TERMS = {'sin', 'no'}

FLOOR_WORDS = {
    'planta', # but not Plantas which indicates multiple
    'entreplanta',
    'semisótano'
}
NUMBER_OF_FLOORS_WORDS = {
    'plantas',
}
ELEVATOR_WORDS = {
        "ascensor",
        # "ascensores",
}
GARAGE_WORDS = {
    'garaje',
}

GARAGE_NOT_INCLUDED_WORDS = {
    'adicionales',
    '€/mes'
}

# Acceso adaptado a personas con movilidad reducida
GARAGE_INCLUDED_WORDS ={
    'incluida en el precio',
    'puerta automática de',
    'incluidas en el precio',
    'tipo de garaje',
    'incluido'
}
BALCONY_WORDS = {
    'balcón',
}
TERRACE_WORDS = {
    'terraza',
}
EXTERIOR_WORDS = {
    'exterior',
}

NON_EXTERIOR_WORDS ={
    'interior',
}

BUILDING_WORDS = {
    'edificio',
}

CONSERVATION_STATUS = {
    'segunda mano',
    'buen estado',
    'para reformar',
    'construcción',
    'terminada',
}

words_for_common_zones = {
    'zonas verdes'
}
words_for_storage = {
    'trastero'
}
words_for_pool = {
    'piscina'
}

words_for_area = {
    'm² construidos'
}

words_for_build_status = {
    'segunda mano'
}

words_for_status = {
    'buen estado',
    'para reformar',
    'construcción',
}
words_for_construction_year = {
    'construido en'
}
words_for_cardinal_direction_id = {
    'orientación',
}
words_for_bathroom = {
    'baño',
    'baños'
}
words_for_room = {
    'habi',
    'habs.', 'habs', 'hab.', 'hab', 'habitaciones', 'habitación'
}
words_for_terrace = {
    'terraza'
}
words_for_balcony = {
    'balcón'
}
words_for_heating_type = {
    'calefacción'
}
words_for_energy_cert_id = {
    'certificación energética'
}
words_for_elevator = {'ascensor'}
words_for_floor = {'planta', ''}
negation_words_for_floor = {'plantas'}
words_for_is_exterior = {'exterior', 'interior'}
words_for_garage = {'garaje', 'garajes'}
words_for_garage_price = {'€/'}
words_for_garage_included = {  'incluida en el precio',
    'puerta automática de',
    'incluidas en el precio',
    'tipo de garaje',
    'incluido'}
words_for_garage_not_included = {
    'adicionales',
    '€/mes'
}
# price negates included
words_for_air_conditioning = {'aire acond', 'aire acondi', 'aire acondicionado'}
words_for_pet_friendly = {'se admiten mascotas'}
words_for_racketzone = {'pista de pádel', 'pista de tenis'}
words_for_description = {}
# Need multiple_word search

spanish_column_with_subtype = {
    NormalizedRealEstateColumns.has_garage.value: words_for_garage,
    NormalizedRealEstateColumns.has_garage_included.value: words_for_garage_included,
    NormalizedRealEstateColumns.garage_price.value: words_for_garage_price,

}

spanish_column_mapper = \
    {
        NormalizedRealEstateColumns.construction_year.value : words_for_construction_year,
        NormalizedRealEstateColumns.description.value : words_for_description,
        NormalizedRealEstateColumns.has_racket_zone.value : words_for_racketzone,
        NormalizedRealEstateColumns.energy_cert_id.value : words_for_energy_cert_id,
        NormalizedRealEstateColumns.has_elevator.value: words_for_elevator,
        NormalizedRealEstateColumns.has_common_zones.value: words_for_common_zones,
        NormalizedRealEstateColumns.has_storage.value: words_for_storage,
        NormalizedRealEstateColumns.has_pool.value: words_for_pool,
        NormalizedRealEstateColumns.has_balcony.value: words_for_balcony,
        NormalizedRealEstateColumns.has_terrace.value: words_for_terrace,
        NormalizedRealEstateColumns.is_exterior.value: words_for_is_exterior,
        NormalizedRealEstateColumns.cardinal_direction_id.value: words_for_cardinal_direction_id,
        # NormalizedRealEstateColumns.property_type_id.value: set(list(IDEE_PROPERTY_TYPES.keys())),
        NormalizedRealEstateColumns.usage_id.value: USAGE_TYPES,
        NormalizedRealEstateColumns.area.value: words_for_area,
        NormalizedRealEstateColumns.conservation_id.value: words_for_status,
        NormalizedRealEstateColumns.build_status_id.value: words_for_build_status,
        NormalizedRealEstateColumns.floor.value: words_for_floor,
        NormalizedRealEstateColumns.has_air_conditioner.value: words_for_air_conditioning,
        NormalizedRealEstateColumns.pet_friendly.value: words_for_pet_friendly,
    }


def retrieve_words_with_spaces_and_without(words: Set[str]):
    space_set = set()
    word_set = set()
    for word in words:
        word_lower = word.lower()
        if word_lower.find(' ') != -1:
            space_set.add(word_lower)
        else:
            word_set.add(word_lower)
    return space_set, word_set

def are_space_words_in_sentence(space_words: Set[str], sentence):
    for space_word in space_words:
        indices = multiple_words_in_sentence(space_word, sentence)
        if len(indices) > 0:
            return space_word
    return None


def are_words_in_sentence(words: Set[str], sentence):
    for word in words:
        if len(multiple_words_in_sentence(word, sentence)):
            return word
    return None


# def look_up_characteristics(word_mapper, sentence:List[str]):
#     result = []
#     for word_key in word_mapper:
#         words = word_mapper[word_key]
#         space_set, word_set = retrieve_words_with_spaces_and_without(words)
#         detected_space_word = are_space_words_in_sentence(space_set, sentence)
#         detected_word = are_words_in_sentence(word_set, sentence)
#         if detected_space_word is not None:
#             result.append((detected_space_word, word_key))
#         elif detected_word is not None:
#             result.append((detected_word, word_key))
#     return result


def look_up_characteristics(sentence: List[str]):
    for word_key in spanish_column_mapper:
        words = spanish_column_mapper[word_key]
        space_set = set()
        for word in words:
            word_lower = word.lower()
            if word_lower.find(' ') != -1:
                # print(f"lower = {prop_type_lower}, {sentence}")
                space_set.add(word_lower)
                words.remove(word)
        for space_word in space_set:
            indices = multiple_words_in_sentence(space_word, sentence)
            if len(indices) > 0:
                return space_word, word_key
        if len(words) > 0:
            index = find_word_in_sentence(words, sentence)
            if index > -1:
                return sentence[index].lower(), word_key
    return None



def map_canonical_contains(canonical_dict: Dict[str, List[str]], column_val: str) -> str:
    """Need to agree on policy which value represents the key"""
    if column_val is None or column_val == '':
        return None
    column_val.strip().lower()
    for status in canonical_dict:
        vals = canonical_dict[status]
        if any([val in column_val for val in vals]):
            return vals[0]
    raise Exception(f"Canonical value cannot be found for {column_val} in\n"
                    f"{canonical_dict}")


def map_canonical_exact(canonical_dict: Dict[str, List[str]], column_val: str) -> str:
    """Need to agree on policy which value represents the key"""
    if column_val is None or column_val == '':
        return None
    column_val.strip().lower()
    for status in canonical_dict:
        vals = canonical_dict[status]
        if column_val in vals:
            return vals[0]
    raise Exception(f"Canonical value cannot be found for {column_val} in\n"
                    f"{canonical_dict}")


def map_canonical_exact_dict(canonical_dict: Dict[str, int], column_val: str) -> int:
    if column_val is not None:
        lowered = column_val.lower()
        for prop_type in canonical_dict:
            lowered_column = prop_type.lower()
            if lowered == lowered_column:
                return canonical_dict[prop_type]
    return None


def map_canonical_contains_dict(canonical_dict: Dict[str, int], column_val: str) -> int:
    if column_val is not None:
        lowered = column_val.lower()
        for prop_type in canonical_dict:
            lowered_column = prop_type.lower()
            if lowered_column in lowered:
                return canonical_dict[prop_type]
    return None
# TODO MOVE THIS OFFICIAL


BUILD_STATUS = {
    "unknown": [
        "0"
    ],
    "good_condition": [
        "1",
        "buen estado",
        "buono / abitabile",
        "ottimo/ristrutturato",
        "preserved"
    ],
    "partial_refurbished": [
        "2",
        "reforma parcial",
        "renovated",
        "renovado"
    ],
    "brand_new": [
        "6",
        "a estrenar",
        "nuovo / in costruzione",
        "new_development",
        "obra nueva",
        "obra nueva terminada"
    ],
    "under_construction": [
        "8",
        "en construcción",
        "en construccion",
        "em construcao",
        "unfinished"
    ],
    "to_completely_refurbish": [
        "5",
        "da ristrutturare",
        "para recuperar",
        "para reformar",
        "ruína",
        "ruina",
        "reforma total",
        "requires_renovation",
        "da ristrutturare"
    ],
    "finished": [
        "7",
        "terminada",
        "renovado",
        "terminada",
        "ottimo / ristrutturato",
        "requires_renovation"
    ]
}

IS_URGENT = {
"unknown": [
        "0"
    ],
    "True" : [
        "1",
        "urge"
    ]
}


HAS_AIR_CONDITIONING = {
    "unknown": [
        "0"
    ],
    "True" : [
        "1",
        "aire acondicionado",
        "aire acond",
    ]
}

CARDINAL_DIRECTION = {
    "este" : 1,
    'norte': 2,
    'oeste': 3,
    'sur': 4
}


CONSERVATION_ID = {
    'buen estado': 5,
    'para reformar': 4,
    'construcción': 7,
    'terminada': 8,
    'unknown': 0
}



SPANISH_MONTHS = {
'enero': 'january',
'febrero': 'february',
'marzo': 'march',
'abril': 'april',
'mayo': 'may',
'junio': 'june',
'julio': 'july',
'agosto': 'august',
'septiembre': 'september',
'octubre': 'october',
'noviembre': 'november',
'diciembre': 'december'
}

MONTH_MAPPING  = {
'january': 1,
'february': 2,
'march': 3,
'april': 4,
'may': 5,
'june': 6,
'july': 7,
'august': 8,
'september': 9,
'october': 10,
'november': 11,
'december': 12
}


def word_in_text(words_tuple: Tuple[str, ...], column_val) -> bool:
    if column_val is not None:
        val = column_val.lower()
        for word in words_tuple:
            # print(f"val={val},"
            #       f"word={word}")
            if word in val:
                return True
    return False


def multiple_words_in_text(words_tuple: Tuple[str, ...], column_val) -> List[str]:
    words_found = []
    if column_val is not None:
        val = column_val.lower()
        for word in words_tuple:
            if word in val:
                words_found.append(word)
    return words_found

def find_negation_in_text(column_val: str) -> bool:
    return word_in_text(NEGATION_TERMS, column_val)

def find_elevator_in_text(column_val: str) -> bool:
    return word_in_text(ELEVATOR_WORDS, column_val)

def find_garage_included_words(column_val: str) -> bool:
    return word_in_text(GARAGE_INCLUDED_WORDS, column_val)

def find_garage_not_included_words(column_val: str) -> bool:
    return word_in_text(GARAGE_NOT_INCLUDED_WORDS, column_val)

def find_balcony_in_text(column_val: str) -> bool:
    return word_in_text(BALCONY_WORDS, column_val)

def find_terrace_in_text(column_val: str) -> bool:
    return word_in_text(TERRACE_WORDS, column_val)

def find_is_exterior_in_text(column_val: str) -> bool:
    return word_in_text(EXTERIOR_WORDS, column_val)