import unittest
import src.dates as d
import src.filtering as f
import src.text_preprocessing as p


class ValidateHomogenizeMethods(unittest.TestCase):
    def test_previous_week(self):
        print('Validate get_previous_week')
        self.assertEqual(d.TodayIso.get_previous_week('2021w32'), '2021w31')
        self.assertEqual(d.TodayIso.get_previous_week('2021w01'), '2020w53')

    def test_get_iso_daybat_format_from_table_name(self):
        print('Validate get_iso_daybat_format_from_table_name')
        self.assertEqual(d.get_iso_daybat_format_from_table_name('test_serp_internal_parsed_2021w32'), '2021w32')
        self.assertEqual(d.get_iso_daybat_format_from_table_name('chrono24_serp_internal_parsed_2021w01'), '2021w01')

    def test_get_previous_week_table_name(self):
        print('Validate get_previous_week_table_name')
        self.assertEqual(d.TodayIso.get_previous_week_table_name('chrono24_serp_internal_parsed_2021w01'),
                         'chrono24_serp_internal_parsed_2020w53')
        self.assertEqual(d.TodayIso.get_previous_week_table_name('test_serp_internal_parsed_2021w32'),
                         'test_serp_internal_parsed_2021w31')

    def test_floor_two_decimals(self):
        print('Validate floor_two_decimals')
        self.assertEqual(f.floor_two_decimals(2.4), 2.4)
        self.assertEqual(f.floor_two_decimals(2.4494), 2.44)
        self.assertEqual(f.floor_two_decimals(0), 0.00)

    def test_ceil_two_decimals(self):
        print('Validate ceil_two_decimals')
        self.assertEqual(f.ceil_two_decimals(0.9999), 1.0)
        self.assertEqual(f.ceil_two_decimals(4.995), 5.0)
        self.assertEqual(f.ceil_two_decimals(4.495), 4.5)

    def split_integer_interval(self):
        print('Validate split_integer_interval')
        self.assertEqual(f.split_integer_interval([3, 4]), ((3, 3), (4, 4)))
        self.assertEqual(f.split_integer_interval([3, 5]), ((3, 4), (4, 5)))

    def test_remove_comma(self):
        print('Validate remove_comma')
        self.assertEqual(p.remove_comma('cats, dogs'), 'cats dogs')
        self.assertEqual(p.remove_comma('cats and dogs'), 'cats and dogs')
        self.assertEqual(p.remove_comma('cats, dogs, pigs and others animals'), 'cats dogs pigs and others animals')


if __name__ == '__main__':
    unittest.main()
